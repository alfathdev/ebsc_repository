package com.alfathindo.ebsc.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.alfathindo.ebsc.R;
import com.alfathindo.ebsc.database.FinalScoreRepo;
import com.alfathindo.ebsc.entity.FinalScore;

/**
 * Created by safeimuslim on 3/14/16.
 */
public class FinalScoreHigh extends Fragment {
    View view;
    EditText ed_nilai,ed_comment;
    Switch switch1;
    Button btn_simpan;
    String id_fema,pesan="";


    FragmentActivity fragmentActivity;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        fragmentActivity = (FragmentActivity) activity;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.final_score_high,container,false);

        id_fema = getArguments().getString("id_fema");

        ed_nilai = (EditText) view.findViewById(R.id.ed_nilai);
        ed_comment = (EditText) view.findViewById(R.id.ed_comment);
        switch1 = (Switch) view.findViewById(R.id.switch1);
        btn_simpan = (Button) view.findViewById(R.id.btn_simpan);

        btn_simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FinalScoreRepo finalScoreRepo = new FinalScoreRepo(fragmentActivity);
                FinalScore finalScore = new FinalScore();
                finalScore.set_id_fema(id_fema);
                finalScore.set_komen(ed_comment.getText().toString());

                if(switch1.isChecked()){
                    finalScore.set_evaluation_required("Ya");
                }else{
                    finalScore.set_evaluation_required("Tidak");
                }

                if(finalScoreRepo.updateKomenFinalScore(FinalScore.TABLE_FINAL_SCORE_HIGH,finalScore) == 1){
                    Toast.makeText(fragmentActivity, "Data Berhasil di update",Toast.LENGTH_SHORT).show();

                }else{
                    Toast.makeText(fragmentActivity, "Data Gagal  di update",Toast.LENGTH_SHORT).show();
                }

            }
        });
        setData();

        return view;

    }
    public void setData(){
        try{
            FinalScore finalScore;
            FinalScoreRepo finalScoreRepo = new FinalScoreRepo(fragmentActivity);
            finalScore = finalScoreRepo.getFinalScroreById(FinalScore.TABLE_FINAL_SCORE_HIGH, id_fema);
            pesan = pesan+"Basic Score : "+finalScore.get_basic_score()+"\n";
            pesan = pesan+"Stories: "+finalScore.get_stories()+"\n";
            pesan = pesan+"Vertical Irregularity: "+finalScore.get_vertical_irregularity()+"\n";
            pesan = pesan+"Plan Irregularity: "+finalScore.get_plan_irregularity()+"\n";
            pesan = pesan+finalScore.get_text_code_bencmark()+": "+finalScore.get_code_bencmark()+"\n";
            pesan = pesan+"Soil type: "+finalScore.get_jenis_tanah_v()+"\n";
            pesan = pesan+"------------------------------------------------\n";
            pesan = pesan+"Final Score: "+finalScore.get_final_score()+"\n";
            pesan = pesan+"Resiko Gempa: "+finalScore.get_resiko_gempa()+"\n";

            ed_nilai.setText(pesan);
            ed_comment.setText(finalScore.get_komen());
            if(finalScore.get_evaluation_required().equals("Ya")){
                switch1.setChecked(true);
            }


        }catch (NullPointerException e){
            Log.d("NullPointerException", e+"");
        }
    }

}