package com.alfathindo.ebsc.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.alfathindo.ebsc.R;
import com.alfathindo.ebsc.database.FemaRepo;
import com.alfathindo.ebsc.entity.Fema;
import com.alfathindo.ebsc.entity.NilaiSeismicityRegion;

/**
 * Created by safeimuslim on 3/2/16.
 */
public class FormulirSeismicityRegion extends Fragment {

    FragmentActivity fragmentActivity;
    EditText ed_city,ed_latitude,ed_longitude,ed_nilai,ed_ss,ed_s1;
    private String latitude;
    private String longitude;
    private String kota;
    private String id_fema;
    public NilaiSeismicityRegion nilaiSeismicityRegion;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        fragmentActivity = (FragmentActivity) activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.formulir_seismicity_region,container,false);

        ed_city = (EditText) view.findViewById(R.id.ed_city);
        ed_latitude = (EditText) view.findViewById(R.id.ed_latitude);
        ed_longitude = (EditText) view.findViewById(R.id.ed_longitude);
        ed_nilai = (EditText) view.findViewById(R.id.ed_nilai);
        ed_ss = (EditText) view.findViewById(R.id.ed_ss);
        ed_s1 = (EditText) view.findViewById(R.id.ed_s1);

        String pesan="";
        try{
            if(getArguments().getString("id_fema") != null){
                id_fema = getArguments().getString("id_fema");
                FemaRepo femaRepo = new FemaRepo(fragmentActivity);
                Fema fema = femaRepo.getFemaById(id_fema);
                latitude = fema.get_latitude();
                longitude = fema.get_longitude();
                kota = fema.get_kota();
            }else{
                latitude = getArguments().getString("latitude");
                longitude = getArguments().getString("longitude");
                kota = getArguments().getString("kota");
            }
        }catch (NullPointerException e){
            Log.i("ERROR from_bundle: ",e+"");
        }

        ed_city.setText(kota);
        ed_latitude.setText(latitude);
        ed_longitude.setText(longitude);


        hitungSeismicity();
        return view;
    }

    public void hitungSeismicity(){
        String tampung = "";
        String ss = "0", s1 = "0";

        nilaiSeismicityRegion = new NilaiSeismicityRegion();
        for (int i=0; i<nilaiSeismicityRegion.getVariabel().length;i++){
            try {
                if (nilaiSeismicityRegion.getNilaiSeismicityRegion(kota) != null) {
                    try {
                        if(i == 1)
                            ss = nilaiSeismicityRegion.getNilaiSeismicityRegion(kota)[i];
                        if(i == 2)
                            s1 = nilaiSeismicityRegion.getNilaiSeismicityRegion(kota)[i];
                        tampung = tampung + nilaiSeismicityRegion.getVariabel()[i] + " : " + nilaiSeismicityRegion.getNilaiSeismicityRegion(kota)[i] + "\n";
                    } catch (ArrayIndexOutOfBoundsException e) {
                        Log.i("Error:", "ArrayIndexOutOfBoundsException" + e);
                        Toast.makeText(fragmentActivity, "ArrayIndexOutOfBoundsException", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    tampung = "Nilai Seismicity Region pada kota " + kota + " tidak ada dalam database!";
                    break;
                }
            }catch (NullPointerException e){
                Toast.makeText(fragmentActivity, "NullPointerException, kota: "+kota, Toast.LENGTH_SHORT).show();
            }
        }
        ed_ss.setText(ss);
        ed_s1.setText(s1);
        ed_nilai.setText(nilaiSeismicityRegion.csar(Double.parseDouble(ss),Double.parseDouble(s1)));

    }
}
