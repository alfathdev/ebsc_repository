package com.alfathindo.ebsc.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.alfathindo.ebsc.R;
import com.alfathindo.ebsc.database.FemaRepo;
import com.alfathindo.ebsc.entity.Fema;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by safeimuslim on 3/1/16.
 */
public class FormulirFemaSatuFragment extends Fragment {
    View view;
    FragmentActivity fragmentActivity;
    private String id_fema,nama_bangunan,alamat,kode_pos,identitas_lain,no_stories,tahun_pembangunan,screener,tanggal,jumlah_lantai,kegunaan;
    EditText ed_nama_bangunan,ed_alamat,ed_kode_pos,ed_identitas_lain,ed_no_stories,ed_tahun_pembangunan,ed_screener,ed_tanggal,ed_jumlah_lantai,ed_kegunaan;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        fragmentActivity = (FragmentActivity) activity;
    }

    public FormulirFemaSatuFragment(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.formulir_fema_satu, container, false);
        ed_nama_bangunan = (EditText) view.findViewById(R.id.ed_nama_bangunan);
        ed_alamat=(EditText) view.findViewById(R.id.ed_alamat);
        ed_kode_pos=(EditText) view.findViewById(R.id.ed_kode_pos);
        ed_identitas_lain=(EditText) view.findViewById(R.id.ed_identitas_lain);
        ed_no_stories=(EditText) view.findViewById(R.id.ed_no_stories);
        ed_tahun_pembangunan=(EditText) view.findViewById(R.id.ed_tahun_pembangunan);
        ed_screener=(EditText) view.findViewById(R.id.ed_screener);
        ed_tanggal=(EditText) view.findViewById(R.id.ed_tanggal);
        ed_jumlah_lantai=(EditText) view.findViewById(R.id.ed_jumlah_lantai);
        ed_kegunaan=(EditText) view.findViewById(R.id.ed_kegunaan);

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String cur_date = sdf.format(new Date());
        ed_tanggal.setText(cur_date);

        try{
            if(getArguments().getString("id_fema") != null){
                id_fema = getArguments().getString("id_fema");
                FemaRepo femaRepo = new FemaRepo(fragmentActivity);
                Fema fema = femaRepo.getFemaById(id_fema);
                ed_nama_bangunan.setText(fema.get_nama_bangunan());
                ed_alamat.setText(fema.get_alamat());
                ed_kode_pos.setText(fema.get_kode_pos());
                ed_identitas_lain.setText(fema.get_identitas_lain());
                ed_no_stories.setText(fema.get_no_stories());
                ed_tahun_pembangunan.setText(fema.get_tahun_pembangunan());
                ed_screener.setText(fema.get_screener());
                ed_tanggal.setText(fema.get_tanggal());
                ed_jumlah_lantai.setText(fema.get_jumlah_lantai());
                ed_kegunaan.setText(fema.get_kegunaan());
            }
        }catch (NullPointerException e){

        }
        return view;
    }
}
