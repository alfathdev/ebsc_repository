package com.alfathindo.ebsc.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alfathindo.ebsc.R;
import com.alfathindo.ebsc.adapter.ImageListAdapter;
import com.alfathindo.ebsc.database.FemaRepo;
import com.alfathindo.ebsc.entity.Fema;
import com.alfathindo.ebsc.entity.ImageItem;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by safeimuslim on 8/14/16.
 */
public class ReviewFragment extends Fragment {
    View view;
    FragmentActivity fragmentActivity;
    String id_fema;
    EditText ed_nilai;
    String pesan = "";
    String folder;
    GridView gridview;
    public  String GridViewDemo_ImagePath;

    ArrayList<ImageItem> arrayImageItem = new ArrayList<>();

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        fragmentActivity = (FragmentActivity) activity;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.review_final_score, container, false);
        id_fema = getArguments().getString("id_fema");
        ed_nilai = (EditText) view.findViewById(R.id.ed_nilai);
        gridview = (GridView) view.findViewById(R.id.gridview);

        try {
            Fema fema;
            FemaRepo femaRepo = new FemaRepo(fragmentActivity);
            fema = femaRepo.getFemaById(id_fema);

            pesan = pesan + "Kota: "+fema.get_kota()+"\n";
            pesan = pesan + "Longitude: "+fema.get_longitude()+"\n";
            pesan = pesan + "Latitude: "+fema.get_latitude()+"\n";
            pesan = pesan + "Seismicity: "+fema.get_seismicity()+"\n";
            pesan = pesan + "Ss: "+fema.get_ss()+"\n";
            pesan = pesan + "s1: "+fema.get_s1()+"\n";

            pesan = pesan + "\n";

            pesan = pesan + "Nama bangunan: "+fema.get_nama_bangunan()+"\n";
            pesan = pesan + "Alamat: "+fema.get_alamat()+"\n";
            pesan = pesan + "Kode pos: "+fema.get_kode_pos()+"\n";
            pesan = pesan + "Identitas lain: "+fema.get_identitas_lain()+"\n";
            pesan = pesan + "No stories: "+fema.get_no_stories()+"\n";
            pesan = pesan + "Screnner: "+fema.get_screener()+"\n";
            pesan = pesan + "Tanggal: "+fema.get_tanggal()+"\n";
            pesan = pesan + "Luas Area: "+fema.get_jumlah_lantai()+"\n";
            pesan = pesan + "Tahun Pembangunan: "+fema.get_tahun_pembangunan()+"\n";
            pesan = pesan + "Kegunaan: "+fema.get_kegunaan()+"\n";
            pesan = pesan + "Kepemilikan: "+fema.get_kepemilikan()+"\n";
            pesan = pesan + "Jumlah pengguna: "+fema.get_jumlah_pengguna()+"\n";
            pesan = pesan + "Jenis tanah: "+fema.get_jenis_tanah()+"\n";
            pesan = pesan + "Jenis bangunan: "+fema.get_jenis_bangunan()+"\n";
            pesan = pesan + "Vertikal Bangunan: "+fema.get_vertikal_bangunan()+"\n";
            pesan = pesan + "Denah Bangunan: "+fema.get_denah_bangunan()+"\n";
            pesan = pesan + "Pembuatan Berdasarkan: "+fema.get_pembuatan_berdasarkan()+"\n";
            pesan = pesan + "Berbahaya jatuh: "+fema.get_berbahaya_jatuh()+"\n";
            pesan = pesan + "lainya: "+fema.get_text_berbahaya_jatuh()+"\n";

            folder = fema.get_folder();
            GridViewDemo_ImagePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)+"/"+folder+"/";
            reloadImage();

            ed_nilai.setText(pesan);

        }catch (NullPointerException e){
            Toast.makeText(fragmentActivity, ""+e, Toast.LENGTH_SHORT).show();
        }

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ImageItem item = arrayImageItem.get(position);

                Dialog dialog = new Dialog(fragmentActivity);
                dialog.setContentView(R.layout.detail_image_activity);
                dialog.setTitle("Detail gambar");
                ImageView image = (ImageView) dialog.findViewById(R.id.image);
                TextView title = (TextView) dialog.findViewById(R.id.title);
                title.setText(item.getTitle());
                image.setImageBitmap(item.getImage());
                int width = (int)(getResources().getDisplayMetrics().widthPixels*0.90);
                int height = (int)(getResources().getDisplayMetrics().heightPixels*0.90);
                dialog.getWindow().setLayout(width,height);
                dialog.show();
            }
        });
        return view;
    }
    public void reloadImage(){
        RetriveCapturedImagePath();
        gridview.setAdapter(new ImageListAdapter(fragmentActivity ,arrayImageItem));
    }
    private void RetriveCapturedImagePath() {

        arrayImageItem = new ArrayList<>();
        File f = new File(GridViewDemo_ImagePath);
        if (f.exists()) {
            File[] files=f.listFiles();
            Arrays.sort(files);

            for(int i=0; i<files.length; i++){
                File file = files[i];
                if(file.isDirectory())
                    continue;
                Bitmap bm = BitmapFactory.decodeFile(new File(file.getPath()).getAbsolutePath());
                ImageItem imageItem = new ImageItem(bm,file.getName());
                arrayImageItem.add(imageItem);
            }
        }
    }
}
