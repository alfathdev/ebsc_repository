package com.alfathindo.ebsc.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.alfathindo.ebsc.R;
import com.alfathindo.ebsc.database.FemaRepo;
import com.alfathindo.ebsc.entity.Fema;
import com.alfathindo.ebsc.model.ToggleButtonGroupTableLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by safeimuslim on 3/1/16.
 */
public class FormulirFemaDuaFragment extends Fragment{
    View view;
    Fema fema;
    Spinner spinnerJenisBangunan,spinnerBentukBangunan;
    private String id_fema,kepemilikan,jumlah_pengguna,jenis_tanah,berbahaya_jatuh,jenis_bangunan,bentuk_bangunan;
    EditText text_bahaya_d;
    FragmentActivity fragmentActivity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        fragmentActivity = (FragmentActivity) activity;

    }

    public FormulirFemaDuaFragment(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.formulir_fema_dua,container, false);
        text_bahaya_d = (EditText) view.findViewById(R.id.text_bahaya_d);
        spinnerJenisBangunan = (Spinner) view.findViewById(R.id.spinnerJenisBangunan);
        spinnerBentukBangunan = (Spinner) view.findViewById(R.id.spinnerBentukBangunan);

        setSpinnerJenisBangunan(view);
        setSpinnerBentukBangunan(view);

        try{
            if(getArguments().getString("id_fema") != null){
                id_fema = getArguments().getString("id_fema");
                FemaRepo femaRepo = new FemaRepo(fragmentActivity);
                fema = femaRepo.getFemaById(id_fema);
                setRadKepemilikan();
                setRadJumlahPengguna();
                setRadGroupJenisTanah();
                setRadGroupBerbahayaJatuh();

                spinnerJenisBangunan.setSelection(getIndex(spinnerJenisBangunan, fema.get_jenis_bangunan()));
                spinnerBentukBangunan.setSelection(getIndex(spinnerBentukBangunan,fema.get_bentuk_bangunan()));

            }
        }catch (NullPointerException e){

        }
        return view;
    }

    private int getIndex(Spinner spinner, String myString){

        int index = 0;

        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).equals(myString)){
                index = i;
            }
        }
        return index;
    }

    public void setRadKepemilikan(){
        //kepemilikan
        ToggleButtonGroupTableLayout radGroupKepemilikan = (ToggleButtonGroupTableLayout) view.findViewById(R.id.radGroupKepemilikan);


        if(fema.get_kepemilikan().equals("Pertemuan")){
            ((RadioButton) view.findViewById(R.id.rdPertemuan)).setChecked(true);
            //radGroupKepemilikan.check(((RadioButton) view.findViewById(R.id.rdPertemuan)).getId());
        }
        if(fema.get_kepemilikan().equals("Industri")){
            ((RadioButton) view.findViewById(R.id.rdIndustri)).setChecked(true);
            //radGroupKepemilikan.check(((RadioButton) view.findViewById(R.id.rdIndustri)).getId());
        }
        if(fema.get_kepemilikan().equals("Perniagaan")){
            ((RadioButton) view.findViewById(R.id.rdPerniagaan)).setChecked(true);
            //radGroupKepemilikan.check(((RadioButton) view.findViewById(R.id.rdPerniagaan)).getId());
        }
        if(fema.get_kepemilikan().equals("Kantor")){
            ((RadioButton) view.findViewById(R.id.rdKantor)).setChecked(true);
            //radGroupKepemilikan.check(((RadioButton) view.findViewById(R.id.rdKantor)).getId());
        }
        if(fema.get_kepemilikan().equals("Gawat Darurat")){
            ((RadioButton) view.findViewById(R.id.rdGawatDarurat)).setChecked(true);
            //radGroupKepemilikan.check(((RadioButton) view.findViewById(R.id.rdGawatDarurat)).getId());
        }
        if(fema.get_kepemilikan().equals("Perumahan")){
            ((RadioButton) view.findViewById(R.id.rdPerumahan)).setChecked(true);
            //radGroupKepemilikan.check(((RadioButton) view.findViewById(R.id.rdPerumahan)).getId());
        }
        if(fema.get_kepemilikan().equals("Pemerintahan")){
            ((RadioButton) view.findViewById(R.id.rdPertemuan)).setChecked(true);
            //radGroupKepemilikan.check(((RadioButton) view.findViewById(R.id.rdPemerintahan)).getId());
        }
        if(fema.get_kepemilikan().equals("Sekolah")){
            ((RadioButton) view.findViewById(R.id.rdSekolah)).setChecked(true);
            //radGroupKepemilikan.check(((RadioButton) view.findViewById(R.id.rdSekolah)).getId());
        }
        if(fema.get_kepemilikan().equals("Sejarah")){
            ((RadioButton) view.findViewById(R.id.rdSejarah)).setChecked(true);
            //radGroupKepemilikan.check(((RadioButton) view.findViewById(R.id.rdSejarah)).getId());
        }
    }

    public void setRadJumlahPengguna(){
        if(fema.get_jumlah_pengguna().equals("0 - 10")){
            ((RadioButton) view.findViewById(R.id.sangat_sedikit)).setChecked(true);
        }
        if(fema.get_jumlah_pengguna().equals("11 - 100")){
            ((RadioButton) view.findViewById(R.id.sedikit)).setChecked(true);
        }
        if(fema.get_jumlah_pengguna().equals("101 - 1000")){
            ((RadioButton) view.findViewById(R.id.sedang)).setChecked(true);
        }
        if(fema.get_jumlah_pengguna().equals(">1000")){
            ((RadioButton) view.findViewById(R.id.banyak)).setChecked(true);
        }
    }


    public void setRadGroupJenisTanah(){
        if(fema.get_jenis_tanah().equals("A - Batuan keras")){
            ((RadioButton) view.findViewById(R.id.tanah_a)).setChecked(true);
        }
        if(fema.get_jenis_tanah().equals("B - Batuan sedang")){
            ((RadioButton) view.findViewById(R.id.tanah_b)).setChecked(true);
        }
        if(fema.get_jenis_tanah().equals("C - Tanah keras")){
            ((RadioButton) view.findViewById(R.id.tanah_c)).setChecked(true);
        }
        if(fema.get_jenis_tanah().equals("D - Tanah kaku")){
            ((RadioButton) view.findViewById(R.id.tanah_d)).setChecked(true);
        }
        if(fema.get_jenis_tanah().equals("E - Tanah lunak")){
            ((RadioButton) view.findViewById(R.id.tanah_e)).setChecked(true);
        }
        if(fema.get_jenis_tanah().equals("F - Tanah buruh")){
            ((RadioButton) view.findViewById(R.id.tanah_f)).setChecked(true);
        }

    }

    public void setRadGroupBerbahayaJatuh(){
        if(fema.get_berbahaya_jatuh().equals("Unreinforced chimneis")){
            ((RadioButton) view.findViewById(R.id.bahaya_a)).setChecked(true);
        }
        if(fema.get_berbahaya_jatuh().equals("Parapets")){
            ((RadioButton) view.findViewById(R.id.bahaya_b)).setChecked(true);
        }
        if(fema.get_berbahaya_jatuh().equals("Hevy clading")){
            ((RadioButton) view.findViewById(R.id.bahaya_c)).setChecked(true);
        }
        if(fema.get_berbahaya_jatuh().equals("Others")){
            ((RadioButton) view.findViewById(R.id.bahaya_d)).setChecked(true);
            text_bahaya_d.setText(fema.get_text_berbahaya_jatuh());
        }
    }



    public void setSpinnerJenisBangunan(View view){

        List<String> itemSpinnerJenisBangunan = new ArrayList<String>();
        itemSpinnerJenisBangunan.add("Pilih jenis bangunan");
        itemSpinnerJenisBangunan.add("W1");
        itemSpinnerJenisBangunan.add("W2");
        itemSpinnerJenisBangunan.add("S1");
        itemSpinnerJenisBangunan.add("S2");
        itemSpinnerJenisBangunan.add("S3");
        itemSpinnerJenisBangunan.add("S4");
        itemSpinnerJenisBangunan.add("S5");
        itemSpinnerJenisBangunan.add("C1");
        itemSpinnerJenisBangunan.add("C2");
        itemSpinnerJenisBangunan.add("C3");
        itemSpinnerJenisBangunan.add("PC1");
        itemSpinnerJenisBangunan.add("PC2");
        itemSpinnerJenisBangunan.add("RM1");
        itemSpinnerJenisBangunan.add("RM2");
        itemSpinnerJenisBangunan.add("URM");

        ArrayAdapter<String> itemSpinnerJenisBangunanAdapter = new ArrayAdapter<String>(fragmentActivity, android.R.layout.simple_spinner_item, itemSpinnerJenisBangunan);
        itemSpinnerJenisBangunanAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spinnerJenisBangunan.setAdapter(itemSpinnerJenisBangunanAdapter);
    }

    public void setSpinnerBentukBangunan(View view){

        List<String> itemSpinnerBentukBangunan = new ArrayList<String>();
        itemSpinnerBentukBangunan.add("Pilih Bentuk bangunan");
        itemSpinnerBentukBangunan.add("Simetris");
        itemSpinnerBentukBangunan.add("Tidak Simetris");

        ArrayAdapter<String> itemSpinnerBentukBangunanAdapter = new ArrayAdapter<String>(fragmentActivity,android.R.layout.simple_spinner_item, itemSpinnerBentukBangunan);
        itemSpinnerBentukBangunanAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spinnerBentukBangunan.setAdapter(itemSpinnerBentukBangunanAdapter);


    }

}
