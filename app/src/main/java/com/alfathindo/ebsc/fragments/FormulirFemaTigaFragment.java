package com.alfathindo.ebsc.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore.MediaColumns;
import android.widget.ImageView;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import com.alfathindo.ebsc.R;
import com.alfathindo.ebsc.activity.DrawImageActivity;
import com.alfathindo.ebsc.database.FemaRepo;
import com.alfathindo.ebsc.entity.Fema;

/**
 * Created by safeimuslim on 3/1/16.
 */
public class FormulirFemaTigaFragment extends Fragment {

    int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    final int CAMERA_CAPTURE = 0;
    String id_fema;
    Fema fema;
    protected FragmentActivity fragmentActivity;
    EditText ed_folder;
    String folder = "EBSC_"+currentDateFormat();
    Button btnTakePhoto;
    RelativeLayout relativeLayout;
    GridView gridView;
    private  List<String> listOfImagesPath;
    private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    //public static final String GridViewDemo_ImagePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/GridViewDemo/";
    //File folder = new File(Environment.getExternalStorageDirectory() + "/sdcard/FemaFolder");
    public String GridViewDemo_ImagePath;
    //File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "EbscImageFolder");
    int imageNum = 0;

    /**
     * Cursor used to access the results from querying for images on the SD card.
     */
    private Cursor cursor;
    /*
     * Column index for the Thumbnails Image IDs.
     */
    private int columnIndex;

    public FormulirFemaTigaFragment(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        fragmentActivity = (FragmentActivity) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.formulir_fema_tiga,container,false);
        //relativeLayout = (RelativeLayout) view.findViewById(R.id.myLayout);

        //ivImage = (ImageView) view.findViewById(R.id.ivImage);
        try{
            if(getArguments().getString("id_fema") != null){
                id_fema = getArguments().getString("id_fema");
                FemaRepo femaRepo = new FemaRepo(fragmentActivity);
                fema = femaRepo.getFemaById(id_fema);
                folder = fema.get_folder();

            }
        }catch (NullPointerException e){

        }

        GridViewDemo_ImagePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)+"/"+folder+"/";

        gridView = (GridView) view.findViewById(R.id.gridview);
        ed_folder = (EditText) view.findViewById(R.id.ed_folder);
        ed_folder.setText(folder);


        listOfImagesPath = null;
        listOfImagesPath = RetriveCapturedImagePath();
        if(listOfImagesPath!=null){
     //       gridView.setAdapter(new ImageListAdapter(fragmentActivity,listOfImagesPath));
        }
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                // Get the data location of the image

                cursor = fragmentActivity.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        null, // Which columns to return
                        MediaStore.Images.Media.DATA + " like ? ",       // Return all rows
                        new String[] {"%"+folder+"%"},
                        null);

                columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if(cursor.moveToPosition(position)){
                    // Get image filename
                    String imagePath = cursor.getString(columnIndex);
                    zoomImage(imagePath);

                }else{
                    Toast.makeText(fragmentActivity,"Gambar tidak ditemukan!",Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnTakePhoto = (Button) view.findViewById(R.id.btnTakePhoto);
        btnTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(fragmentActivity,"btnTakePhoto di klik",Toast.LENGTH_SHORT).show();
                selectImage();
                //addImage(view);

            }
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Toast.makeText(fragmentActivity,"onActivityResult",Toast.LENGTH_SHORT).show();
        if (resultCode == Activity.RESULT_OK) {
            Toast.makeText(fragmentActivity,"RESULT_OK",Toast.LENGTH_SHORT).show();
            //if (requestCode == REQUEST_CAMERA) {
            if (requestCode == CAMERA_CAPTURE) {
                Toast.makeText(fragmentActivity,"CAMERA_CAPTURE",Toast.LENGTH_SHORT).show();
                if(isDeviceSupportCamera()) {
                    Bundle extras = data.getExtras();
                    Bitmap thePic = extras.getParcelable("data");
                    String imgcurTime = dateFormat.format(new Date());
                    File imageDirectory = new File(GridViewDemo_ImagePath);

                    if (!imageDirectory.exists()) {
                        if (imageDirectory.mkdirs()) {
                            Log.i("CREATE DIRECTORY", " SUKSES");
                        } else {
                            Log.i("CREATE DIRECTORY", " GAGAL");
                        }
                    } else {
                        Log.i("CREATE DIRECTORY FEMA", " FOLDER SUDAH ADA");
                    }

                    String _path = GridViewDemo_ImagePath + "image_" + currentDateFormat() + ".jpg";
                    try {
                        FileOutputStream out = new FileOutputStream(_path);
                        BufferedOutputStream bof = new BufferedOutputStream(out);
                        thePic.compress(Bitmap.CompressFormat.JPEG, 90, bof);
                        //bof.flush();
                        //bof.close();
                        Log.i("SAVE IMAGE", "BERHASIL");
                    } catch (FileNotFoundException e) {
                        e.getMessage();
                    }

                    listOfImagesPath = null;
                    listOfImagesPath = RetriveCapturedImagePath();
                    if (listOfImagesPath != null) {
                 //       gridView.setAdapter(new ImageListAdapter(fragmentActivity, listOfImagesPath));
                    }

                }else{
                    Toast.makeText(fragmentActivity,"Device Tidak Mendukung Kamera!",Toast.LENGTH_SHORT).show();
                }
            } else if (requestCode == SELECT_FILE && data != null) {

                Toast.makeText(fragmentActivity,"_Path: ",Toast.LENGTH_SHORT).show();

                Uri selectedImageUri = data.getData();
                String[] projection = { MediaColumns.DATA };
                CursorLoader cursorLoader = new CursorLoader(fragmentActivity,selectedImageUri, projection, null, null,
                        null);
                Cursor cursor =cursorLoader.loadInBackground();
                int column_index = cursor.getColumnIndexOrThrow(MediaColumns.DATA);
                cursor.moveToFirst();


                String selectedImagePath = cursor.getString(column_index);
                Toast.makeText(fragmentActivity,"_Path: "+selectedImagePath,Toast.LENGTH_SHORT).show();

                String outputPath = GridViewDemo_ImagePath + "image_" + currentDateFormat() + ".jpg";
                InputStream in = null;
                OutputStream out = null;

                try {
                    in = new FileInputStream(selectedImagePath);
                    out = new FileOutputStream(outputPath);
                    byte[] buffer = new byte[1024];
                    int read;
                    while ((read = in.read(buffer)) != -1) {
                        out.write(buffer, 0, read);
                    }
                    in.close();
                    in = null;

                    // write the output file (You have now copied the file)
                    out.flush();
                    out.close();
                    out = null;
                }  catch (FileNotFoundException fnfe1) {
                    Log.e("tag", fnfe1.getMessage());
                }
                catch (Exception e) {
                    Log.e("tag", e.getMessage());
                }
                cursor.close();
                listOfImagesPath = null;
                listOfImagesPath = RetriveCapturedImagePath();
                if (listOfImagesPath != null) {
                //    gridView.setAdapter(new ImageListAdapter(fragmentActivity, listOfImagesPath));
                }
            }
        }
    }


    private String currentDateFormat(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HH_mm_ss");
        String  currentTimeStamp = dateFormat.format(new Date());
        return currentTimeStamp;
    }

    private boolean isDeviceSupportCamera(){
        if(getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            return true;
        }else {
            return false;
        }

    }

    public void zoomImage(String _path){
        final File imgFile = new File(_path);

        Dialog dialog = new Dialog(fragmentActivity);
        //dialog.setContentView(R.layout.zoom_image_layout);
        dialog.setTitle("Detail Gambar");
        ImageView imageView = (ImageView) dialog.findViewById(R.id.image);

        if(imgFile.exists()){
            Bitmap bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            imageView.setImageBitmap(bitmap);

        }
        dialog.show();
    }

    private void selectImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Library", "Draw Image","Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(fragmentActivity);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {

                    try {
                        //use standard intent to capture an image
                        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        //we will handle the returned data in onActivityResult
                        startActivityForResult(captureIntent, CAMERA_CAPTURE);
                    } catch(ActivityNotFoundException anfe){
                        //display an error message
                        String errorMessage = "Whoops - your device doesn't support capturing images!";
                        Toast toast = Toast.makeText(fragmentActivity, errorMessage, Toast.LENGTH_SHORT);
                        toast.show();
                    }

                } else if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            SELECT_FILE);
                } else if(items[item].equals("Sketsa Denah atau Elevasi")){
                    Intent intent = new Intent(getActivity(), DrawImageActivity.class);
                    startActivity(intent);

                }else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private List<String> RetriveCapturedImagePath() {
        List<String> tFileList = new ArrayList<String>();
        File f = new File(GridViewDemo_ImagePath);
        if (f.exists()) {
            File[] files=f.listFiles();
            Arrays.sort(files);

            for(int i=0; i<files.length; i++){
                File file = files[i];
                if(file.isDirectory())
                    continue;
                tFileList.add(file.getPath());
            }
        }
        return tFileList;
    }
}
