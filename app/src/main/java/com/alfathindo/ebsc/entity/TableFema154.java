package com.alfathindo.ebsc.entity;

/**
 * Created by safeimuslim on 3/9/16.
 */
public class TableFema154 {

    public static final String bulding_types [] = {"W1","W2","S1","S2","S3","S4","S5","C1","C2","C3","PC1","PC2","RM1","RM2","URM"};
    public static final String building_types_as [] = {"","","(MRF)","(BR)","(LM)","(RC SW)","(URM INF)","(MRF)","(SW)","(URM INF)","(TU)","","(FD)","(RD)",""};
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    //RENDAH (LOW)
    public static final String low_basic_score [] = {"7.4", "6.0", "4.6" ,"4.8", "4.6", "4.8", "5.0", "4.4", "4.8", "4.4", "4.4", "4.6", "4.8", "4.6", "4.6"};
    public static final String low_mid_rise []  = {"N/A","N/A","0.2","0.4","N/A","0.2","-0.2","0.4","-0.2","-0.4","N/A","-0.2","-0.4","-0.2","-0.6"};
    public static final String low_high_rise [] = {"N/A","N/A","1.0","1.0","N/A","1.0","1.2","1.0","0.0","-0.4","N/A","-0.2","N/A","0.0","N/A"};
    public static final String low_vertical_irregularity[] = {"-4.0","-3.0","-2.0","-2.0","N/A","-2.0","-2.0","-1.5","-2.0","-2.0","N/A","-1.5","-2.0","-1.5","-1.5"};
    public static final String low_plan_irregularity[] = {"-0.8","-0.8","-0.8","-0.8","-0.8","-0.8","-0.8","-0.8","-0.8","-0.8","-0.8","-0.8","-0.8","-0.8","-0.8"};
    public static final String low_pre_code[] = {"N/A","N/A","N/A","N/A","N/A","N/A","N/A","N/A","N/A","N/A","N/A","N/A","N/A","N/A","N/A"};
    public static final String low_post_bencmark[] = {"0.0","0.2","0.4","0.6","N/A","0.6","N/A","0.6","0.4","N/A","0.2","N/A","0.2","0.4","0.4"};
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    public static final String low_soil_type_c[] = {"-0.4","-0.4","-0.8","-0.4","-0.4","-0.4","-0.4","-0.6","-0.4","-0.4","-0.4","-0.2","-0.4","-0.2","-0.4"};
    public static final String low_soil_type_d[] = {"-1.0","-0.8","-1.4","-1.2","-1.0","-1.4","-0.8","-1.4","-0.8","-0.8","-0.8","-1.0","-0.8","-0.8","-0.8"};
    public static final String low_soil_type_e[] = {"-1.8","-2.0","-2.0","-2.0","-2.0","-2.2","-2.0","-2.0","-2.0","-2.0","-1.8","-2.0","-1.4","-1.6","-1.4"};


    //SEDANG (MODERATE)
    public static final String moderate_basic_score [] = {"5.2", "4.8", "3.6" ,"3.6", "3.8", "3.6", "3.6", "3.0", "3.6", "3.2", "3.2", "3.2", "3.6", "3.4", "3.4"};
    public static final String moderate_mid_rise []  = {"N/A","N/A","0.4","0.4","N/A","0.4","0.4","0.2","0.4","0.2","N/A","0.4","0.4","0.4","-0.4"};
    public static final String moderate_high_rise [] = {"N/A","N/A","1.4","1.4","N/A","1.4","0.8","0.5","0.8","0.4","N/A","0.6","N/A","0.6","N/A"};
    public static final String moderate_vertical_irregularity[] = {"-3.5","-3.0","-2.0","-2.0","N/A","-2.0","-2.0","-2.0","-2.0","-2.0","N/A","-1.5","-2.0","-1.5","-1.5"};
    public static final String moderate_plan_irregularity[] = {"-0.5","-0.5","-0.5","-0.5","-0.5","-0.5","-0.5","-0.5","-0.5","-0.5","-0.5","-0.5","-0.5","-0.5","-0.5"};
    public static final String moderate_pre_code[] = {"0.0","-0.2","-0.4","-0.4","-0.4","-0.4","-0.2","-1.0","-0.4","-1.0","-0.2","-0.4","-0.4","-0.4","-0.4"};
    public static final String moderate_post_bencmark[] = {"1.6","1.6","1.4","1.4","N/A","1.2","N/A","1.2","1.6","N/A","1.8","N/A","2.0","1.8","N/A"};
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    public static final String moderate_soil_type_c[] = {"-0.2","-0.8","-0.6","-0.8","-0.6","-0.8","-0.8","-0.6","-0.8","-0.6","-0.6","-0.6","-0.8","-0.6","-0.4"};
    public static final String moderate_soil_type_d[] = {"-0.6","-1.2","-1.0","-1.2","-1.0","-1.2","-1.2","-1.0","-1.2","-1.0","-1.0","-1.2","-1.2","-1.2","-0.8"};
    public static final String moderate_soil_type_e[] = {"-1.2","-1.8","-1.6","-1.6","-1.6","-1.6","-1.6","-1.6","-1.6","-1.6","-1.6","-1.6","-1.6","-1.6","-1.6"};


    //TINGGI (HIGH)
    public static final String high_basic_score [] = {"4.4", "3.8", "2.8" ,"3.0", "3.2", "2.8", "2.0", "2.5", "2.8", "1.6", "2.6", "2.4", "2.8", "2.8", "1.8"};
    public static String high_mid_rise []  = {"N/A","N/A","0.2","0.4","N/A","0.4","0.4","0.4","0.4","0.2","N/A","0.2","0.4","0.4","0.0"};
    public static final String high_high_rise [] = {"N/A","N/A","0.6","0.8","N/A","0.8","0.8","0.6","0.8","0.3","N/A","0.4","N/A","0.6","N/A"};
    public static final String high_vertical_irregularity[] = {"-2.5","-2.0","-1.0","-1.5","N/A","-1.0","-1.0","-1.5","-1.0","-1.0","N/A","-1.0","-1.0","-1.0","-1.0"};
    public static final String high_plan_irregularity[] = {"-0.5","-0.5","-0.5","-0.5","-0.5","-0.5","-0.5","-0.5","-0.5","-0.5","-0.5","-0.5","-0.5","-0.5","-0.5"};
    public static final String high_pre_code[] = {"0.0","-1.0","-1.0","-0.8","-0.6","-0.8","-0.2","-1.2","-1.0","-0.2","-0.8","-0.8","-1.0","-0.8","-0.2"};
    public static final String high_post_bencmark[] = {"2.4","2.4","1.4","1.4","N/A","1.6","N/A","1.4","2.4","N/A","2.4","N/A","2.8","2.6","N/A"};
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    public static final String high_soil_type_c[] = {"0.0","-0.4","-0.4","-0.4","-0.4","-0.4","-0.4","-0.4","-0.4","-0.4","-0.4","-0.4","-0.4","-0.4","-0.4"};
    public static final String high_soil_type_d[] = {"0.0","-0.8","-0.6","-0.6","-0.6","-0.6","-0.4","-0.6","-0.6","-0.4","-0.6","-0.6","-0.6","-0.6","-0.6"};
    public static final String high_soil_type_e[] = {"0.0","-0.8","-1.2","-1.2","-1.0","-1.2","-0.8","-1.2","-0.8","-0.8","-0.4","-1.2","-0.4","-0.6","-0.8"};

}
