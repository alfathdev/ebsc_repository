package com.alfathindo.ebsc.entity;

/**
 * Created by safeimuslim on 3/5/16.
 */
public class NilaiSeismicityRegion {
    public String[] variabel = {
    "PGA (g)","SS (g)","S1 (g)","CRS", "CR1", "FPGA","FA","FV","PSA (g)","SMS (g)","SM1 (g)","SDS (g)","SD1 (g)","T0 (detik)","TS (detik)",
    "PGA (g)","SS (g)","S1 (g)","CRS ","CR1","FPGA","FA","FV","PSA (g)","SMS (g)","SM1 (g)","SDS (g)","SD1 (g)","T0 (detik)","TS (detik)",
    "PGA (g)", "SS (g)","S1 (g)","CRS","CR1","FPGA","FA","FV","PSA (g)","SMS (g)","SM1 (g)","SDS (g)","SD1 (g)","T0 (detik)","TS (detik)",
    "PGA (g)","SS (g)","S1 (g)","CRS","CR1","FPGA","FA","FV","PSA (g)","SMS (g)","SM1 (g)","SDS (g)","SD1 (g)","T0 (detik)","TS (detik)"
    };

    public String[] bali = {
    "0.452","0.983","0.346","1.047","0.942","1", "1", "1", " 0.452", "0.983", "0.346", "0.655", "0.231", "0.07", "0.352",
    "0.452", "0.983", "0.346", "1.047", "0.942", "1", "1.007", "1.454", "0.452", "0.989", "0.503", "0.66", "0.336", "0.102", "0.509",
    "0.452", "0.983", "0.346", "1.047", "0.942", "1.048", "1.107", "1.708", "0.474", "1.088", "0.591", "0.725", "0.394", "0.109", "0.544",
    "0.452", "0.983", "0.346", "1.047", "0.942", "0.9", "0.921", "2.615", "0.407", "0.905", "0.905", "0.603", "0.604", "0.2", "1.001"};

    public String[] banda_aceh = {"0.621","1.349","0.642","0.978","0.947","1", "1", "1", "0.621", "1.349", "0.642", "0.9", "0.428", "0.095", "0.476", "0.621", "1.349", "0.642", "0.978", "0.947", "1", "1", "1.3", "0.621", "1.349", "0.835", "0.9", "0.556", "0.124", "0.619", "0.621", "1.349", "0.642", "0.978", "0.947", "1", "1", "1.5", "0.621", "1.349", "0.963", "0.9", "0.642", "0.143", "0.714", "0.621", "1.349", "0.642", "0.978", "0.947", "0.9", "0.9", "2.4", "0.559", "1.214", "1.541", "0.81", "1.027", "0.254", "1.269"};
    public String[] bangka_belitung = {"0.029","0.063","0.078","0.983","0.929","1", "1", "1", "0.029", "0.063", "0.078", "0.042", "0.052", "0.246", "1.229", "0.029", "0.063", "0.078", "0.983", "0.929", "1.2", "1.2", "1.7", "0.035", "0.076", "0.133", "0.051", "0.088", "0.348", "1.741", "0.029", "0.063", "0.078", "0.983", "0.929", "1.6", "1.6", "2,4", "0.047", "0.102", "0.187", "0.068", "0.125", "0.369", "1.844", "0.029", "0.063", "0.078", "0.983", "0.929", "2.5", "2.5", "3.5", "0.074", "0.159", "0.273", "0.106", "0.182", "0.344", "1.721"};
    public String[] banten = {"0.46","0.942","0.383","1.016","0.929","1", "1", "1", "0.46", "0.942", "0.383", "0.628", "0.255", "0.081", "0.406", "0.46", "0.942", "0.383", "1.016", "0.929", "1", "1.023", "1.417", "0.46", "0.964", "0.542", "0.643", "0.362", "0.113", "0.563", "0.46", "0.942", "0.383", "1.016", "0.929", "1.04", "1.123", "1.635", "0.479", "1.058", "0.625", "0.705", "0.417", "0.118", "0.591", "0.46", "0.942", "0.383", "1.106", "0.929", "0.9", "0.97", "2.47", "0.414", "0.913", "0.945", "0.609", "0.63", "0.207", "1.034"};
    public String[] bengkulu = {"0.442","1.128","0.508","1.02","0.958","1", "1", "1", "0.442", "1.128", "0.508", "0.752", "0.339", "0.09", "0.45", "0.442", "1.128", "0.508", "1.02", "0.958", "1", "1", "1.3", "0.442", "1.128", "0.661", "0.752", "0.44", "0.117", "0.585", "0.442", "1.128", "0.508", "1.02", "0.958", "1.058", "1.049", "1.5", "0.468", "1.183", "0.762", "0.789", "0.508", "0.129", "0.644", "0.442", "1.128", "0.508", "1.02", "0.958", "0.9", "0.9", "2.4", "0.398", "1.015", "1.22", "0.677", "0.813", "0.24", "1.201"};
    public String[] gorontalo = {"0.707","1.759","0.692","0.948","1.1","1", "1", "1", "0.707", "1.759", "0.692", "1.172", "0.461", "0.079", "0.393", "0.707", "1.759", "0.692", "0.948", "1.1", "1", "1", "1.3", "0.707", "1.759", "0.899", "1.172", "0.6", "0.102", "0.511", "0.707", "1.759", "0.692", "0.948", "1.1", "1", "1", "1.5", "0.707", "1.759", "1.038", "1.172", "0.692", "0.118", "0.59", "0.707", "1.759", "0.692", "0.948", "1.1", "0.9", "0.9", "2.4", "0.636", "1.583", "1.661", "1.055", "1.107", "0.21", "1.049"};
    public String[] jakarta = {"0.361","0.686","0.3","0.995","0.939","1", "1", "1", "0.361", "0.686", "0.3", "0.457", "0.2", "0.087", "0.437", "0.361", "0.686", "0.3", "0.995", "0.939", "1.039", "1.126", "1.5", "0.375", "0.772", "0.45", "0.515", "0.3", "0.116", "0.582", "0.361", "0.686", "0.3", "0.995", "0.939", "1.139", "1.252", "1.801", "0.411", "0.858", "0.54", "0.572", "0.36", "0.126", "0.629", "0.361", "0.686", "0.3", "0.995", "0.939", "1.018", "1.329", "2.802", "0.367", "0.911", "0.839", "0.607", "0.56", "0.184", "0.921"};
    public String[] jambi = {"0.105","0.211","0.17","0.954","0.951","1", "1", "1", "0.105", "0.211", "0.17", "0.141", "0.114", "0.161", "0.806", "0.105", "0.211", "0.17", "0.954", "0.951", "1.2", "1.2", "1.63", "0.126", "0.254", "0.278", "0.169", "0.185", "0.219", "1.095", "0.105", "0.211", "0.17", "0.954", "0.951", "1.59", "1.6", "2.118", "0.167", "0.338", "0.361", "0.226", "0.241", "0.213", "1.067", "0.105", "0.211", "0.17", "0.954", "0.951", "2.46", "2.5", "3.289", "0.258", "0.529", "0.561", "0.352", "0.374", "0.212", "1.06"};
    public String[] jawa_barat = {"0.45","1.005","0.386","1.036","0.905","1", "1", "1", "0.45", "1.005", "0.386", "0.67", "0.257", "0.077", "0.384", "0.45", "1.005", "0.386", "1.036", "0.905", "1", "1", "1.414", "0.45", "1.005", "0.545", "0.67", "0.364", "0.109", "0.543", "0.45", "1.005", "0.386", "1.036", "0.905", "1.05", "1.098", "1.629", "0.472", "1.104", "0.628", "0.736", "0.419", "0.114", "0.569", "0.45", "1.005", "0.386", "1.036", "0.905", "0.9", "0.9", "2.457", "0.405", "0.905", "0.948", "0.603", "0.632", "0.21", "1.048"};
    public String[] jawa_tengah = {"0.31","0.655","0.265","1.003","0","1", "1", "1", "0.31", "0.655", "0.265", "0.436", "0.177", "0.081", "0.405", "0.31", "0.655", "0.265", "1.003", "0", "1.09", "1.138", "1.535", "0.338", "0.745", "0.407", "0.497", "0.271", "0.109", "0.546", "0.31", "0.655", "0.265", "1.003", "0", "1.19", "1.276", "1.869", "0.369", "0.836", "0.496", "0.557", "0.331", "0.119", "0.594", "0.31", "0.655", "0.265", "1.003", "0", "1.17", "1.391", "2.939", "0.363", "0.91", "0.78", "0.607", "0.52", "0.171", "1.856"};
    public String[] jawa_timur = {"0.361","0.725","0.293","1.004","0.945","1", "1", "1", "0.361", "0.725", "0.293", "0.483", "0.195", "0.081", "0.404", "0.361", "0.725", "0.293", "1.004", "0.945", "1.039", "1.11", "1.507", "0.375", "0.805", "0.441", "0.536", "0.294", "0.11", "0.548", "0.361", "0.725", "0.293", "1.004", "0.945", "1.139", "1.22", "1.814", "0.411", "0.884", "0.531", "0.59", "0.354", "0.12", "0.601", "0.361", "0.725", "0.293", "1.004", "0.945", "1.018", "1.25", "2.829", "0.367", "0.906", "0.828", "0.604", "0.552", "0.183", "0.914"};
    public String[] kalimantan_barat = {"0.005","0.012","0.013","0.917","0.949","1", "1", "1", "0.005", "0.012", "0.013", "0.008", "0.009", "0.231", "1.156", "0.005", "0.012", "0.013", "0.917", "0.949", "1.2", "1.2", "1.7", "0.006", "0.014", "0.023", "0.009", "0.015", "0.327", "1.637", "0.005", "0.012", "0.013", "0.917", "0.949", "1.6", "1.6", "2.4", "0.008", "0.019", "0.032", "0.012", "0.021", "0.347", "1.734", "0.005", "0.012", "0.013", "0.917", "0.949", "2.5", "2.5", "3.5", "0.012", "0.029", "0.047", "0.019", "0.031", "0.324", "1.618"};
    public String[] kalimantan_selatan = {"0.085","0.149","0.056","0.88","0.931","1", "1", "1", "0.085", "0.149", "0.056", "0.099", "0.038", "0.076", "0.379", "0.085", "0.149", "0.056", "0.88", "0.931", "1.2", "1.2", "1.7", "0.102", "0.178", "0.096", "0.119", "0.064", "0.108", "0.538", "0.085", "0.149", "0.056", "0.88", "0.931", "1.6", "1.6", "2.4", "0.136", "0.238", "0.135", "0.159", "0.09", "0.114", "0.569", "0.085", "0.149", "0.056", "0.88", "0.931", "2.5", "2.5", "3.5", "0.213", "0.372", "0.198", "0.248", "0.132", "0.106", "0.531"};
    public String[] kalimantan_tengah = {"0.021","0.042","0.026","0.907","0.97","1", "1", "1", "0.021", "0.042", "0.026", "0.028", "0.017", "0.121", "0.605", "0.021", "0.042", "0.026", "0.907", "0.97", "1.2", "1.2", "1.7", "0.025", "0.051", "0.043", "0.034", "0.029", "0.172", "0.858", "0.021", "0.042", "0.026", "0.907", "0.97", "1.6", "1.6", "2.4", "0.033", "0.067", "0.061", "0.045", "0.041", "0.182", "0.908", "0.021", "0.042", "0.026", "0.907", "0.97", "2.5", "2.5", "3.5", "0.051", "0.105", "0.089", "0.07", "0.06", "0.17", "0.848"};
    public String[] kalimantan_timur = {"0.102","0.209","0.083","0.929","1.009","1", "1", "1", "0.102", "0.209", "0.083", "0.139", "0.055", "0.08", "0.398", "0.102", "0.209", "0.083", "0.929", "1.009", "1.2", "1.2", "1.7", "0.122", "0.251", "0.141", "0.167", "0.094", "0.113", "0.564", "0.102", "0.209", "0.083", "0.929", "1.009", "1.597", "1.6", "2.4", "0.162", "0.334", "0.119", "0.223", "0.133", "0.119", "0.597", "0.102", "0.209", "0.083", "0.929", "1.009", "2.487", "2.5", "3.5", "0.253", "0.522", "0.291", "0.348", "0.194", "0.111", "0.557"};
    public String[] kalimantan_utara = {"0.066","0.136","0.067","0.954","1.01","1", "1", "1", "0.066", "0.136", "0.067", "0.091", "0.045", "0.098", "0.491", "0.066", "0.136", "0.067", "0.954", "1.01", "1.2", "1.2", "1.7", "0.079", "0.164", "0.114", "0.109", "0.076", "0.139", "0.695", "0.066", "0.136", "0.067", "0.954", "1.01", "1.6", "1.6", "2.4", "0.106", "0.218", "0.161", "0.146", "0.107", "0.147", "0.736", "0.066", "0.136", "0.067", "0.954", "1.01", "2.5", "2.5", "3.5", "0.166", "0.341", "0.234", "0.227", "0.156", "0.137", "0.687"};
    public String[] kepulauan_riau = {"0.027","0.061","0.088","0.952","0.938","1", "1", "1", "0.027", "0.061", "0.088", "0.041", "0.059", "0.29", "1.448", "0.027", "0.061", "0.088", "0.952", "0.938", "1.2", "1.2", "1.7", "0.033", "0.073", "0.15", "0.049", "0.1", "0.41", "2.051", "0.027", "0.061", "0.088", "0.952", "0.938", "1.6", "1.6", "2.4", "0.043", "0.097", "0.211", "0.065", "0.141", "0.434", "2.171", "0.027", "0.061", "0.088", "0.952", "0.938", "2.5", "2.5", "3.5", "0.068", "0.152", "0.308", "0.101", "0.205", "0.405", "2.027"};
    public String[] lampung = {"0.356","0.739","0.318","1.022","0.923","1", "1", "1", "0.356", "0.739", "0.318", "0.493", "0.212", "0.086", "0.429", "0.356", "0.739", "0.318", "1.022", "0.923", "1.044", "1.104", "1.482", "0.372", "0.817", "0.471", "0.544", "0.314", "0.115", "0.557", "0.356", "0.739", "0.318", "1.022", "0.923", "1.144", "1.208", "1.765", "0.407", "0.894", "0.56", "0.596", "0.374", "0.125", "0.627", "0.356", "0.739", "0.318", "1.022", "0.923", "1.032", "1.221", "2.73", "0.367", "0.903", "0.867", "0.602", "0.578", "0.192", "0.96"};
    public String[] maluku = {"0.595","1.482","0.448","1.067","1.078","1", "1", "1", "0.595", "1.482", "0.448", "0.988", "0.299", "0.06", "0.302", "0.595", "1.482", "0.448", "1.067", "1.078", "1", "1", "1.352", "0.595", "1.482", "0.606", "0.988", "0.404", "0.082", "0.409", "0.595", "1.482", "0.448", "1.067", "1.078", "1", "1", "1.552", "0.595", "1.482", "0.695", "0.988", "0.464", "0.094", "0.469", "0.595", "1.482", "0.448", "1.067", "1.078", "0.9", "0.9", "2.4", "0.536", "1.334", "1.075", "0.889", "0.717", "0.161", "0.806"};
    public String[] maluku_utara = {"0.547","1.397","0.6","1.095","1.032","1", "1", "1", "0.547", "1.397", "0.6", "0.931", "0.4", "0.086", "0.43", "0.547", "1.397", "0.6", "1.095", "1.032", "1", "1", "1.3", "0.547", "1.397", "0.78", "0.931", "0.52", "0.112", "0.558", "0.547", "1.397", "0.6", "1.095", "1.032", "1", "1", "1.5", "0.547", "1.397", "0.9", "0.931", "0.6", "0.129", "0.644", "0.547", "1.397", "0.6", "1.095", "1.032", "0.9", "0.9", "2.4", "0.492", "1.257", "1.44", "0.838", "0.96", "0.229", "1.146"};
    public String[] nusa_tenggara_barat = {"0.495","1.098","0.428","1.058","0.938","1", "1", "1", "0.495", "1.098", "0.428", "0.732", "0.285", "0.078", "0.389", "0.495", "1.098", "0.428", "1.058", "0.938", "1", "1", "1.372", "0.495", "1.098", "0.587", "0.732", "0.391", "0.107", "0.534", "0.495", "1.098", "0.428", "1.058", "0.938", "1.005", "1.061", "1.572", "0.497", "1.165", "0.673", "0.777", "0.448", "0.115", "0.577", "0.495", "1.098", "0.428", "1.058", "0.938", "0.9", "0.9", "2.4", "0.445", "0.989", "1.027", "0.659", "0.684", "0.208", "1.038"};
    public String[] nusa_tenggara_timur = {"0.438","0.973","0.385","1.064","0.978","1", "1", "1", "0.438", "0.973", "0.385", "0.648", "0.257", "0.079", "0.396", "0.438", "0.973", "0.385", "1.064", "0.978", "1", "1.011", "1.415", "0.438", "0.983", "0.545", "0.656", "0.363", "0.111", "0.554", "0.438", "0.973", "0.385", "1.064", "0.978", "1.062", "1.111", "1.629", "0.465", "1.081", "0.628", "0.72", "0.419", "0.116", "0.581", "0.438", "0.973", "0.385", "1.064", "0.978", "0.9", "0.933", "2.458", "0.394", "0.907", "0.948", "0.605", "0.632", "0.209", "1.044"};
    public String[] papua = {"0.752","1.966","0.615","1.681","1.033","1", "1", "1", "0.752", "1.966", "0.615", "1.311", "0.41", "0.063", "0.313", "0.752", "1.966", "0.615", "1.681", "1.033", "1", "1", "1.3", "0.752", "1.996", "0.779", "1.311", "0.533", "0.081", "0.407", "0.752", "1.966", "0.615", "1.681", "1.033", "1", "1", "1.5", "0.752", "1.966", "0.922", "1.311", "0.615", "0.094", "0.469", "0.752", "1.966", "0.615", "1.681", "1.033", "0.9", "0.9", "2.4", "0.676", "1.77", "1.476", "1.18", "0.984", "0.167", "0.834"};
    public String[] papua_barat = {"0.392","0.966","0.382","1.122","1.055","1","1","1","0.392", "0.966", "0.382", "0.644", "0.255", "0.079", "0.396", "0.392", "0.966", "0.382", "1.122", "1.055", "1.008", "1.014", "1.418", "0.395", "0.979", "0.542", "0.653", "0.361", "0.111", "0.554", "0.392", "0.966", "0.382", "1.122", "1.055", "1.108", "1.114", "1.635", "0.434", "1.076", "0.625", "0.717", "0.417", "0.116", "0.581", "0.392", "0.966", "0.382", "1.122", "1.055", "0.925", "0.941", "2.471", "0.362", "0.909", "0.945", "0.606", "0.63", "0.208", "1.039"};
    public String[] riau = {"0.22","0.437","0.258","0.969","0.953","1","1", "1", "0.22", "0.437", "0.258", "0.291", "0.172", "0.118", "0.59", "0.22", "0.437", "0.258", "0.969", "0.953", "1.18", "1.2", "1.542", "0.259", "0.524", "0.397", "0.35", "0.265", "0.152", "0.758", "0.22", "0.437", "0.258", "0.969", "0.953", "1.361", "1.45", "1.885", "0.299", "0.634", "0.486", "0.423", "0.324", "0.153", "0.766", "0.22", "0.437", "0.258", "0.969", "0.953", "1.602", "1.902", "2.969", "0.352", "0.831", "0.765", "0.554", "0.51", "0.184", "0.921"};
    public String[] sulawesi_barat = {"0.573","1.5","0.593","1.31","1.1","1","1", "1", "0.573", "1.5", "0.593", "1", "0.395", "0.079", "0.395", "0.573", "1.5", "0.593", "1.31", "1.1", "1", "1", "1.3", "0.573", "1.5", "0.771", "1", "0.514", "0.103", "0.514", "0.573", "1.5", "0.593", "1.31", "1.1", "1", "1", "1.5", "0.573", "1.5", "0.889", "1", "0.593", "0.119", "0.593", "0.573", "1.5", "0.593", "1.31", "1.1", "0.9", "0.9", "2.4", "0.516", "1.35", "1.423", "0.9", "0.949", "0.211", "1.504"};
    public String[] sulawesi_selatan = {"0.312","0.897","0.335","1.183","1.048","1","1", "1", "0.312", "0.897", "0.335", "0.598", "0.223", "0.075", "0.373", "0.312", "0.897", "0.335", "1.183", "1.048", "1.088", "1.041", "1.465", "0.34", "0.934", "0.491", "0.623", "0.327", "0.105", "0.525", "0.312", "0.897", "0.335", "1.183", "1.048", "1.188", "1.141", "1.73", "0.371", "1.024", "0.579", "0.683", "0.386", "0.113", "0.556", "0.312", "0.897", "0.335", "1.183", "1.048", "1.163", "1.023", "2.661", "0.363", "0.918", "0.891", "0.612", "0.594", "0.194", "0.97"};
    public String[] sulawesi_tengah = {"0.419","0.926","0.331","1.044","1.31","1","1", "1", "0.419", "0.926", "0.331", "0.617", "0.221", "0.072", "0.358", "0.419", "0.926", "0.331", "1.044", "1.31", "1", "1.03", "1.469", "0.419", "0.953", "0.486", "0.636", "0.324", "0.102", "0.51", "0.419", "0.926", "0.331", "1.044", "1.31", "1.081", "1.13", "1.738", "0.453", "1.046", "0.575", "0.697", "0.384", "0.11", "0.55", "0.419", "0.926", "0.331", "1.044", "1.31", "0.9", "0.989", "2.675", "0.337", "0.916", "0.886", "0.61", "0.591", "0.194", "0.968"};
    public String[] sulawesi_tenggara = {"0.311","0.673","0.227","1.003","0.987","1","1", "1", "0.311", "0.673", "0.227", "0.449", "0.152", "0.068", "0.338", "0.311", "0.673", "0.227", "1.003", "0.987", "1.089", "1.131", "1.573", "0.339", "0.761", "0.358", "0.507", "0.238", "0.094", "0.47", "0.311", "0.673", "0.227", "1.003", "0.987", "1.189", "1.261", "1.945", "0.37", "0.849", "0.442", "0.566", "0.295", "0.104", "0.521", "0.311", "0.673", "0.227", "1.003", "0.987", "1.166", "1.354", "3.09", "0.363", "0.911", "0.703", "0.607", "0.469", "0.154", "0.771"};
    public String[] sulawesi_utara = {"0.519","1.217","0.512","1.003","1.062","1", "1", "1", "0.519", "1.217", "0.512", "0.811", "0.341", "0.084", "0.421", "0.519", "1.217", "0.512", "1.003", "1.062", "1", "1", "1.3", "0.519", "1.217", "0.665", "0.811", "0.444", "0.109", "0.547", "0.519", "1.217", "0.512", "1.003", "1.062", "1", "1.013", "1.5", "0.519", "1.233", "0.768", "0.822", "0.512", "0.125", "0.623", "0.519", "1.217", "0.512", "1.003", "1.062", "0.9", "0.9", "2.4", "0.467", "1.095", "1.228", "0.73", "0.819", "0.224", "1.122"};
    public String[] sumatera_barat = {"0.51","1.415","0.588","1.138","0.961","1", "1", "1", "0.51", "1.415", "0.588", "0.943", "0.392", "0.083", "0.416", "0.51", "1.415", "0.588", "1.138", "0.961", "1", "1", "1.3", "0.51", "1.415", "0.765", "0.943", "0.51", "0.108", "0.541", "0.51", "1.415", "0.588", "1.138", "0.961", "1", "1", "1.5", "0.51", "1.415", "0.882", "0.943", "0.558", "0.125", "0.624", "0.51", "1.415", "0.588", "1.138", "0.961", "0.9", "0.9", "2.4", "0.459", "1.273", "1.412", "0.849", "0.941", "0.222", "1.109"};
    public String[] sumatera_selatan = {"0.211","0.421","0.246","0.985","0.937","1", "1", "1", "0.211", "0.421", "0.246", "0.281", "0.164", "0.117", "0.584", "0.211", "0.421", "0.246", "0.985", "0.937", "1.189", "1.2", "1.554", "0.251", "0.505", "0.382", "0.337", "0.255", "0.151", "0.757", "0.211", "0.421", "0.246", "0.985", "0.937", "1.378", "1.463", "1.908", "0.291", "0.616", "0.47", "0.411", "0.313", "0.512", "0.762", "0.211", "0.421", "0.246", "0.985", "0.937", "1.645", "1.952", "3.015", "0.347", "0.822", "0.742", "0.548", "0.495", "0.181", "0.903"};
    public String[] sumatera_utara = {"0.766","1.898","0.765","0.893","0.947","1", "1", "1", "0.766", "1.898", "0.765", "1.265", "0.51", "0.081", "0.403", "0.766", "1.898", "0.765", "0.893", "0.947", "1", "1", "1.3", "0.766", "1.898", "0.995", "1.265", "0.663", "0.105", "0.524", "0.766", "1.898", "0.765", "0.893", "0.974", "1", "1", "1.5", "0.766", "1.898", "1.148", "1.265", "0.765", "0.121", "0.605", "0.766", "1.898", "0.765", "0.893", "0.947", "0.9", "0.9", "2.4", "0.689", "1.708", "1.837", "1.139", "1.224", "0.215", "1.075"};
    public String[] yogyakarta = {
            "0.529","1.212","0.444","0.928","0","1", "1", "1", "0.529", "1.212", "0.444", "0.808", "0.296", "0.073", "0.367",
            "0.529", "1.212", "0.444", "0.928", "0", "1", "1", "1.356", "0.529", "1.212", "0.602", "0.808", "0.402", "0.099", "0.497",
            "0.529", "1.212", "0.444", "0.928", "0", "1", "1.015", "1.556", "0.529", "1.23", "0.691", "0.82", "0.461", "0.112", "0.562",
            "0.529", "1.212", "0.444", "0.928", "0", "0.9", "0.9", "2.4", "0.476", "1.091", "1.067", "0.727", "0.711", "0.196", "0.978"};


    public String[] getNilaiSeismicityRegion(String kota) {
        String[] nilai = null;
        if(kota.equals("Bali")){
            nilai = bali;
        }else if(kota.equals("Banda Aceh")){
            nilai = banda_aceh;
        }else if(kota.equals("Bangka Belitung")){
            nilai = bangka_belitung;
        }else if(kota.equals("Banten")){
            nilai = banten;
        }else if(kota.equals("Bengkulu")){
            nilai = bengkulu;
        }else if(kota.equals("Gorontalo")){
            nilai = gorontalo;
        }else if(kota.equals("Jakarta")){
            nilai = jakarta;
        }else if(kota.equals("Jambi")){
            nilai = jambi;
        }else if(kota.equals("Jawa Barat")){
            nilai = jawa_barat;
        }else if(kota.equals("Jawa Tengah")){
            nilai =  jawa_tengah;
        }else if(kota.equals("Jawa Timur")){
            nilai = jawa_timur;
        }else if(kota.equals("Kalimantan Barat")){
            nilai = kalimantan_barat;
        }else if(kota.equals("Kalimantan Selatan")){
            nilai = kalimantan_selatan;
        }else if(kota.equals("Kalimantan Tengah")){
            nilai = kalimantan_tengah;
        }else if(kota.equals("Kalimantan Timur")){
            nilai = kalimantan_timur;
        }else if(kota.equals("Kalimantan Utara")){
            nilai = kalimantan_utara;
        }else if(kota.equals("Kepuluan Riau")){
            nilai = kepulauan_riau;
        }else if(kota.equals("Lampung")){
            nilai = lampung;
        }else if(kota.equals("Maluku")){
            nilai = maluku;
        }else if(kota.equals("Maluku Utara")){
            nilai = maluku_utara;
        }else if(kota.equals("Nusa Tenggara Barat")){
            nilai = nusa_tenggara_barat;
        }else if(kota.equals("Nusa Tenggara Timur")){
            nilai = nusa_tenggara_timur;
        }else if(kota.equals("Papua")){
            nilai = papua;
        }else if(kota.equals("Papua Barat")){
            nilai = papua_barat;
        }else if(kota.equals("Riau")){
            nilai = riau;
        }else if(kota.equals("Sulawesi Barat")){
            nilai = sulawesi_barat;
        }else if(kota.equals("Sulawesi Selatan")){
            nilai = sulawesi_selatan;
        }else if(kota.equals("Sulawesi Tengah")){
            nilai = sulawesi_tengah;
        }else if(kota.equals("Sulawesi Tenggara")){
            nilai = sulawesi_tenggara;
        }else if(kota.equals("Sulawesi Utara")){
            nilai = sulawesi_utara;
        }else if(kota.equals("Sumatera Barat")){
            nilai = sumatera_barat;
        }else if(kota.equals("Sumatera Selatan")){
            nilai = sumatera_selatan;
        }else if(kota.equals("Sumatera Utara")){
            nilai = sumatera_utara;
        }else if(kota.equals("Yogyakarta") || kota.equals("Daerah Istimewa Yogyakarta")){
            nilai = yogyakarta;
        }else{
            nilai = null;
        }
        return nilai;
    }


    public String[] getVariabel() {
        return variabel;
    }

    public String csar(double ss , double s1){
        String ros = "";

        if((ss < 0.167) || (s1 < 0.067)){
            ros = "LOW";
        }

        if((ss > 0.167 && ss < 0.500) || (s1 > 0.067 && s1 < 0.200)){
            ros = "MODERATE";
        }

        if((ss > 0.500) || (s1 > 0.200)){
            ros = "HIGH";
        }
        return ros;
    }
}
