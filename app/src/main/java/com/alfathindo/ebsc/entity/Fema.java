package com.alfathindo.ebsc.entity;

import android.app.Application;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

/**
 * Created by safeimuslim on 3/7/16.
 */
public class Fema extends Application{

    public static final String TABLE_FEMA = "fema";
    public static final String KEY_ID = "id";

    public static final String KEY_KOTA = "kota";
    public static final String KEY_LONGITUDE = "longitude";
    public static final String KEY_LATITUDE = "latitude";
    public static final String KEY_SEISMICITY= "seismicity";
    public static final String KEY_SS= "ss";
    public static final String KEY_S1= "s1";

    public static final String KEY_NAMA_BANGUNAN = "nama_bangunan";
    public static final String KEY_ALAMAT = "alamat";
    public static final String KEY_KODE_POS = "kode_pos";
    public static final String KEY_IDENTITAS_LAIN = "identitas_lain";
    public static final String KEY_NO_STORIES = "no_stories";
    public static final String KEY_TAHUN_PEMBANGUNAN = "tahun_pembangunan";
    public static final String KEY_SCREENER = "screener";
    public static final String KEY_TANGGAL= "tanggal";
    public static final String KEY_JUMLAH_LANTAI= "jumlah_lantai";
    public static final String KEY_KEGUNAAN= "kegunaan";

    public static final String KEY_KEPEMILIKAN= "kepemilikan";
    public static final String KEY_JUMLAH_PENGGUNA= "jumlah_pengguna";
    public static final String KEY_JENIS_TANAH= "jenis_tanah";
    public static final String KEY_BERBAHAYA_JATUH= "berbahaya_jatuh";
    public static final String KEY_TEXT_BERBAHAYA_JATUH= "text_berbahaya_jatuh";
    public static final String KEY_JENIS_BANGUNAN= "jenis_bangunan";
    public static final String KEY_BENTUK_BANGUNAN= "bentuk_bangunan";
    public static final String KEY_VERTIKAL_BANGUNAN= "vertikal_bangunan";
    public static final String KEY_DENAH_BANGUNAN= "denah_bangunan";
    public static final String KEY_PEMBUATAN_BERDASARKAN= "pembuatan_berdasarkan";

    public static final String KEY_FOLDER= "folder";



    public String _id;
    public String _kota;
    public String _longitude;
    public String _latitude;
    public String _nama_bangunan;
    public String _alamat;
    public String _kode_pos;
    public String _identitas_lain;
    public String _no_stories;
    public String _tahun_pembangunan;
    public String _screener;
    public String _tanggal;
    public String _jumlah_lantai;
    public String _kegunaan;
    public String _kepemilikan;
    public String _jumlah_pengguna;
    public String _jenis_tanah;
    public String _berbahaya_jatuh;
    public String _jenis_bangunan;
    public String _bentuk_bangunan;
    public String _folder;
    public String _seismicity;
    public String _text_berbahaya_jatuh;
    public String _ss;
    public String _s1;
    public String _vertikal_bangunan;
    public String _denah_bangunan;
    public String _pembuatan_berdasarkan;

    public String get_pembuatan_berdasarkan() {
        return _pembuatan_berdasarkan;
    }

    public void set_pembuatan_berdasarkan(String _pembuatan_berdasarkan) {
        this._pembuatan_berdasarkan = _pembuatan_berdasarkan;
    }

    public String get_s1() {
        return _s1;
    }

    public void set_s1(String _s1) {
        this._s1 = _s1;
    }

    public String get_ss() {
        return _ss;
    }

    public void set_ss(String _ss) {
        this._ss = _ss;
    }

    private static Fema mInstance= null;
    public static synchronized Fema getmInstance(){
        if(null == mInstance){
            mInstance = new Fema();
        }
        return mInstance;
    }

    public Fema() {
    }

    public Fema(String _kota, String _longitude, String _latitude, String _kode_pos) {
        this._kota = _kota;
        this._longitude = _longitude;
        this._latitude = _latitude;
        this._kode_pos = _kode_pos;
    }

    public Fema(String _id, String _kota, String _longitude, String _latitude, String _nama_bangunan, String _alamat, String _kode_pos, String _identitas_lain, String _no_stories, String _tahun_pembangunan, String _screener, String _tanggal, String _jumlah_lantai, String _kegunaan, String _kepemilikan, String _jumlah_pengguna, String _jenis_tanah, String _berbahaya_jatuh, String _jenis_bangunan, String _bentuk_bangunan, String _folder, String _seismicity, String _text_berbahaya_jatuh) {
        this._id = _id;
        this._kota = _kota;
        this._longitude = _longitude;
        this._latitude = _latitude;
        this._nama_bangunan = _nama_bangunan;
        this._alamat = _alamat;
        this._kode_pos = _kode_pos;
        this._identitas_lain = _identitas_lain;
        this._no_stories = _no_stories;
        this._tahun_pembangunan = _tahun_pembangunan;
        this._screener = _screener;
        this._tanggal = _tanggal;
        this._jumlah_lantai = _jumlah_lantai;
        this._kegunaan = _kegunaan;
        this._kepemilikan = _kepemilikan;
        this._jumlah_pengguna = _jumlah_pengguna;
        this._jenis_tanah = _jenis_tanah;
        this._berbahaya_jatuh = _berbahaya_jatuh;
        this._jenis_bangunan = _jenis_bangunan;
        this._bentuk_bangunan = _bentuk_bangunan;
        this._folder = _folder;
        this._seismicity = _seismicity;
        this._text_berbahaya_jatuh = _text_berbahaya_jatuh;
    }

    private Fema(Parcel in){
        this._id = in.readString();
        this._kota = in.readString();
        this._longitude = in.readString();
        this._latitude = in.readString();
        this._nama_bangunan = in.readString();
        this._alamat = in.readString();
        this._kode_pos = in.readString();
        this._identitas_lain = in.readString();
        this._no_stories = in.readString();
        this._tahun_pembangunan = in.readString();
        this._screener = in.readString();
        this._tanggal = in.readString();
        this._jumlah_lantai = in.readString();
        this._kegunaan = in.readString();
        this._kepemilikan = in.readString();
        this._jumlah_pengguna = in.readString();
        this._jenis_tanah = in.readString();
        this._berbahaya_jatuh = in.readString();
        this._jenis_bangunan = in.readString();
        this._bentuk_bangunan = in.readString();
        this._folder = in.readString();
        this._seismicity = in.readString();
        this._text_berbahaya_jatuh = in.readString();

    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String get_kota() {
        return _kota;
    }

    public void set_kota(String _kota) {
        this._kota = _kota;
    }

    public String get_longitude() {
        return _longitude;
    }

    public void set_longitude(String _longitude) {
        this._longitude = _longitude;
    }

    public String get_latitude() {
        return _latitude;
    }

    public void set_latitude(String _latitude) {
        this._latitude = _latitude;
    }

    public String get_nama_bangunan() {
        return _nama_bangunan;
    }

    public void set_nama_bangunan(String _nama_bangunan) {
        this._nama_bangunan = _nama_bangunan;
    }

    public String get_alamat() {
        return _alamat;
    }

    public void set_alamat(String _alamat) {
        this._alamat = _alamat;
    }

    public String get_kode_pos() {
        return _kode_pos;
    }

    public void set_kode_pos(String _kode_pos) {
        this._kode_pos = _kode_pos;
    }

    public String get_identitas_lain() {
        return _identitas_lain;
    }

    public void set_identitas_lain(String _identitas_lain) {
        this._identitas_lain = _identitas_lain;
    }

    public String get_no_stories() {
        return _no_stories;
    }

    public void set_no_stories(String _no_stories) {
        this._no_stories = _no_stories;
    }

    public String get_tahun_pembangunan() {
        return _tahun_pembangunan;
    }

    public void set_tahun_pembangunan(String _tahun_pembangunan) {
        this._tahun_pembangunan = _tahun_pembangunan;
    }

    public String get_screener() {
        return _screener;
    }

    public void set_screener(String _screener) {
        this._screener = _screener;
    }

    public String get_tanggal() {
        return _tanggal;
    }

    public void set_tanggal(String _tanggal) {
        this._tanggal = _tanggal;
    }

    public String get_jumlah_lantai() {
        return _jumlah_lantai;
    }

    public void set_jumlah_lantai(String _jumlah_lantai) {
        this._jumlah_lantai = _jumlah_lantai;
    }

    public String get_kegunaan() {
        return _kegunaan;
    }

    public void set_kegunaan(String _kegunaan) {
        this._kegunaan = _kegunaan;
    }

    public String get_kepemilikan() {
        return _kepemilikan;
    }

    public void set_kepemilikan(String _kepemilikan) {
        this._kepemilikan = _kepemilikan;
    }

    public String get_jumlah_pengguna() {
        return _jumlah_pengguna;
    }

    public void set_jumlah_pengguna(String _jumlah_pengguna) {
        this._jumlah_pengguna = _jumlah_pengguna;
    }

    public String get_jenis_tanah() {
        return _jenis_tanah;
    }

    public void set_jenis_tanah(String _jenis_tanah) {
        this._jenis_tanah = _jenis_tanah;
    }

    public String get_berbahaya_jatuh() {
        return _berbahaya_jatuh;
    }

    public void set_berbahaya_jatuh(String _berbahaya_jatuh) {
        this._berbahaya_jatuh = _berbahaya_jatuh;
    }

    public String get_jenis_bangunan() {
        return _jenis_bangunan;
    }

    public void set_jenis_bangunan(String _jenis_bangunan) {
        this._jenis_bangunan = _jenis_bangunan;
    }

    public String get_bentuk_bangunan() {
        return _bentuk_bangunan;
    }

    public void set_bentuk_bangunan(String _bentuk_bangunan) {
        this._bentuk_bangunan = _bentuk_bangunan;
    }

    public String get_folder() {
        return _folder;
    }

    public void set_folder(String _folder) {
        this._folder = _folder;
    }

    public String get_seismicity() {
        return _seismicity;
    }

    public void set_seismicity(String _seismicity) {
        this._seismicity = _seismicity;
    }

    public String get_text_berbahaya_jatuh() {
        return _text_berbahaya_jatuh;
    }

    public void set_text_berbahaya_jatuh(String _text_berbahaya_jatuh) {
        this._text_berbahaya_jatuh = _text_berbahaya_jatuh;
    }

    public String get_denah_bangunan() {
        return _denah_bangunan;
    }

    public void set_denah_bangunan(String _denah_bangunan) {
        this._denah_bangunan = _denah_bangunan;
    }

    public String get_vertikal_bangunan() {
        return _vertikal_bangunan;
    }

    public void set_vertikal_bangunan(String _vertikal_bangunan) {
        this._vertikal_bangunan = _vertikal_bangunan;
    }
}
