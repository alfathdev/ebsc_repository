package com.alfathindo.ebsc.entity;

import android.app.Application;

/**
 * Created by safeimuslim on 8/14/16.
 */
public class Settings extends Application {

    public boolean peta_aktif = false; //default
    public String base_layers = "NORMAL"; //default

    public Settings() {
    }

    private static Settings mInstance = null;
    public static synchronized Settings getmInstance(){
        if(mInstance == null){
            mInstance = new Settings();
        }
        return mInstance;
    }
}
