package com.alfathindo.ebsc.entity;

/**
 * Created by safeimuslim on 3/14/16.
 */
public class FinalScore {

    public static final String TABLE_FINAL_SCORE_LOW = "FINAL_SCORE_LOW";
    public static final String TABLE_FINAL_SCORE_MODERATE = "FINAL_SCORE_MODERATE";
    public static final String TABLE_FINAL_SCORE_HIGH = "FINAL_SCORE_HIGH";


    public static final String KEY_ID = "id";
    public static final String KEY_ID_FEMA = "id_fema";
    public static final String KEY_BASIC_SCORE = "basic_score";
    public static final String KEY_STORIES = "stories";
    public static final String KEY_VERTICAL_IRREGULARITY= "vertical_irregularity";
    public static final String KEY_PLAN_IRREGULARITY= "plan_irregularity";
    public static final String KEY_IRREGULARITY= "irregularity";
    public static final String KEY_CODE_BENCMARK = "code_bencmark";
    public static final String KEY_TEXT_CODE_BENCMARK = "text_code_bencmark";
    public static final String KEY_JENIS_TANAH_V = "jenis_tanah_v";
    public static final String KEY_FINAL_SCORE = "final_score";
    public static final String KEY_RESIKO_GEMPA = "resiko_gempa";
    public static final String KEY_COMMENT = "komen";
    public static final String KEY_EVALUATION_REQUIRED = "evaluation_required";

    private String _id;
    private String _id_fema;
    private String _basic_score;
    private String _stories;
    private String _vertical_irregularity;
    private String _plan_irregularity;
    private String _irregularity;
    private String _code_bencmark;
    private String _jenis_tanah_v;
    private String _final_score;
    private String _resiko_gempa;
    private String _komen;
    private String _evaluation_required;
    private String _text_code_bencmark;

    public String get_text_code_bencmark() {
        return _text_code_bencmark;
    }

    public void set_text_code_bencmark(String _text_code_bencmark) {
        this._text_code_bencmark = _text_code_bencmark;
    }

    public String get_vertical_irregularity() {
        return _vertical_irregularity;
    }

    public void set_vertical_irregularity(String _vertical_irregularity) {
        this._vertical_irregularity = _vertical_irregularity;
    }

    public String get_plan_irregularity() {
        return _plan_irregularity;
    }

    public void set_plan_irregularity(String _plan_irregularity) {
        this._plan_irregularity = _plan_irregularity;
    }

    public String get_basic_score() {
        return _basic_score;
    }

    public void set_basic_score(String _basic_score) {
        this._basic_score = _basic_score;
    }

    public String get_stories() {
        return _stories;
    }

    public void set_stories(String _stories) {
        this._stories = _stories;
    }

    public String get_irregularity() {
        return _irregularity;
    }

    public void set_irregularity(String _irregularity) {
        this._irregularity = _irregularity;
    }

    public String get_code_bencmark() {
        return _code_bencmark;
    }

    public void set_code_bencmark(String _code_bencmark) {
        this._code_bencmark = _code_bencmark;
    }

    public String get_jenis_tanah_v() {
        return _jenis_tanah_v;
    }

    public void set_jenis_tanah_v(String _jenis_tanah_v) {
        this._jenis_tanah_v = _jenis_tanah_v;
    }

    public String get_final_score() {
        return _final_score;
    }

    public void set_final_score(String _final_score) {
        this._final_score = _final_score;
    }

    public String get_resiko_gempa() {
        return _resiko_gempa;
    }

    public void set_resiko_gempa(String _resiko_gempa) {
        this._resiko_gempa = _resiko_gempa;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String get_id_fema() {
        return _id_fema;
    }

    public void set_id_fema(String _id_fema) {
        this._id_fema = _id_fema;
    }

    public String get_komen() {
        return _komen;
    }

    public void set_komen(String _komen) {
        this._komen = _komen;
    }

    public String get_evaluation_required() {
        return _evaluation_required;
    }

    public void set_evaluation_required(String _evaluation_required) {
        this._evaluation_required = _evaluation_required;
    }
}
