package com.alfathindo.ebsc.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.alfathindo.ebsc.entity.Fema;
import com.alfathindo.ebsc.entity.FinalScore;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by safeimuslim on 3/7/16.
 */
public class FemaRepo {
    private DatabaseHandler databaseHandler;
    SQLiteDatabase sqLiteDatabase;

    public FemaRepo(Context context){
        databaseHandler = new DatabaseHandler(context);
    }

    public void addFema(Fema fema){
         sqLiteDatabase = databaseHandler.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Fema.KEY_KOTA, fema.get_kota());
        contentValues.put(Fema.KEY_LATITUDE,fema.get_latitude());
        contentValues.put(Fema.KEY_LONGITUDE,fema.get_longitude());
        contentValues.put(Fema.KEY_SEISMICITY,fema.get_seismicity());
        contentValues.put(Fema.KEY_SS,fema.get_ss());
        contentValues.put(Fema.KEY_S1,fema.get_s1());

        contentValues.put(Fema.KEY_NAMA_BANGUNAN, fema.get_nama_bangunan());
        contentValues.put(Fema.KEY_ALAMAT, fema.get_alamat());
        contentValues.put(Fema.KEY_KODE_POS, fema.get_kode_pos());
        contentValues.put(Fema.KEY_IDENTITAS_LAIN, fema.get_identitas_lain());
        contentValues.put(Fema.KEY_NO_STORIES, fema.get_no_stories());
        contentValues.put(Fema.KEY_TAHUN_PEMBANGUNAN, fema.get_tahun_pembangunan());
        contentValues.put(Fema.KEY_SCREENER, fema.get_screener());
        contentValues.put(Fema.KEY_TANGGAL, fema.get_tanggal());
        contentValues.put(Fema.KEY_JUMLAH_LANTAI, fema.get_jumlah_lantai());
        contentValues.put(Fema.KEY_KEGUNAAN, fema.get_kegunaan());

        contentValues.put(Fema.KEY_KEPEMILIKAN, fema.get_kepemilikan());
        contentValues.put(Fema.KEY_JUMLAH_PENGGUNA, fema.get_jumlah_pengguna());
        contentValues.put(Fema.KEY_JENIS_TANAH, fema.get_jenis_tanah());
        contentValues.put(Fema.KEY_BERBAHAYA_JATUH, fema.get_berbahaya_jatuh());
        contentValues.put(Fema.KEY_JENIS_BANGUNAN, fema.get_jenis_bangunan());
        contentValues.put(Fema.KEY_BENTUK_BANGUNAN, fema.get_bentuk_bangunan());
        contentValues.put(Fema.KEY_TEXT_BERBAHAYA_JATUH, fema.get_text_berbahaya_jatuh());

        contentValues.put(Fema.KEY_FOLDER, fema.get_folder());
        contentValues.put(Fema.KEY_VERTIKAL_BANGUNAN, fema.get_vertikal_bangunan());
        contentValues.put(Fema.KEY_DENAH_BANGUNAN, fema.get_denah_bangunan());
        contentValues.put(Fema.KEY_PEMBUATAN_BERDASARKAN, fema.get_pembuatan_berdasarkan());


        sqLiteDatabase.insert(Fema.TABLE_FEMA, null,contentValues);
        sqLiteDatabase.close();
    }

    public int updateFema(Fema fema){
        sqLiteDatabase = databaseHandler.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(Fema.KEY_KOTA, fema.get_kota());
        contentValues.put(Fema.KEY_LATITUDE,fema.get_latitude());
        contentValues.put(Fema.KEY_LONGITUDE,fema.get_longitude());
        contentValues.put(Fema.KEY_SEISMICITY,fema.get_seismicity());
        contentValues.put(Fema.KEY_SS,fema.get_ss());
        contentValues.put(Fema.KEY_S1,fema.get_s1());


        contentValues.put(Fema.KEY_NAMA_BANGUNAN, fema.get_nama_bangunan());
        contentValues.put(Fema.KEY_ALAMAT, fema.get_alamat());
        contentValues.put(Fema.KEY_KODE_POS, fema.get_kode_pos());
        contentValues.put(Fema.KEY_IDENTITAS_LAIN, fema.get_identitas_lain());
        contentValues.put(Fema.KEY_NO_STORIES, fema.get_no_stories());
        contentValues.put(Fema.KEY_TAHUN_PEMBANGUNAN, fema.get_tahun_pembangunan());
        contentValues.put(Fema.KEY_SCREENER, fema.get_screener());
        contentValues.put(Fema.KEY_TANGGAL, fema.get_tanggal());
        contentValues.put(Fema.KEY_JUMLAH_LANTAI, fema.get_jumlah_lantai());
        contentValues.put(Fema.KEY_KEGUNAAN, fema.get_kegunaan());

        contentValues.put(Fema.KEY_KEPEMILIKAN, fema.get_kepemilikan());
        contentValues.put(Fema.KEY_JUMLAH_PENGGUNA, fema.get_jumlah_pengguna());
        contentValues.put(Fema.KEY_JENIS_TANAH, fema.get_jenis_tanah());
        contentValues.put(Fema.KEY_BERBAHAYA_JATUH, fema.get_berbahaya_jatuh());
        contentValues.put(Fema.KEY_JENIS_BANGUNAN, fema.get_jenis_bangunan());
        contentValues.put(Fema.KEY_BENTUK_BANGUNAN, fema.get_bentuk_bangunan());
        contentValues.put(Fema.KEY_TEXT_BERBAHAYA_JATUH, fema.get_text_berbahaya_jatuh());
        contentValues.put(Fema.KEY_VERTIKAL_BANGUNAN, fema.get_vertikal_bangunan());
        contentValues.put(Fema.KEY_DENAH_BANGUNAN, fema.get_denah_bangunan());
        contentValues.put(Fema.KEY_PEMBUATAN_BERDASARKAN, fema.get_pembuatan_berdasarkan());



        return sqLiteDatabase.update(Fema.TABLE_FEMA, contentValues, Fema.KEY_ID+ "="+fema.get_id(),null);
    }

    public int getFemaLastId(){
        int _last_id = 0;
         sqLiteDatabase =  databaseHandler.getWritableDatabase();
        String query = "SELECT "+Fema.KEY_ID+" FROM "+Fema.TABLE_FEMA+" ORDER BY "+Fema.KEY_ID+" DESC LIMIT 1";
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            _last_id = cursor.getInt(cursor.getColumnIndex(Fema.KEY_ID));
        }
        return _last_id;
    }

    public Fema getFemaById(String id){
         sqLiteDatabase = databaseHandler.getWritableDatabase();
        //String selectQuery = "SELECT "+Fema.KEY_KOTA+", "+Fema.KEY_LONGITUDE+", "+Fema.KEY_LATITUDE+" FROM "+Fema.TABLE_FEMA+" WHERE "+Fema.KEY_ID+"=?;"
        Cursor cursor = sqLiteDatabase.query(Fema.TABLE_FEMA,new String[]{
                Fema.KEY_ID,
                Fema.KEY_KOTA,
                Fema.KEY_LATITUDE,
                Fema.KEY_LONGITUDE,
                Fema.KEY_SEISMICITY,
                Fema.KEY_SS,
                Fema.KEY_S1,
                Fema.KEY_NAMA_BANGUNAN,
                Fema.KEY_ALAMAT,
                Fema.KEY_KODE_POS,
                Fema.KEY_IDENTITAS_LAIN,
                Fema.KEY_NO_STORIES,
                Fema.KEY_TAHUN_PEMBANGUNAN,
                Fema.KEY_SCREENER,
                Fema.KEY_TANGGAL,
                Fema.KEY_JUMLAH_LANTAI,
                Fema.KEY_KEGUNAAN,
                Fema.KEY_KEPEMILIKAN,
                Fema.KEY_JUMLAH_PENGGUNA,
                Fema.KEY_JENIS_TANAH,
                Fema.KEY_BERBAHAYA_JATUH,
                Fema.KEY_JENIS_BANGUNAN,
                Fema.KEY_BENTUK_BANGUNAN,
                Fema.KEY_TEXT_BERBAHAYA_JATUH,
                Fema.KEY_VERTIKAL_BANGUNAN,
                Fema.KEY_DENAH_BANGUNAN,
                Fema.KEY_PEMBUATAN_BERDASARKAN,
                Fema.KEY_FOLDER
                },Fema.KEY_ID+"=?",new String[]{String.valueOf(id)},null,null,null,null);

        Fema fema = new Fema();
        if(cursor !=null) {
            cursor.moveToFirst();
            fema.set_kota(cursor.getString(cursor.getColumnIndex(Fema.KEY_KOTA)));
            fema.set_latitude(cursor.getString(cursor.getColumnIndex(Fema.KEY_LATITUDE)));
            fema.set_longitude(cursor.getString(cursor.getColumnIndex(Fema.KEY_LONGITUDE)));
            fema.set_seismicity(cursor.getString(cursor.getColumnIndex(Fema.KEY_SEISMICITY)));
            fema.set_ss(cursor.getString(cursor.getColumnIndex(Fema.KEY_SS)));
            fema.set_s1(cursor.getString(cursor.getColumnIndex(Fema.KEY_S1)));

            fema.set_nama_bangunan(cursor.getString(cursor.getColumnIndex(Fema.KEY_NAMA_BANGUNAN)));
            fema.set_alamat(cursor.getString(cursor.getColumnIndex(Fema.KEY_ALAMAT)));
            fema.set_kode_pos(cursor.getString(cursor.getColumnIndex(Fema.KEY_KODE_POS)));
            fema.set_identitas_lain(cursor.getString(cursor.getColumnIndex(Fema.KEY_IDENTITAS_LAIN)));
            fema.set_no_stories(cursor.getString(cursor.getColumnIndex(Fema.KEY_NO_STORIES)));
            fema.set_tahun_pembangunan(cursor.getString(cursor.getColumnIndex(Fema.KEY_TAHUN_PEMBANGUNAN)));
            fema.set_screener(cursor.getString(cursor.getColumnIndex(Fema.KEY_SCREENER)));
            fema.set_tanggal(cursor.getString(cursor.getColumnIndex(Fema.KEY_TANGGAL)));
            fema.set_jumlah_lantai(cursor.getString(cursor.getColumnIndex(Fema.KEY_JUMLAH_LANTAI)));
            fema.set_kegunaan(cursor.getString(cursor.getColumnIndex(Fema.KEY_KEGUNAAN)));

            fema.set_kepemilikan(cursor.getString(cursor.getColumnIndex(Fema.KEY_KEPEMILIKAN)));
            fema.set_jumlah_pengguna(cursor.getString(cursor.getColumnIndex(Fema.KEY_JUMLAH_PENGGUNA)));
            fema.set_jenis_tanah(cursor.getString(cursor.getColumnIndex(Fema.KEY_JENIS_TANAH)));
            fema.set_berbahaya_jatuh(cursor.getString(cursor.getColumnIndex(Fema.KEY_BERBAHAYA_JATUH)));
            fema.set_jenis_bangunan(cursor.getString(cursor.getColumnIndex(Fema.KEY_JENIS_BANGUNAN)));
            fema.set_bentuk_bangunan(cursor.getString(cursor.getColumnIndex(Fema.KEY_BENTUK_BANGUNAN)));
            fema.set_text_berbahaya_jatuh(cursor.getString(cursor.getColumnIndex(Fema.KEY_TEXT_BERBAHAYA_JATUH)));

            fema.set_vertikal_bangunan(cursor.getString(cursor.getColumnIndex(Fema.KEY_VERTIKAL_BANGUNAN)));
            fema.set_denah_bangunan(cursor.getString(cursor.getColumnIndex(Fema.KEY_DENAH_BANGUNAN)));
            fema.set_pembuatan_berdasarkan(cursor.getString(cursor.getColumnIndex(Fema.KEY_PEMBUATAN_BERDASARKAN)));

            fema.set_folder(cursor.getString(cursor.getColumnIndex(Fema.KEY_FOLDER)));
        }
        cursor.close();
        sqLiteDatabase.close();
        return fema;
    }

    public List<Fema> getAllFema(){
        List<Fema> femaList = new ArrayList<Fema>();
         sqLiteDatabase =  databaseHandler.getWritableDatabase();
        String selectQuery = "SELECT * FROM "+Fema.TABLE_FEMA;
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery,null);
        if(cursor.moveToFirst()){
            do {
                Fema fema = new Fema();
                fema.set_id(cursor.getString(cursor.getColumnIndex(Fema.KEY_ID)));
                fema.set_kota(cursor.getString(cursor.getColumnIndex(Fema.KEY_KOTA)));
                fema.set_latitude(cursor.getString(cursor.getColumnIndex(Fema.KEY_LATITUDE)));
                fema.set_longitude(cursor.getString(cursor.getColumnIndex(Fema.KEY_LONGITUDE)));
                fema.set_seismicity(cursor.getString(cursor.getColumnIndex(Fema.KEY_SEISMICITY)));
                femaList.add(fema);
            }while (cursor.moveToNext());
        }
        return femaList;
    }

    public boolean delete(String id){
        sqLiteDatabase = databaseHandler.getWritableDatabase();

        return (
                sqLiteDatabase.delete(Fema.TABLE_FEMA, Fema.KEY_ID + "=" + id, null) > 0
                &&sqLiteDatabase.delete(FinalScore.TABLE_FINAL_SCORE_HIGH, FinalScore.KEY_ID_FEMA + "=" + id, null) > 0
                &&sqLiteDatabase.delete(FinalScore.TABLE_FINAL_SCORE_MODERATE, FinalScore.KEY_ID_FEMA + "=" + id, null) > 0
                &&sqLiteDatabase.delete(FinalScore.TABLE_FINAL_SCORE_LOW, FinalScore.KEY_ID_FEMA + "=" + id, null) > 0
        );
    }
}
