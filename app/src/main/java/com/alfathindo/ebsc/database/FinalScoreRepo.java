package com.alfathindo.ebsc.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

import com.alfathindo.ebsc.entity.Fema;
import com.alfathindo.ebsc.entity.FinalScore;

/**
 * Created by safeimuslim on 3/14/16.
 */
public class FinalScoreRepo {

    private DatabaseHandler databaseHandler;

    public FinalScoreRepo(Context context){
        databaseHandler = new DatabaseHandler(context);
    }

    public void addFinalScore(String tabel, FinalScore finalScore){
        SQLiteDatabase sqLiteDatabase = databaseHandler.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(FinalScore.KEY_ID_FEMA, finalScore.get_id_fema());
        contentValues.put(FinalScore.KEY_BASIC_SCORE, finalScore.get_basic_score());
        contentValues.put(FinalScore.KEY_STORIES,finalScore.get_stories());
        contentValues.put(FinalScore.KEY_IRREGULARITY,finalScore.get_irregularity());
        contentValues.put(FinalScore.KEY_VERTICAL_IRREGULARITY,finalScore.get_vertical_irregularity());
        contentValues.put(FinalScore.KEY_PLAN_IRREGULARITY,finalScore.get_plan_irregularity());
        contentValues.put(FinalScore.KEY_CODE_BENCMARK,finalScore.get_code_bencmark());
        contentValues.put(FinalScore.KEY_TEXT_CODE_BENCMARK,finalScore.get_text_code_bencmark());
        contentValues.put(FinalScore.KEY_JENIS_TANAH_V,finalScore.get_jenis_tanah_v());
        contentValues.put(FinalScore.KEY_FINAL_SCORE,finalScore.get_final_score());
        contentValues.put(FinalScore.KEY_RESIKO_GEMPA,finalScore.get_resiko_gempa());

        sqLiteDatabase.insert(tabel, null,contentValues);
        sqLiteDatabase.close();
    }

    public int updateFinalScore(String tabel, FinalScore finalScore){
        SQLiteDatabase sqLiteDatabase = databaseHandler.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(FinalScore.KEY_BASIC_SCORE, finalScore.get_basic_score());
        contentValues.put(FinalScore.KEY_STORIES,finalScore.get_stories());
        contentValues.put(FinalScore.KEY_IRREGULARITY,finalScore.get_irregularity());
        contentValues.put(FinalScore.KEY_VERTICAL_IRREGULARITY,finalScore.get_vertical_irregularity());
        contentValues.put(FinalScore.KEY_PLAN_IRREGULARITY,finalScore.get_plan_irregularity());

        contentValues.put(FinalScore.KEY_CODE_BENCMARK,finalScore.get_code_bencmark());
        contentValues.put(FinalScore.KEY_TEXT_CODE_BENCMARK,finalScore.get_text_code_bencmark());
        contentValues.put(FinalScore.KEY_JENIS_TANAH_V,finalScore.get_jenis_tanah_v());
        contentValues.put(FinalScore.KEY_FINAL_SCORE,finalScore.get_final_score());
        contentValues.put(FinalScore.KEY_RESIKO_GEMPA,finalScore.get_resiko_gempa());

        return sqLiteDatabase.update(tabel, contentValues, FinalScore.KEY_ID_FEMA + "="+finalScore.get_id_fema(),null);
    }

    public int updateKomenFinalScore(String tabel, FinalScore finalScore){
        SQLiteDatabase sqLiteDatabase = databaseHandler.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(FinalScore.KEY_COMMENT, finalScore.get_komen());
        contentValues.put(FinalScore.KEY_EVALUATION_REQUIRED, finalScore.get_evaluation_required());

        return sqLiteDatabase.update(tabel, contentValues, FinalScore.KEY_ID_FEMA + "="+finalScore.get_id_fema(),null);
    }

    public String getResikoGempa(String table, String id_fema){
        String resiko_gempa ="";
        SQLiteDatabase sqLiteDatabase =  databaseHandler.getWritableDatabase();
        String query = "SELECT "+FinalScore.KEY_RESIKO_GEMPA+" FROM "+table+" WHERE "+Fema.KEY_ID+" = "+id_fema+" ";
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            resiko_gempa = cursor.getString(cursor.getColumnIndex(FinalScore.KEY_RESIKO_GEMPA));
        }
        return resiko_gempa;
    }

    public FinalScore getFinalScroreById(String table, String id_fema){
        SQLiteDatabase sqLiteDatabase =  databaseHandler.getWritableDatabase();
        FinalScore finalScore = new FinalScore();
        String query = "SELECT * FROM "+table+" WHERE "+Fema.KEY_ID+" = "+id_fema+" ";
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        if(cursor.moveToFirst()){
            finalScore.set_basic_score(cursor.getString(cursor.getColumnIndex(FinalScore.KEY_BASIC_SCORE)));
            finalScore.set_stories(cursor.getString(cursor.getColumnIndex(FinalScore.KEY_STORIES)));
            finalScore.set_irregularity(cursor.getString(cursor.getColumnIndex(FinalScore.KEY_IRREGULARITY)));
            finalScore.set_vertical_irregularity(cursor.getString(cursor.getColumnIndex(FinalScore.KEY_VERTICAL_IRREGULARITY)));
            finalScore.set_plan_irregularity(cursor.getString(cursor.getColumnIndex(FinalScore.KEY_PLAN_IRREGULARITY)));
            finalScore.set_code_bencmark(cursor.getString(cursor.getColumnIndex(FinalScore.KEY_CODE_BENCMARK)));
            finalScore.set_text_code_bencmark(cursor.getString(cursor.getColumnIndex(FinalScore.KEY_TEXT_CODE_BENCMARK)));
            finalScore.set_jenis_tanah_v(cursor.getString(cursor.getColumnIndex(FinalScore.KEY_JENIS_TANAH_V)));
            finalScore.set_resiko_gempa(cursor.getString(cursor.getColumnIndex(FinalScore.KEY_RESIKO_GEMPA)));
            finalScore.set_final_score(cursor.getString(cursor.getColumnIndex(FinalScore.KEY_FINAL_SCORE)));
            finalScore.set_komen(cursor.getString(cursor.getColumnIndex(FinalScore.KEY_COMMENT)));
            finalScore.set_evaluation_required(cursor.getString(cursor.getColumnIndex(FinalScore.KEY_EVALUATION_REQUIRED)));
        }

        cursor.close();
        sqLiteDatabase.close();
        return finalScore;
    }
}
