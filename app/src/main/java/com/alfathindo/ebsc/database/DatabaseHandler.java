package com.alfathindo.ebsc.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.alfathindo.ebsc.entity.Fema;
import com.alfathindo.ebsc.entity.FinalScore;

/**
 * Created by safeimuslim on 3/7/16.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    public static final int DATABASE_VERSION = 4;

    // Database Name
    public static final String DATABASE_NAME = "ebsc_database.db";

    String CREATE_FEMA_TABLE = "CREATE TABLE "+ Fema.TABLE_FEMA+" ("+
            Fema.KEY_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+
            Fema.KEY_KOTA+" TEXT,"+
            Fema.KEY_LATITUDE+" TEXT,"+
            Fema.KEY_LONGITUDE+" TEXT,"+
            Fema.KEY_SEISMICITY+" TEXT,"+
            Fema.KEY_SS+" TEXT,"+
            Fema.KEY_S1+" TEXT,"+
            Fema.KEY_NAMA_BANGUNAN+" TEXT,"+
            Fema.KEY_ALAMAT+" TEXT,"+
            Fema.KEY_KODE_POS+" TEXT,"+
            Fema.KEY_IDENTITAS_LAIN+" TEXT,"+
            Fema.KEY_NO_STORIES+" TEXT,"+
            Fema.KEY_TAHUN_PEMBANGUNAN+" TEXT,"+
            Fema.KEY_SCREENER+" TEXT,"+
            Fema.KEY_TANGGAL+" TEXT,"+
            Fema.KEY_JUMLAH_LANTAI+" TEXT,"+
            Fema.KEY_KEGUNAAN+" TEXT,"+
            Fema.KEY_KEPEMILIKAN+" TEXT,"+
            Fema.KEY_JUMLAH_PENGGUNA+" TEXT,"+
            Fema.KEY_JENIS_TANAH+" TEXT,"+
            Fema.KEY_BERBAHAYA_JATUH+" TEXT,"+
            Fema.KEY_JENIS_BANGUNAN+" TEXT,"+
            Fema.KEY_BENTUK_BANGUNAN+" TEXT,"+
            Fema.KEY_VERTIKAL_BANGUNAN+" TEXT,"+
            Fema.KEY_DENAH_BANGUNAN+" TEXT,"+
            Fema.KEY_PEMBUATAN_BERDASARKAN+" TEXT,"+

            Fema.KEY_TEXT_BERBAHAYA_JATUH+" TEXT,"+
            Fema.KEY_FOLDER+" TEXT)";

    String CREATE_LOW_FINAL_SCORE_TABLE = "CREATE TABLE "+ FinalScore.TABLE_FINAL_SCORE_LOW+"("+
            FinalScore.KEY_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+
            FinalScore.KEY_ID_FEMA+" INTEGER, "+
            FinalScore.KEY_BASIC_SCORE+" TEXT, "+
            FinalScore.KEY_STORIES+" TEXT, "+
            FinalScore.KEY_IRREGULARITY+" TEXT, "+
            FinalScore.KEY_VERTICAL_IRREGULARITY+" TEXT,"+
            FinalScore.KEY_PLAN_IRREGULARITY+" TEXT,"+
            FinalScore.KEY_CODE_BENCMARK+" TEXT, "+
            FinalScore.KEY_TEXT_CODE_BENCMARK+" TEXT, "+
            FinalScore.KEY_JENIS_TANAH_V+" TEXT, "+
            FinalScore.KEY_FINAL_SCORE+" TEXT, "+
            FinalScore.KEY_RESIKO_GEMPA+" TEXT, "+
            FinalScore.KEY_COMMENT+" TEXT, "+
            FinalScore.KEY_EVALUATION_REQUIRED+" TEXT "+")";

    String CREATE_MODERATE_FINAL_SCORE_TABLE = "CREATE TABLE "+ FinalScore.TABLE_FINAL_SCORE_MODERATE+"("+
            FinalScore.KEY_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+
            FinalScore.KEY_ID_FEMA+" INTEGER, "+
            FinalScore.KEY_BASIC_SCORE+" TEXT, "+
            FinalScore.KEY_STORIES+" TEXT, "+
            FinalScore.KEY_IRREGULARITY+" TEXT, "+
            FinalScore.KEY_VERTICAL_IRREGULARITY+" TEXT,"+
            FinalScore.KEY_PLAN_IRREGULARITY+" TEXT,"+
            FinalScore.KEY_CODE_BENCMARK+" TEXT, "+
            FinalScore.KEY_TEXT_CODE_BENCMARK+" TEXT, "+
            FinalScore.KEY_JENIS_TANAH_V+" TEXT, "+
            FinalScore.KEY_FINAL_SCORE+" TEXT, "+
            FinalScore.KEY_RESIKO_GEMPA+" TEXT, "+
            FinalScore.KEY_COMMENT+" TEXT, "+
            FinalScore.KEY_EVALUATION_REQUIRED+" TEXT "+")";

    String CREATE_HIGH_FINAL_SCORE_TABLE = "CREATE TABLE "+ FinalScore.TABLE_FINAL_SCORE_HIGH+"("+
            FinalScore.KEY_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+
            FinalScore.KEY_ID_FEMA+" INTEGER, "+
            FinalScore.KEY_BASIC_SCORE+" TEXT, "+
            FinalScore.KEY_STORIES+" TEXT, "+
            FinalScore.KEY_IRREGULARITY+" TEXT, "+
            FinalScore.KEY_VERTICAL_IRREGULARITY+" TEXT,"+
            FinalScore.KEY_PLAN_IRREGULARITY+" TEXT,"+
            FinalScore.KEY_CODE_BENCMARK+" TEXT, "+
            FinalScore.KEY_TEXT_CODE_BENCMARK+" TEXT, "+
            FinalScore.KEY_JENIS_TANAH_V+" TEXT, "+
            FinalScore.KEY_FINAL_SCORE+" TEXT, "+
            FinalScore.KEY_RESIKO_GEMPA+" TEXT, "+
            FinalScore.KEY_COMMENT+" TEXT, "+
            FinalScore.KEY_EVALUATION_REQUIRED+" TEXT "+")";



    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_FEMA_TABLE);
        db.execSQL(CREATE_LOW_FINAL_SCORE_TABLE);
        db.execSQL(CREATE_MODERATE_FINAL_SCORE_TABLE);
        db.execSQL(CREATE_HIGH_FINAL_SCORE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + Fema.TABLE_FEMA);

        // Create tables again
        onCreate(db);
    }
}
