package com.alfathindo.ebsc.wizard.model;

import android.support.v4.app.Fragment;
import android.text.TextUtils;

import com.alfathindo.ebsc.wizard.ui.FemaDuaFragment;
import com.alfathindo.ebsc.wizard.ui.FemaSatuFragment;
import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

/**
 * Created by safeimuslim on 7/25/16.
 */
public class FemaDuaPage extends Page{

    public static final String SPINNER_PEMBUATAN_BERDASARKAN_DATA_KEY = "spinnerPembuatanBerdasarkan";
    public static final String SPINNER_JENIS_BANGUNAN_DATA_KEY = "spinnerJenisBangunan";
    public static final String SPINNER_VERTIKAL_BANGUNAN_DATA_KEY = "spinnerVertikalBangunan";
    public static final String SPINNER_DENAH_BANGUNAN_DATA_KEY = "spinnerDenahBangunan";
    public static final String SPINNER_BENTUK_BANGUNAN_DATA_KEY =  "spinnerBentukBangunan";
    public static final String SPINNER_KEPEMILIKAN_DATA_KEY = "spinnerKepemilikan";
    public static final String SPINNER_JUMLAH_PENGGUNA_DATA_KEY = "spinnerJumlahPengguna";
    public static final String SPINNER_JENIS_TANAH_DATA_KEY = "spinnerJenisTanah";
    public static final String SPINNER_ELEMEN_BERBAHAYA_JATUH_DATA_KEY = "spinnerElemen_berbahayaJatuh";
    public static final String LAINYA_DATA_KEY = "ed_lainya";

    public FemaDuaPage(ModelCallbacks callbacks, String title) {
        super(callbacks, title);
      /*  mData.putString(SPINNER_JENIS_BANGUNAN_DATA_KEY, "Pilih Jenis Bangunan");
        mData.putString(SPINNER_BENTUK_BANGUNAN_DATA_KEY, "Pilih Bentuk Bangunan");
        mData.putString(SPINNER_KEPEMILIKAN_DATA_KEY, "Pilih Kepemilikan");
        mData.putString(SPINNER_JUMLAH_PENGGUNA_DATA_KEY, "Pilih Jumlah Pengguna");
        mData.putString(SPINNER_JENIS_TANAH_DATA_KEY, "Pilih Jenis Tanah");
        mData.putString(SPINNER_ELEMEN_BERBAHAYA_JATUH_DATA_KEY, "Pilih Elemen Berbaya Jatuh");*/
    }
    @Override
    public Fragment createFragment() {
        return FemaDuaFragment.create(getKey());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {
        dest.add(new ReviewItem("Jenis Bangunan", mData.getString(SPINNER_JENIS_BANGUNAN_DATA_KEY), getKey(), -1));
        dest.add(new ReviewItem("Vertikal Bangunan", mData.getString(SPINNER_VERTIKAL_BANGUNAN_DATA_KEY), getKey(), -1));
        dest.add(new ReviewItem("Denah Bangunan", mData.getString(SPINNER_DENAH_BANGUNAN_DATA_KEY), getKey(), -1));
        dest.add(new ReviewItem("Pembuatan Berdasarkan", mData.getString(SPINNER_PEMBUATAN_BERDASARKAN_DATA_KEY), getKey(), -1));
        dest.add(new ReviewItem("Kepemilikan", mData.getString(SPINNER_KEPEMILIKAN_DATA_KEY), getKey(), -1));
        dest.add(new ReviewItem("Jumlah Pengguna", mData.getString(SPINNER_JUMLAH_PENGGUNA_DATA_KEY), getKey(), -1));
        dest.add(new ReviewItem("Jenis Tanah", mData.getString(SPINNER_JENIS_TANAH_DATA_KEY), getKey(), -1));
        dest.add(new ReviewItem("Berbahaya Jatuh", mData.getString(SPINNER_ELEMEN_BERBAHAYA_JATUH_DATA_KEY), getKey(), -1));
        if(mData.getString(SPINNER_ELEMEN_BERBAHAYA_JATUH_DATA_KEY).equals("Lainya")){
            dest.add(new ReviewItem("Lainya", mData.getString(LAINYA_DATA_KEY), getKey(), -1));
        }
    }

    @Override
    public boolean isCompleted() {
       /* return (!mData.getString(SPINNER_JENIS_BANGUNAN_DATA_KEY).equals("Pilih Jenis Bangunan")
                && !mData.getString(SPINNER_BENTUK_BANGUNAN_DATA_KEY).equals("Pilih Bentuk Bangunan")
                && !mData.getString(SPINNER_KEPEMILIKAN_DATA_KEY).equals("Pilih Kepemilikan")
                && !mData.getString(SPINNER_JUMLAH_PENGGUNA_DATA_KEY).equals("Pilih Jumlah Pengguna")
                && !mData.getString(SPINNER_JENIS_TANAH_DATA_KEY).equals("Pilih Jenis Tanah")
                && !mData.getString(SPINNER_ELEMEN_BERBAHAYA_JATUH_DATA_KEY).equals("Pilih Elemen Berbaya Jatuh")
        );*/
        return true;
    }
}
