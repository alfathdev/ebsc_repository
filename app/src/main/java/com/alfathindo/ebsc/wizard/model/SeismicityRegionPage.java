package com.alfathindo.ebsc.wizard.model;

import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.alfathindo.ebsc.entity.Fema;
import com.alfathindo.ebsc.entity.FemaCalculate;
import com.alfathindo.ebsc.entity.NilaiSeismicityRegion;
import com.alfathindo.ebsc.wizard.ui.SeismicityRegionFragment;
import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

/**
 * Created by safeimuslim on 7/25/16.
 */
public class SeismicityRegionPage extends Page {
    public static final String CITY_DATA_KEY = "city";
    public static final String LATITUDE_DATA_KEY = "latitude";
    public static final String LONGITUDE_DATA_KEY = "longitude";
    public static final String SS_DATA_KEY = "ss";
    public static final String SS1_DATA_KEY = "ss1";
    public static final String NILAI_DATA_KEY = "nilai";

    public NilaiSeismicityRegion nilaiSeismicityRegion;
    String kota;

    public SeismicityRegionPage(ModelCallbacks callbacks, String title) {
        super(callbacks, title);
        mData.putString(CITY_DATA_KEY, Fema.getmInstance()._kota);
        mData.putString(LATITUDE_DATA_KEY, Fema.getmInstance()._latitude);
        mData.putString(LONGITUDE_DATA_KEY, Fema.getmInstance()._longitude);
        hitungSeismicity();


    }
    @Override
    public Fragment createFragment() {
        return SeismicityRegionFragment.create(getKey());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {
        dest.add(new ReviewItem("Kota", mData.getString(CITY_DATA_KEY), getKey(), -1));
        dest.add(new ReviewItem("Latitude", mData.getString(LATITUDE_DATA_KEY), getKey(), -1));
        dest.add(new ReviewItem("Longitude", mData.getString(LONGITUDE_DATA_KEY), getKey(), -1));
        dest.add(new ReviewItem("SS", mData.getString(SS_DATA_KEY), getKey(), -1));
        dest.add(new ReviewItem("SS1", mData.getString(SS1_DATA_KEY), getKey(), -1));
        dest.add(new ReviewItem("Nilai", mData.getString(NILAI_DATA_KEY), getKey(), -1));
    }

    @Override
    public boolean isCompleted() {
        /*return (!TextUtils.isEmpty(mData.getString(CITY_DATA_KEY))
                && !TextUtils.isEmpty(mData.getString(LATITUDE_DATA_KEY))
                && !TextUtils.isEmpty(mData.getString(LONGITUDE_DATA_KEY))
                && !TextUtils.isEmpty(mData.getString(SS_DATA_KEY))
                && !TextUtils.isEmpty(mData.getString(SS1_DATA_KEY))
        );*/
        return true;
    }

    public void hitungSeismicity(){
        kota = Fema.getmInstance()._kota;
        String tampung = "";
        String ss = "0", s1 = "0";

        nilaiSeismicityRegion = new NilaiSeismicityRegion();
        for (int i=0; i<nilaiSeismicityRegion.getVariabel().length;i++){
            try {
                if (nilaiSeismicityRegion.getNilaiSeismicityRegion(kota) != null) {
                    try {
                        if(i == 1)
                            ss = nilaiSeismicityRegion.getNilaiSeismicityRegion(kota)[i];
                        if(i == 2)
                            s1 = nilaiSeismicityRegion.getNilaiSeismicityRegion(kota)[i];
                        tampung = tampung + nilaiSeismicityRegion.getVariabel()[i] + " : " + nilaiSeismicityRegion.getNilaiSeismicityRegion(kota)[i] + "\n";
                    } catch (ArrayIndexOutOfBoundsException e) {
                        Log.i("Error:", "ArrayIndexOutOfBoundsException" + e);
                        //Toast.makeText(get, "ArrayIndexOutOfBoundsException", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    tampung = "Nilai Seismicity Region pada kota " + kota + " tidak ada dalam database!";
                    break;
                }
            }catch (NullPointerException e){
                Log.i("Error:", "NullPointerException" + e);
                //Toast.makeText(fragmentActivity, "NullPointerException, kota: "+kota, Toast.LENGTH_SHORT).show();
            }
        }

        String csar = nilaiSeismicityRegion.csar(Double.parseDouble(ss),Double.parseDouble(s1));
        //save to global variabel
        FemaCalculate.getmInstance().ss = ss;
        FemaCalculate.getmInstance().s1 = s1;
        FemaCalculate.getmInstance().nilai = csar;

        mData.putString(SS_DATA_KEY, ss);
        mData.putString(SS1_DATA_KEY, s1);
        mData.putString(NILAI_DATA_KEY, csar);
    }
}
