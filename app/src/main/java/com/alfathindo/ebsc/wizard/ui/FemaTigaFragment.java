package com.alfathindo.ebsc.wizard.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alfathindo.ebsc.R;
import com.alfathindo.ebsc.activity.DrawImageActivity;
import com.alfathindo.ebsc.adapter.ImageListAdapter;
import com.alfathindo.ebsc.database.FemaRepo;
import com.alfathindo.ebsc.entity.Fema;
import com.alfathindo.ebsc.entity.ImageItem;
import com.alfathindo.ebsc.wizard.model.FemaTigaPage;
import com.tech.freak.wizardpager.ui.PageFragmentCallbacks;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by safeimuslim on 7/26/16.
 */
public class FemaTigaFragment extends Fragment implements View.OnClickListener {

    private static final String ARG_KEY = "key";

    private PageFragmentCallbacks mCallbacks;
    private String mKey;
    private FemaTigaPage mPage;
    private EditText ed_folder;
    public static final int CAMERA_CAPTURE = 0, SELECT_FILE = 1, CREATE_IMAGE = 2;
    String id_fema;
    Fema fema;
    protected FragmentActivity fragmentActivity;
    String folder = "ebsc_"+currentDateFormat();
    Button btnTakePhoto;
    GridView gridView;
    private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public  String GridViewDemo_ImagePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)+"/"+folder+"/";

    ArrayList<ImageItem> arrayImageItem = new ArrayList<>();

    /**
     * Cursor used to access the results from querying for images on the SD card.
     */
    private Cursor cursor;
    /*
     * Column index for the Thumbnails Image IDs.
     */
    private int columnIndex;


    public static FemaTigaFragment create(String key){
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);

        FemaTigaFragment fragment = new FemaTigaFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public FemaTigaFragment(){

    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        mKey = args.getString(ARG_KEY);
        mPage = (FemaTigaPage) mCallbacks.onGetPage(mKey);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_fema_tiga_page, container, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        ed_folder = (EditText) rootView.findViewById(R.id.ed_folder);
        if(mPage.getData().getString(FemaTigaPage.FOLDER_DATA_KEY) == null){
            mPage.getData().putString(FemaTigaPage.FOLDER_DATA_KEY, folder);
            Fema.getmInstance()._folder = folder;
            ed_folder.setText(folder);
        }else {
            ed_folder.setText(mPage.getData().getString(FemaTigaPage.FOLDER_DATA_KEY));
        }

        btnTakePhoto = (Button) rootView.findViewById(R.id.btnTakePhoto);
        gridView = (GridView) rootView.findViewById(R.id.gridview);

        try{
            if(getArguments().getString("id_fema") != null){
                id_fema = getArguments().getString("id_fema");
                FemaRepo femaRepo = new FemaRepo(fragmentActivity);
                fema = femaRepo.getFemaById(id_fema);
                folder = fema.get_folder();

            }
        }catch (NullPointerException e){
            Log.d("ERROR", "Id Fema: "+e);
        }
        reloadImage();
        return rootView;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (!(activity instanceof PageFragmentCallbacks)) {
            throw new ClassCastException("Activity must implement PageFragmentCallbacks");
        }
        fragmentActivity = (FragmentActivity) activity;
        mCallbacks = (PageFragmentCallbacks) activity;
    }

    public void reloadImage(){
        RetriveCapturedImagePath();
        gridView.setAdapter(new ImageListAdapter(fragmentActivity ,arrayImageItem));
    }
    public void createFolder(){
        File imageDirectory = new File(GridViewDemo_ImagePath);
        if (!imageDirectory.exists()) {
            if (imageDirectory.mkdirs()) {
                Log.i("CREATE DIRECTORY", " SUKSES");
            } else {
                Log.i("CREATE DIRECTORY", " GAGAL");
            }
        } else {
            Log.i("CREATE DIRECTORY FEMA", " FOLDER SUDAH ADA");
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //create folder
        createFolder();
        reloadImage();
        if (resultCode == Activity.RESULT_OK) {
            //Toast.makeText(fragmentActivity,"RESULT_OK",Toast.LENGTH_SHORT).show();
            if (requestCode == CAMERA_CAPTURE) {
                //Toast.makeText(fragmentActivity,"CAMERA_CAPTURE",Toast.LENGTH_SHORT).show();
                takeImageFromCamera(data);
            } else if (requestCode == SELECT_FILE && data != null) {
                takeImageFromMemory(data);
            } else if(requestCode == CREATE_IMAGE && data !=null){
                // TODO: 7/26/16 create create image
                reloadImage();
                Toast.makeText(fragmentActivity, "Gambar berhasil dibuat", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void takeImageFromCamera(Intent data){
        if(isDeviceSupportCamera()) {
            Bundle extras = data.getExtras();
            Bitmap thePic = extras.getParcelable("data");
            String imgcurTime = dateFormat.format(new Date());
            String _path = GridViewDemo_ImagePath + "image_" + currentDateFormat() + ".jpg";
            try {
                FileOutputStream out = new FileOutputStream(_path);
                BufferedOutputStream bof = new BufferedOutputStream(out);
                thePic.compress(Bitmap.CompressFormat.JPEG, 100, bof);
                //bof.flush();  //bof.close();
                Log.i("SAVE IMAGE", "BERHASIL");
            } catch (FileNotFoundException e) {
                e.getMessage();
            }
            reloadImage();
        }else{
            Toast.makeText(fragmentActivity,"Device Tidak Mendukung Kamera!",Toast.LENGTH_SHORT).show();
        }
    }

    public void takeImageFromMemory(Intent data){
        //Toast.makeText(fragmentActivity,"_Path: ",Toast.LENGTH_SHORT).show();

        Uri selectedImageUri = data.getData();
        String[] projection = { MediaStore.MediaColumns.DATA };
        CursorLoader cursorLoader = new CursorLoader(fragmentActivity,selectedImageUri, projection, null, null,
                null);
        Cursor cursor =cursorLoader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();


        String selectedImagePath = cursor.getString(column_index);
        //Toast.makeText(fragmentActivity,"_Path: "+selectedImagePath,Toast.LENGTH_SHORT).show();

        String outputPath = GridViewDemo_ImagePath + "image_" + currentDateFormat() + ".jpg";
        InputStream in = null;
        OutputStream out = null;

        try {
            in = new FileInputStream(selectedImagePath);
            out = new FileOutputStream(outputPath);
            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            in = null;
            // write the output file (You have now copied the file)
            out.flush();
            out.close();
            out = null;
        }  catch (FileNotFoundException fnfe1) {
            Log.e("tag", fnfe1.getMessage());
        }
        catch (Exception e) {
            Log.e("tag", e.getMessage());
        }
        cursor.close();
        reloadImage();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        ed_folder.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mPage.getData().putString(FemaTigaPage.FOLDER_DATA_KEY,
                        (editable != null) ? editable.toString() : null);
                mPage.notifyDataChanged();
            }
        });

        btnTakePhoto.setOnClickListener(this);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ImageItem item = arrayImageItem.get(position);

                Dialog dialog = new Dialog(fragmentActivity);
                dialog.setContentView(R.layout.detail_image_activity);
                dialog.setTitle("Detail gambar");
                ImageView image = (ImageView) dialog.findViewById(R.id.image);
                TextView title = (TextView) dialog.findViewById(R.id.title);
                title.setText(item.getTitle());
                image.setImageBitmap(item.getImage());
                int width = (int)(getResources().getDisplayMetrics().widthPixels*0.90);
                int height = (int)(getResources().getDisplayMetrics().heightPixels*0.90);
                dialog.getWindow().setLayout(width,height);
                dialog.show();
            }
        });
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);

        // In a future update to the support library, this should override setUserVisibleHint
        // instead of setMenuVisibility.
        if (ed_folder != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            if (!menuVisible) {
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
            }
        }
    }

    private String currentDateFormat(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HH_mm_ss");
        String  currentTimeStamp = dateFormat.format(new Date());
        return currentTimeStamp;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnTakePhoto:
                selectImage();
                break;
            default:
                break;
        }

    }

    private void selectImage() {
        final CharSequence[] items = { "Kamera", "SD Card", "Sketsa Denah dan Elevasi","Batal" };
        AlertDialog.Builder builder = new AlertDialog.Builder(fragmentActivity);
        builder.setTitle("Tambah Gambar");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Kamera")) {
                    try {
                        //use standard intent to capture an image
                        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        //we will handle the returned data in onActivityResult
                        startActivityForResult(captureIntent, CAMERA_CAPTURE);
                    } catch(ActivityNotFoundException anfe){
                        //display an error message
                        String errorMessage = "Whoops - your device doesn't support capturing images!";
                        Toast toast = Toast.makeText(fragmentActivity, errorMessage, Toast.LENGTH_SHORT);
                        toast.show();
                    }
                } else if (items[item].equals("SD Card")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, "Pilih Gambar"),SELECT_FILE);
                } else if(items[item].equals("Sketsa Denah dan Elevasi")){
                    Intent intent = new Intent(getActivity(), DrawImageActivity.class);
                    intent.putExtra("folder",GridViewDemo_ImagePath);
                    startActivityForResult(intent, CREATE_IMAGE);
                }else if (items[item].equals("Batal")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    private boolean isDeviceSupportCamera(){
        if(getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            return true;
        }else {
            return false;
        }
    }

    private void RetriveCapturedImagePath() {
        arrayImageItem = new ArrayList<>();
        File f = new File(GridViewDemo_ImagePath);
        if (f.exists()) {
            File[] files=f.listFiles();
            Arrays.sort(files);

            for(int i=0; i<files.length; i++){
                File file = files[i];
                if(file.isDirectory())
                    continue;
                Bitmap bm = BitmapFactory.decodeFile(new File(file.getPath()).getAbsolutePath());
                ImageItem imageItem = new ImageItem(bm,file.getName());
                arrayImageItem.add(imageItem);
            }
        }
     }
}
