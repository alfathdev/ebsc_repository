package com.alfathindo.ebsc.wizard.model;

import android.support.v4.app.Fragment;
import android.text.TextUtils;

import com.alfathindo.ebsc.wizard.ui.FemaDuaFragment;
import com.alfathindo.ebsc.wizard.ui.FemaTigaFragment;
import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

/**
 * Created by safeimuslim on 7/26/16.
 */
public class FemaTigaPage extends Page {
    public static final String FOLDER_DATA_KEY = "folder";

    public FemaTigaPage(ModelCallbacks callbacks, String title) {
        super(callbacks, title);
    }
    @Override
    public Fragment createFragment() {
        return FemaTigaFragment.create(getKey());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {
        dest.add(new ReviewItem("Folder", mData.getString(FOLDER_DATA_KEY), getKey(), -1));
    }

    @Override
    public boolean isCompleted() {
        //return (!TextUtils.isEmpty(mData.getString(FOLDER_DATA_KEY)));
        return true;
    }
}
