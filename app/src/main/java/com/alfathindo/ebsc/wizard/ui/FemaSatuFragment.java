package com.alfathindo.ebsc.wizard.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.alfathindo.ebsc.R;
import com.alfathindo.ebsc.entity.Fema;
import com.alfathindo.ebsc.wizard.model.FemaSatuPage;
import com.tech.freak.wizardpager.ui.PageFragmentCallbacks;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by safeimuslim on 7/25/16.
 */
public class FemaSatuFragment extends Fragment {
    private static final String ARG_KEY = "key";

    private PageFragmentCallbacks mCallbacks;
    private String mKey;
    private FemaSatuPage mPage;
    private EditText ed_nama_bangunan,ed_alamat,ed_kode_pos,ed_identitas_lain,ed_no_stories,ed_tahun_pembangunan,ed_screener,ed_tanggal,ed_jumlah_lantai,ed_kegunaan;

    public static FemaSatuFragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);

        FemaSatuFragment fragment = new FemaSatuFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public FemaSatuFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        mKey = args.getString(ARG_KEY);
        mPage = (FemaSatuPage) mCallbacks.onGetPage(mKey);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_fema_satu_page, container, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        ed_nama_bangunan = ((EditText) rootView.findViewById(R.id.ed_nama_bangunan));
        ed_nama_bangunan.setText(mPage.getData().getString(FemaSatuPage.NAMA_BANGUNAN_DATA_KEY));

        ed_alamat = ((EditText) rootView.findViewById(R.id.ed_alamat));
        ed_alamat.setText(mPage.getData().getString(FemaSatuPage.ALAMAT_DATA_KEY));

        ed_kode_pos = ((EditText) rootView.findViewById(R.id.ed_kode_pos ));
        ed_kode_pos.setText(mPage.getData().getString(FemaSatuPage.KODE_POS_DATA_KEY));

        ed_identitas_lain= ((EditText) rootView.findViewById(R.id.ed_identitas_lain));
        ed_identitas_lain.setText(mPage.getData().getString(FemaSatuPage.IDENTITAS_LAIN_DATA_KEY));

        ed_no_stories= ((EditText) rootView.findViewById(R.id.ed_no_stories));
        ed_no_stories.setText(mPage.getData().getString(FemaSatuPage.NO_STORIES_DATA_KEY));

        ed_tahun_pembangunan= ((EditText) rootView.findViewById(R.id.ed_tahun_pembangunan));
        ed_tahun_pembangunan.setText(mPage.getData().getString(FemaSatuPage.TAHUN_PEMBANGUNAN_DATA_KEY));

        ed_screener= ((EditText) rootView.findViewById(R.id.ed_screener));
        ed_screener.setText(mPage.getData().getString(FemaSatuPage.SCREENER_DATA_KEY));

        ed_tanggal= ((EditText) rootView.findViewById(R.id.ed_tanggal));
        if(mPage.getData().getString(FemaSatuPage.TANGGAL_KEY) == null){
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            String cur_date = sdf.format(new Date());
            mPage.getData().putString(FemaSatuPage.TANGGAL_KEY, cur_date);
            Fema.getmInstance()._tanggal = cur_date;
            ed_tanggal.setText(cur_date);
        }else {
            ed_tanggal.setText(mPage.getData().getString(FemaSatuPage.TANGGAL_KEY));
        }



        ed_jumlah_lantai= ((EditText) rootView.findViewById(R.id.ed_jumlah_lantai));
        ed_jumlah_lantai.setText(mPage.getData().getString(FemaSatuPage.JUMLAH_LANTAI_DATA_KEY));

        ed_kegunaan= ((EditText) rootView.findViewById(R.id.ed_kegunaan));
        ed_kegunaan.setText(mPage.getData().getString(FemaSatuPage.KEGUNAAN_DATA_KEY));


        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (!(activity instanceof PageFragmentCallbacks)) {
            throw new ClassCastException("Activity must implement PageFragmentCallbacks");
        }

        mCallbacks = (PageFragmentCallbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ed_nama_bangunan.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mPage.getData().putString(FemaSatuPage.NAMA_BANGUNAN_DATA_KEY,
                        (editable != null) ? editable.toString() : null);
                Fema.getmInstance()._nama_bangunan = editable.toString();
                mPage.notifyDataChanged();

            }
        });

        ed_alamat.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mPage.getData().putString(FemaSatuPage.ALAMAT_DATA_KEY,
                        (editable != null) ? editable.toString() : null);
                Fema.getmInstance()._alamat = editable.toString();
                mPage.notifyDataChanged();

            }
        });

        ed_kode_pos.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mPage.getData().putString(FemaSatuPage.KODE_POS_DATA_KEY,
                        (editable != null) ? editable.toString() : null);
                Fema.getmInstance()._kode_pos = editable.toString();
                mPage.notifyDataChanged();

            }
        });

        ed_identitas_lain.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mPage.getData().putString(FemaSatuPage.IDENTITAS_LAIN_DATA_KEY,
                        (editable != null) ? editable.toString() : null);

                Fema.getmInstance()._identitas_lain = editable.toString();
                mPage.notifyDataChanged();

            }
        });

        ed_no_stories.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mPage.getData().putString(FemaSatuPage.NO_STORIES_DATA_KEY,
                        (editable != null) ? editable.toString() : null);

                Fema.getmInstance()._no_stories = editable.toString();
                mPage.notifyDataChanged();

            }
        });
        ed_tahun_pembangunan.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mPage.getData().putString(FemaSatuPage.TAHUN_PEMBANGUNAN_DATA_KEY,
                        (editable != null) ? editable.toString() : null);

                Fema.getmInstance()._tahun_pembangunan = editable.toString();
                mPage.notifyDataChanged();

            }
        });

        ed_screener.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mPage.getData().putString(FemaSatuPage.SCREENER_DATA_KEY,
                        (editable != null) ? editable.toString() : null);
                Fema.getmInstance()._screener = editable.toString();
                mPage.notifyDataChanged();
            }
        });

        ed_tanggal.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mPage.getData().putString(FemaSatuPage.TANGGAL_KEY,
                        (editable != null) ? editable.toString() : null);
                Fema.getmInstance()._tanggal = editable.toString();
                mPage.notifyDataChanged();
            }
        });

        ed_jumlah_lantai.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mPage.getData().putString(FemaSatuPage.JUMLAH_LANTAI_DATA_KEY,
                        (editable != null) ? editable.toString() : null);
                Fema.getmInstance()._jumlah_lantai = editable.toString();
                mPage.notifyDataChanged();
            }
        });

        ed_kegunaan.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mPage.getData().putString(FemaSatuPage.KEGUNAAN_DATA_KEY,
                        (editable != null) ? editable.toString() : null);
                Fema.getmInstance()._kegunaan = editable.toString();
                mPage.notifyDataChanged();
            }
        });
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);

        // In a future update to the support library, this should override setUserVisibleHint
        // instead of setMenuVisibility.
        if (ed_nama_bangunan != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            if (!menuVisible) {
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
            }
        }
    }
}
