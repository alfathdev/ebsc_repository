package com.alfathindo.ebsc.wizard.model;

import android.support.v4.app.Fragment;
import android.text.TextUtils;

import com.alfathindo.ebsc.entity.Fema;
import com.alfathindo.ebsc.wizard.ui.FemaSatuFragment;
import com.alfathindo.ebsc.wizard.ui.SeismicityRegionFragment;
import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

/**
 * Created by safeimuslim on 7/25/16.
 */
public class FemaSatuPage extends Page {
    public static final String NAMA_BANGUNAN_DATA_KEY = "nama_bangunan";
    public static final String ALAMAT_DATA_KEY = "alamat";
    public static final String KODE_POS_DATA_KEY = "kode_pos";
    public static final String IDENTITAS_LAIN_DATA_KEY = "identitas_lain";
    public static final String NO_STORIES_DATA_KEY = "no_stories";
    public static final String TAHUN_PEMBANGUNAN_DATA_KEY = "tahun_pembangunan";
    public static final String SCREENER_DATA_KEY = "screener";
    public static final String TANGGAL_KEY = "tanggal";
    public static final String JUMLAH_LANTAI_DATA_KEY = "jumlah_lantai";
    public static final String KEGUNAAN_DATA_KEY = "kegunaan";

    public FemaSatuPage(ModelCallbacks callbacks, String title) {
        super(callbacks, title);
        mData.putString(KODE_POS_DATA_KEY , Fema.getmInstance()._kode_pos);
    }
    @Override
    public Fragment createFragment() {
        return FemaSatuFragment.create(getKey());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {
        dest.add(new ReviewItem("Nama Bangunan", mData.getString(NAMA_BANGUNAN_DATA_KEY), getKey(), -1));
        dest.add(new ReviewItem("Tahun Pembangunan", mData.getString(TAHUN_PEMBANGUNAN_DATA_KEY), getKey(), -1));
        dest.add(new ReviewItem("Alamat", mData.getString(ALAMAT_DATA_KEY), getKey(), -1));
        dest.add(new ReviewItem("Kode Pos", mData.getString(KODE_POS_DATA_KEY), getKey(), -1));
        dest.add(new ReviewItem("Identitas Lain", mData.getString(IDENTITAS_LAIN_DATA_KEY), getKey(), -1));
        dest.add(new ReviewItem("No Stories", mData.getString(NO_STORIES_DATA_KEY), getKey(), -1));
        dest.add(new ReviewItem("Screener", mData.getString(SCREENER_DATA_KEY), getKey(), -1));
        dest.add(new ReviewItem("Tanggal", mData.getString(TANGGAL_KEY), getKey(), -1));
        dest.add(new ReviewItem("Luas Area", mData.getString(JUMLAH_LANTAI_DATA_KEY), getKey(), -1));
        dest.add(new ReviewItem("Kegunaan", mData.getString(KEGUNAAN_DATA_KEY), getKey(), -1));



    }

    @Override
    public boolean isCompleted() {
        /*return (!TextUtils.isEmpty(mData.getString(NAMA_BANGUNAN_DATA_KEY))
                && !TextUtils.isEmpty(mData.getString(ALAMAT_DATA_KEY))
                && !TextUtils.isEmpty(mData.getString(KODE_POS_DATA_KEY))
                && !TextUtils.isEmpty(mData.getString(IDENTITAS_LAIN_DATA_KEY))
                && !TextUtils.isEmpty(mData.getString(NO_STORIES_DATA_KEY))
                && !TextUtils.isEmpty(mData.getString(TAHUN_PEMBANGUNAN_DATA_KEY))
                && !TextUtils.isEmpty(mData.getString(SCREENER_DATA_KEY))
                && !TextUtils.isEmpty(mData.getString(TANGGAL_KEY))
                && !TextUtils.isEmpty(mData.getString(JUMLAH_LANTAI_DATA_KEY))
                && !TextUtils.isEmpty(mData.getString(KEGUNAAN_DATA_KEY))
        );*/
        return true;
    }
}
