package com.alfathindo.ebsc.wizard.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.alfathindo.ebsc.R;
import com.alfathindo.ebsc.entity.Fema;
import com.alfathindo.ebsc.wizard.model.FemaDuaPage;
import com.alfathindo.ebsc.wizard.model.FemaSatuPage;
import com.tech.freak.wizardpager.ui.PageFragmentCallbacks;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by safeimuslim on 7/25/16.
 */
public class FemaDuaFragment extends Fragment {

    private static final String ARG_KEY = "key";

    FragmentActivity fragmentActivity;
    private PageFragmentCallbacks mCallbacks;
    private String mKey;
    private FemaDuaPage mPage;
    private Spinner spinnerJenisBangunan,spinnerBentukBangunan,spinnerKepemilikan,spinnerJumlahPengguna,spinnerJenisTanah,spinnerElemenBerbahayaJatuh,spinnerVertikalBangunan,spinnerDenahBangunan,spinnerPembuatanBerdasarkan;
    private EditText ed_lainya;
    String[] jenis_bangunan_save = {"Pilih Jenis Bangunan","W1", "W2", "S1","S2", "S3", "S4", "S5", "C1", "C2", "C3", "PC1", "PC2", "RM1", "RM2","URM"};



    public static FemaDuaFragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);

        FemaDuaFragment fragment = new FemaDuaFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public FemaDuaFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        mKey = args.getString(ARG_KEY);
        mPage = (FemaDuaPage) mCallbacks.onGetPage(mKey);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_fema_dua_page, container, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        spinnerPembuatanBerdasarkan = (Spinner) rootView.findViewById(R.id.spinnerPembuatanBerdasarkan);
        String val_pembuatan_berdasarkan = mPage.getData().getString(FemaDuaPage.SPINNER_PEMBUATAN_BERDASARKAN_DATA_KEY);
        setSpinnerPembuatanBerdasarkan(rootView, val_pembuatan_berdasarkan);

        spinnerJenisBangunan = (Spinner) rootView.findViewById(R.id.spinnerJenisBangunan);
        String val_jenis_bangunan = mPage.getData().getString(FemaDuaPage.SPINNER_JENIS_BANGUNAN_DATA_KEY);
        setSpinnerJenisBangunan(rootView, val_jenis_bangunan);

        spinnerVertikalBangunan  = (Spinner) rootView.findViewById(R.id.spinnerVertikalBangunan);
        String val_vertikal_bangunan = mPage.getData().getString(FemaDuaPage.SPINNER_VERTIKAL_BANGUNAN_DATA_KEY);
        setSpinnerVertikalBangunan(rootView, val_vertikal_bangunan);

        spinnerDenahBangunan = (Spinner) rootView.findViewById(R.id.spinnerDenahBangunan);
        String val_denah_bangunan = mPage.getData().getString(FemaDuaPage.SPINNER_DENAH_BANGUNAN_DATA_KEY);
        setSpinnerDenahBangunan(rootView, val_denah_bangunan);

        spinnerBentukBangunan = (Spinner) rootView.findViewById(R.id.spinnerBentukBangunan);
        String val_bentuk_bangunan= mPage.getData().getString(FemaDuaPage.SPINNER_JENIS_BANGUNAN_DATA_KEY);
        setSpinnerBentukBangunan(rootView, val_bentuk_bangunan);

        spinnerKepemilikan = (Spinner) rootView.findViewById(R.id.spinnerKepemilikan);
        String val_kepemilikan = mPage.getData().getString(FemaDuaPage.SPINNER_KEPEMILIKAN_DATA_KEY);
        setSpinnerKepemilikan(rootView, val_kepemilikan);

        spinnerJumlahPengguna = (Spinner) rootView.findViewById(R.id.spinnerJumlahPengguna);
        String val_jumlah_pengguna = mPage.getData().getString(FemaDuaPage.SPINNER_JUMLAH_PENGGUNA_DATA_KEY);
        setSpinnerJumlahPengguna(rootView, val_jumlah_pengguna);

        spinnerJenisTanah = (Spinner) rootView.findViewById(R.id.spinnerJenisTanah);
        String val_jenis_tanah = mPage.getData().getString(FemaDuaPage.SPINNER_JENIS_TANAH_DATA_KEY);
        setSpinnerJenisTanah(rootView, val_jenis_tanah);

        spinnerElemenBerbahayaJatuh = (Spinner) rootView.findViewById(R.id.spinnerElemen_berbahayaJatuh);
        String val_elemen_berbahaya_jatuh = mPage.getData().getString(FemaDuaPage.SPINNER_ELEMEN_BERBAHAYA_JATUH_DATA_KEY);
        setSpinnerElemenBerbahayaJatuh(rootView, val_elemen_berbahaya_jatuh);

        ed_lainya = (EditText) rootView.findViewById(R.id.ed_lainya);

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (!(activity instanceof PageFragmentCallbacks)) {
            throw new ClassCastException("Activity must implement PageFragmentCallbacks");
        }
        fragmentActivity = (FragmentActivity) activity;

        mCallbacks = (PageFragmentCallbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        spinnerPembuatanBerdasarkan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mPage.getData().putString(FemaDuaPage.SPINNER_PEMBUATAN_BERDASARKAN_DATA_KEY, spinnerPembuatanBerdasarkan.getSelectedItem().toString());
                Fema.getmInstance()._pembuatan_berdasarkan = spinnerPembuatanBerdasarkan.getSelectedItem().toString();
                mPage.notifyDataChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerJenisBangunan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //mPage.getData().putString(FemaDuaPage.SPINNER_JENIS_BANGUNAN_DATA_KEY, spinnerJenisBangunan.getSelectedItem().toString());
                //Fema.getmInstance()._jenis_bangunan = spinnerJenisBangunan.getSelectedItem().toString();
                try{
                    mPage.getData().putString(FemaDuaPage.SPINNER_JENIS_BANGUNAN_DATA_KEY, jenis_bangunan_save[position]);
                    Fema.getmInstance()._jenis_bangunan = jenis_bangunan_save[position];
                    mPage.notifyDataChanged();

                }catch (ArrayIndexOutOfBoundsException e){}
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerVertikalBangunan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mPage.getData().putString(FemaDuaPage.SPINNER_VERTIKAL_BANGUNAN_DATA_KEY, spinnerVertikalBangunan.getSelectedItem().toString());
                Fema.getmInstance()._vertikal_bangunan= spinnerVertikalBangunan.getSelectedItem().toString();
                mPage.notifyDataChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerDenahBangunan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mPage.getData().putString(FemaDuaPage.SPINNER_DENAH_BANGUNAN_DATA_KEY, spinnerDenahBangunan.getSelectedItem().toString());
                Fema.getmInstance()._denah_bangunan= spinnerDenahBangunan.getSelectedItem().toString();
                mPage.notifyDataChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spinnerBentukBangunan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mPage.getData().putString(FemaDuaPage.SPINNER_BENTUK_BANGUNAN_DATA_KEY, spinnerBentukBangunan.getSelectedItem().toString());
                Fema.getmInstance()._bentuk_bangunan = spinnerBentukBangunan.getSelectedItem().toString();
                mPage.notifyDataChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerKepemilikan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mPage.getData().putString(FemaDuaPage.SPINNER_KEPEMILIKAN_DATA_KEY, spinnerKepemilikan.getSelectedItem().toString());
                Fema.getmInstance()._kepemilikan = spinnerKepemilikan.getSelectedItem().toString();
                mPage.notifyDataChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerJumlahPengguna.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mPage.getData().putString(FemaDuaPage.SPINNER_JUMLAH_PENGGUNA_DATA_KEY, spinnerJumlahPengguna.getSelectedItem().toString());
                Fema.getmInstance()._jumlah_pengguna = spinnerJumlahPengguna.getSelectedItem().toString();
                mPage.notifyDataChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerJenisTanah.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mPage.getData().putString(FemaDuaPage.SPINNER_JENIS_TANAH_DATA_KEY, spinnerJenisTanah.getSelectedItem().toString());
                Fema.getmInstance()._jenis_tanah = spinnerJenisTanah.getSelectedItem().toString();
                mPage.notifyDataChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerElemenBerbahayaJatuh.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(spinnerElemenBerbahayaJatuh.getSelectedItem().toString().equals("Lainya")){
                    ed_lainya.setEnabled(true);
                    Fema.getmInstance()._berbahaya_jatuh = spinnerElemenBerbahayaJatuh.getSelectedItem().toString();
                    ed_lainya.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {}

                        @Override
                        public void afterTextChanged(Editable editable) {
                            mPage.getData().putString(FemaDuaPage.LAINYA_DATA_KEY,
                                    (editable != null) ? editable.toString() : null);
                            Fema.getmInstance()._text_berbahaya_jatuh = editable.toString();
                            mPage.notifyDataChanged();
                        }
                    });
                    mPage.getData().putString(FemaDuaPage.SPINNER_ELEMEN_BERBAHAYA_JATUH_DATA_KEY, spinnerElemenBerbahayaJatuh.getSelectedItem().toString());
                    mPage.notifyDataChanged();

                }else{
                    mPage.getData().putString(FemaDuaPage.SPINNER_ELEMEN_BERBAHAYA_JATUH_DATA_KEY, spinnerElemenBerbahayaJatuh.getSelectedItem().toString());
                    Fema.getmInstance()._berbahaya_jatuh = spinnerElemenBerbahayaJatuh.getSelectedItem().toString();
                    mPage.notifyDataChanged();
                    ed_lainya.setEnabled(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);

        // In a future update to the support library, this should override setUserVisibleHint
        // instead of setMenuVisibility.
        if (spinnerJenisBangunan != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            if (!menuVisible) {
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
            }
        }
    }

    public void setSpinnerPembuatanBerdasarkan(View view, String value){
        List<String> itemSpinnerPembuatanBerdasarkan = new ArrayList<String>();
        itemSpinnerPembuatanBerdasarkan.add("Pilih Pembuatan Berdasarkan");
        itemSpinnerPembuatanBerdasarkan.add("Sebelum SNI");
        itemSpinnerPembuatanBerdasarkan.add("Sesudah SNI");
        ArrayAdapter<String> itemSpinnerPembuatanBerdasarkanAdapter = new ArrayAdapter<String>(fragmentActivity,R.layout.spinner_row, itemSpinnerPembuatanBerdasarkan);
        itemSpinnerPembuatanBerdasarkanAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spinnerPembuatanBerdasarkan.setAdapter(itemSpinnerPembuatanBerdasarkanAdapter);
        if(value != null){
            int spinnerPosition = itemSpinnerPembuatanBerdasarkanAdapter.getPosition(value);
            spinnerPembuatanBerdasarkan.setSelection(spinnerPosition);
        }

    }
    public void setSpinnerJenisBangunan(View view, String value){
        List<String> itemSpinnerJenisBangunan = new ArrayList<String>();
        itemSpinnerJenisBangunan.add("Pilih Jenis Bangunan");
        itemSpinnerJenisBangunan.add("W1 = rangka kayu dengan luas ≤ 5000 ft2");
        itemSpinnerJenisBangunan.add("W2 = rangka kayu dengan luas ≥ 5000 ft2");
        itemSpinnerJenisBangunan.add("S1 = rangka baja");
        itemSpinnerJenisBangunan.add("S2 = rangka baja dengan pengaku (bracing)");
        itemSpinnerJenisBangunan.add("S3 = rangka logam");
        itemSpinnerJenisBangunan.add("S4 = rangka baja dengan dinding geser");
        itemSpinnerJenisBangunan.add("S5 = rangka baja dengan pasangan batu bata");
        itemSpinnerJenisBangunan.add("C1 = rangka balok dan kolom dari beton bertulang");
        itemSpinnerJenisBangunan.add("C2 = rangka dinding geser dari beton bertulang");
        itemSpinnerJenisBangunan.add("C3 = rangka dinding batu bata tidak diperkuat ");
        itemSpinnerJenisBangunan.add("PC1 = dinding panel beton yang diletakan di tanah");
        itemSpinnerJenisBangunan.add("PC2 = rangka beton pracetak");
        itemSpinnerJenisBangunan.add("RM1= pasangan bata diperkuat dengan semen");
        itemSpinnerJenisBangunan.add("RM2 = Rangka Atap dari kayu dan baja");
        itemSpinnerJenisBangunan.add("URM = rangka struktur dinding dari bearing wall");
        /*itemSpinnerJenisBangunan.add("W1");
        itemSpinnerJenisBangunan.add("W2");
        itemSpinnerJenisBangunan.add("S1");
        itemSpinnerJenisBangunan.add("S2");
        itemSpinnerJenisBangunan.add("S3");
        itemSpinnerJenisBangunan.add("S4");
        itemSpinnerJenisBangunan.add("S5");
        itemSpinnerJenisBangunan.add("C1");
        itemSpinnerJenisBangunan.add("C2");
        itemSpinnerJenisBangunan.add("C3");
        itemSpinnerJenisBangunan.add("PC1");
        itemSpinnerJenisBangunan.add("PC2");
        itemSpinnerJenisBangunan.add("RM1");
        itemSpinnerJenisBangunan.add("RM2");
        itemSpinnerJenisBangunan.add("URM");*/

        ArrayAdapter<String> itemSpinnerJenisBangunanAdapter = new ArrayAdapter<String>(fragmentActivity, R.layout.spinner_row, itemSpinnerJenisBangunan);
        itemSpinnerJenisBangunanAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spinnerJenisBangunan.setAdapter(itemSpinnerJenisBangunanAdapter);

        if(value != null){
            int spinnerPosition = itemSpinnerJenisBangunanAdapter.getPosition(value);
            spinnerJenisBangunan.setSelection(spinnerPosition);
        }
    }

    public void setSpinnerVertikalBangunan(View view, String value){
        List<String> itemSpinnerVertikalBangunan = new ArrayList<String>();
        itemSpinnerVertikalBangunan.add("Pilih Vertikal Bangunan");
        itemSpinnerVertikalBangunan.add("Beraturan");
        itemSpinnerVertikalBangunan.add("Tidak Beraturan");
        ArrayAdapter<String> itemSpinnerVertikalBangunanAdapter = new ArrayAdapter<String>(fragmentActivity,R.layout.spinner_row, itemSpinnerVertikalBangunan);
        itemSpinnerVertikalBangunanAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spinnerVertikalBangunan.setAdapter(itemSpinnerVertikalBangunanAdapter);
        if(value != null){
            int spinnerPosition = itemSpinnerVertikalBangunanAdapter.getPosition(value);
            spinnerVertikalBangunan.setSelection(spinnerPosition);
        }
    }

    public void setSpinnerDenahBangunan(View view, String value){
        List<String> itemSpinnerDenahBangunan = new ArrayList<String>();
        itemSpinnerDenahBangunan.add("Pilih Denah Bangunan");
        itemSpinnerDenahBangunan.add("Simetris");
        itemSpinnerDenahBangunan.add("Tidak Simetris");
        ArrayAdapter<String> itemSpinnerDenahBangunanAdapter = new ArrayAdapter<String>(fragmentActivity,R.layout.spinner_row, itemSpinnerDenahBangunan);
        itemSpinnerDenahBangunanAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spinnerDenahBangunan.setAdapter(itemSpinnerDenahBangunanAdapter);
        if(value != null){
            int spinnerPosition = itemSpinnerDenahBangunanAdapter.getPosition(value);
            spinnerDenahBangunan.setSelection(spinnerPosition);
        }
    }

    public void setSpinnerBentukBangunan(View view, String value){
        List<String> itemSpinnerBentukBangunan = new ArrayList<String>();
        itemSpinnerBentukBangunan.add("Pilih Bentuk Bangunan");
        itemSpinnerBentukBangunan.add("Simetris");
        itemSpinnerBentukBangunan.add("Tidak Simetris");

        ArrayAdapter<String> itemSpinnerBentukBangunanAdapter = new ArrayAdapter<String>(fragmentActivity,R.layout.spinner_row, itemSpinnerBentukBangunan);
        itemSpinnerBentukBangunanAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spinnerBentukBangunan.setAdapter(itemSpinnerBentukBangunanAdapter);
        if(value != null){
            int spinnerPosition = itemSpinnerBentukBangunanAdapter.getPosition(value);
            spinnerBentukBangunan.setSelection(spinnerPosition);
        }
    }

    public void setSpinnerKepemilikan(View view, String value){
        List<String> itemSpinnerKepemilikan = new ArrayList<String>();
        itemSpinnerKepemilikan.add("Pilih Kepemilikan");
        itemSpinnerKepemilikan.add("Pertemuan");
        itemSpinnerKepemilikan.add("Industri");
        itemSpinnerKepemilikan.add("Perniagaan");
        itemSpinnerKepemilikan.add("Kantor");
        itemSpinnerKepemilikan.add("Gawat Darurat");
        itemSpinnerKepemilikan.add("Perumahaan");
        itemSpinnerKepemilikan.add("Pemerintahan");
        itemSpinnerKepemilikan.add("Sekolah");
        itemSpinnerKepemilikan.add("Sejarah");

        ArrayAdapter<String> itemSpinnerKepemilikanAdapter = new ArrayAdapter<String>(fragmentActivity,R.layout.spinner_row, itemSpinnerKepemilikan);
        itemSpinnerKepemilikanAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spinnerKepemilikan.setAdapter(itemSpinnerKepemilikanAdapter);
        if(value != null){
            int spinnerPosition = itemSpinnerKepemilikanAdapter.getPosition(value);
            spinnerKepemilikan.setSelection(spinnerPosition);
        }

    }
    public void setSpinnerJumlahPengguna(View view, String value){
        List<String> itemSpinnerKepemilikan = new ArrayList<String>();
        itemSpinnerKepemilikan.add("Pilih Jumlah Pengguna");
        itemSpinnerKepemilikan.add("0 - 10");
        itemSpinnerKepemilikan.add("11 - 100");
        itemSpinnerKepemilikan.add("101 - 1000");
        itemSpinnerKepemilikan.add("> 1000");

        ArrayAdapter<String> itemSpinnerKepemilikanAdapter = new ArrayAdapter<String>(fragmentActivity,R.layout.spinner_row, itemSpinnerKepemilikan);
        itemSpinnerKepemilikanAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spinnerJumlahPengguna.setAdapter(itemSpinnerKepemilikanAdapter);
        if(value != null){
            int spinnerPosition = itemSpinnerKepemilikanAdapter.getPosition(value);
            spinnerJumlahPengguna.setSelection(spinnerPosition);
        }

    }
    public void setSpinnerJenisTanah(View view, String value){
        List<String> itemSpinnerKepemilikan = new ArrayList<String>();
        itemSpinnerKepemilikan.add("Pilih Jenis Tanah");
        itemSpinnerKepemilikan.add("A - Batuan Keras");
        itemSpinnerKepemilikan.add("B - Batuan Sedang");
        itemSpinnerKepemilikan.add("C - Tanah Keras");
        itemSpinnerKepemilikan.add("D - Tanah Kaku");
        itemSpinnerKepemilikan.add("E - Tanah Lunak");
        itemSpinnerKepemilikan.add("F - Tanah Rentan");


        ArrayAdapter<String> itemSpinnerKepemilikanAdapter = new ArrayAdapter<String>(fragmentActivity,R.layout.spinner_row, itemSpinnerKepemilikan);
        itemSpinnerKepemilikanAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spinnerJenisTanah.setAdapter(itemSpinnerKepemilikanAdapter);
        if(value != null){
            int spinnerPosition = itemSpinnerKepemilikanAdapter.getPosition(value);
            spinnerJenisTanah.setSelection(spinnerPosition);
        }

    }
    public void setSpinnerElemenBerbahayaJatuh(View view, String value){
        List<String> itemSpinnerElemenBerbahayaJatuh = new ArrayList<String>();
        itemSpinnerElemenBerbahayaJatuh.add("Pilih Elemen Berbaya Jatuh");
        itemSpinnerElemenBerbahayaJatuh.add("Cerobong Asap");
        itemSpinnerElemenBerbahayaJatuh.add("Genteng");
        itemSpinnerElemenBerbahayaJatuh.add("Penutup Diding Pracetak");
        itemSpinnerElemenBerbahayaJatuh.add("Lainya");

        ArrayAdapter<String> itemSpinnerElemenBerbahayaJatuhAdapter= new ArrayAdapter<String>(fragmentActivity,R.layout.spinner_row, itemSpinnerElemenBerbahayaJatuh);
        itemSpinnerElemenBerbahayaJatuhAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spinnerElemenBerbahayaJatuh.setAdapter(itemSpinnerElemenBerbahayaJatuhAdapter);
        if(value != null){
            int spinnerPosition = itemSpinnerElemenBerbahayaJatuhAdapter.getPosition(value);
            spinnerElemenBerbahayaJatuh.setSelection(spinnerPosition);
        }

        if(spinnerElemenBerbahayaJatuh.getSelectedItem().toString().equals("Lainya")){
            try{
                ed_lainya.setEnabled(true);
                ed_lainya.setText(mPage.getData().getString(FemaDuaPage.LAINYA_DATA_KEY));
            }catch (NullPointerException e){}

        }

    }
}
