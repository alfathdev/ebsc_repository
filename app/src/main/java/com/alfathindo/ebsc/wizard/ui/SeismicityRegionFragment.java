package com.alfathindo.ebsc.wizard.ui;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.alfathindo.ebsc.R;
import com.alfathindo.ebsc.entity.Fema;
import com.alfathindo.ebsc.wizard.model.SeismicityRegionPage;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.ui.PageFragmentCallbacks;

/**
 * Created by safeimuslim on 7/25/16.
 */
public class SeismicityRegionFragment extends Fragment {
    private static final String ARG_KEY = "key";

    private PageFragmentCallbacks mCallbacks;
    private String mKey;
    private SeismicityRegionPage mPage;
    private EditText ed_city,ed_latitude,ed_longitude,ed_ss,ed_s1,ed_nilai;

    public static SeismicityRegionFragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);

        SeismicityRegionFragment fragment = new SeismicityRegionFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public SeismicityRegionFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        mKey = args.getString(ARG_KEY);
        mPage = (SeismicityRegionPage) mCallbacks.onGetPage(mKey);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_seismicity_region_page, container, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        ed_city = ((EditText) rootView.findViewById(R.id.ed_city));
        if(mPage.getData().getString(SeismicityRegionPage.CITY_DATA_KEY) != null){
            //load from db
        //    String city = Fema.getmInstance()._kota;
        //    mPage.getData().putString(SeismicityRegionPage.CITY_DATA_KEY, city);
        //    ed_city.setText(city);
        //    mPage.notifyDataChanged();
        //}else{
            ed_city.setText(mPage.getData().getString(SeismicityRegionPage.CITY_DATA_KEY));
        }

        ed_latitude = ((EditText) rootView.findViewById(R.id.ed_latitude));
        if(mPage.getData().getString(SeismicityRegionPage.LATITUDE_DATA_KEY) != null){
            ed_latitude.setText(mPage.getData().getString(SeismicityRegionPage.LATITUDE_DATA_KEY));
        }

        ed_longitude = ((EditText) rootView.findViewById(R.id.ed_longitude));
        if(mPage.getData().getString(SeismicityRegionPage.LONGITUDE_DATA_KEY) != null){
            ed_longitude.setText(mPage.getData().getString(SeismicityRegionPage.LONGITUDE_DATA_KEY));
        }

        ed_ss= ((EditText) rootView.findViewById(R.id.ed_ss));
        if(mPage.getData().getString(SeismicityRegionPage.SS_DATA_KEY) != null){
            ed_ss.setText(mPage.getData().getString(SeismicityRegionPage.SS_DATA_KEY));
        }

        ed_s1= ((EditText) rootView.findViewById(R.id.ed_s1));
        if(mPage.getData().getString(SeismicityRegionPage.SS1_DATA_KEY) != null){
            ed_s1.setText(mPage.getData().getString(SeismicityRegionPage.SS1_DATA_KEY));
        }

        ed_nilai= ((EditText) rootView.findViewById(R.id.ed_nilai));
        if(mPage.getData().getString(SeismicityRegionPage.NILAI_DATA_KEY) != null){
            ed_nilai.setText(mPage.getData().getString(SeismicityRegionPage.NILAI_DATA_KEY));
        }

        return rootView;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (!(activity instanceof PageFragmentCallbacks)) {
            throw new ClassCastException("Activity must implement PageFragmentCallbacks");
        }

        mCallbacks = (PageFragmentCallbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ed_city.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mPage.getData().putString(SeismicityRegionPage.CITY_DATA_KEY , (editable !=null) ? editable.toString() : null);
                Fema.getmInstance()._kota = editable.toString();
                mPage.notifyDataChanged();
            }
        });

        ed_longitude.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1,
                                          int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                mPage.getData().putString(SeismicityRegionPage.LONGITUDE_DATA_KEY,
                        (editable != null) ? editable.toString() : null);
                Fema.getmInstance()._longitude = editable.toString();
                mPage.notifyDataChanged();
            }
        });

        ed_latitude.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1,
                                          int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                mPage.getData().putString(SeismicityRegionPage.LATITUDE_DATA_KEY,
                        (editable != null) ? editable.toString() : null);
                Fema.getmInstance()._latitude = editable.toString();
                mPage.notifyDataChanged();
            }
        });

        ed_ss.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1,
                                          int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                mPage.getData().putString(SeismicityRegionPage.SS_DATA_KEY,
                        (editable != null) ? editable.toString() : null);
                mPage.notifyDataChanged();
            }
        });

        ed_s1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1,
                                          int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                mPage.getData().putString(SeismicityRegionPage.SS1_DATA_KEY,
                        (editable != null) ? editable.toString() : null);
                mPage.notifyDataChanged();
            }
        });

        ed_nilai.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1,
                                          int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                mPage.getData().putString(SeismicityRegionPage.NILAI_DATA_KEY,
                        (editable != null) ? editable.toString() : null);
                mPage.notifyDataChanged();
            }
        });


    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);

        // In a future update to the support library, this should override setUserVisibleHint
        // instead of setMenuVisibility.
        if (ed_city != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            if (!menuVisible) {
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
            }
        }
    }
}
