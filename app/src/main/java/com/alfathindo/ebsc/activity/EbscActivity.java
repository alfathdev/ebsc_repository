package com.alfathindo.ebsc.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.os.AsyncTaskCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alfathindo.ebsc.R;
import com.alfathindo.ebsc.database.FinalScoreRepo;
import com.alfathindo.ebsc.entity.Fema;
import com.alfathindo.ebsc.database.FemaRepo;
import com.alfathindo.ebsc.entity.FinalScore;
import com.alfathindo.ebsc.entity.Settings;
import com.alfathindo.ebsc.fragments.FragmentDrawer;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.content.DialogInterface;

import au.com.bytecode.opencsv.CSVWriter;

/**
 * Created by safeimuslim on 2/27/16.
 */
public class EbscActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMapClickListener,
        LocationListener,
        OnMapReadyCallback,
        GoogleMap.OnMarkerClickListener,
        CompoundButton.OnCheckedChangeListener
{

    public static final String MyPREFERENCES = "MyPrefs";
    public static final String aktifkan_peta_status = "aktifkan_peta_status";
    public static final String current_lol = "current_lol";
    public static final String current_lang = "current_lang";
    public static final String type_map = "type_map";

    SharedPreferences preferences;
    private Toolbar toolbar;
    private FragmentDrawer fragmentDrawer;

    private GoogleMap mMap;
    private GoogleApiClient googleApiClient;
    private SupportMapFragment supportMapFragment;
    private Location location;
    private LocationRequest mLocationRequest;

    private long UPDATE_INTERVAL = 60000;  /* 60 secs */
    private long FASTEST_INTERVAL = 5000; /* 5 secs */
    private Geocoder geocoder;
    private List<Address> addresses;

    private String country;
    private String city;
    private String region_code;
    private String zipcode;
    private String admin_area;
    private String sub_admin_area;
    private String latitude;
    private String longitude;

    private static final LatLng INDONESIA = new LatLng(-8.567904, 117.339850);

    TextView tvLat;
    TextView tvLng;
    Button btn_simpan;
    View infoWindow;
    SwitchCompat aktifkan_peta;
    LinearLayout btn_copyright;
    private OnInfoWindowElemTouchListener infoButtonListener;

    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setToolbar();
        setUpMapIfNeeded();
        setFragmentDrawer();

        infoWindow = getLayoutInflater().inflate(R.layout.info_window_layout,null);
        btn_simpan = (Button) infoWindow.findViewById(R.id.btn_simpan);
        infoButtonListener = new OnInfoWindowElemTouchListener(btn_simpan,
                getResources().getDrawable(R.drawable.btn_default_normal),
                getResources().getDrawable(R.drawable.btn_default_pressed)) {
            @Override
            protected void onClickConfirmed(View v, Marker marker) {
                Toast.makeText(EbscActivity.this, "s button clicked!",Toast.LENGTH_SHORT).show();
            }
        };
        btn_simpan.setOnTouchListener(infoButtonListener);
    }


    //set up the toolbar
    private void setToolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    //set up the Fragment Drawer
    private void setFragmentDrawer(){
        fragmentDrawer = (FragmentDrawer) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        fragmentDrawer.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout),toolbar);
        fragmentDrawer.setDrawerListener(this);


        aktifkan_peta =(SwitchCompat) findViewById(R.id.aktifkan_peta);
        btn_copyright = (LinearLayout) findViewById(R.id.btn_copyright);
        aktifkan_peta.setChecked(Settings.getmInstance().peta_aktif);

        btn_copyright.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String licenseInfo = GoogleApiAvailability.getInstance().getOpenSourceSoftwareLicenseInfo(v.getContext());
                AlertDialog.Builder LicenseDialog = new AlertDialog.Builder(EbscActivity.this);
                LicenseDialog.setTitle("Legal Notices");
                LicenseDialog.setMessage("Lisensi: "+licenseInfo);
                LicenseDialog.show();
            }
        });
       /* try {
            SharedPreferences pref = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            boolean aktif = pref.getBoolean(aktifkan_peta_status, false);
            aktifkan_peta.setChecked(aktif);
            if(aktif){
                mMap.setOnMapClickListener(this);
                //Toast.makeText(EbscActivity.this, "PETA HARUS AKTIF", Toast.LENGTH_SHORT).show();
            }else{
                //Toast.makeText(EbscActivity.this, "PETA GAK AKTIF", Toast.LENGTH_SHORT).show();
            }

        }catch (NullPointerException e){
            Log.d("Error: ","setFragmentDrawer(); ", e);
        }*/
        aktifkan_peta.setOnCheckedChangeListener(this);
    }

    //switch commpund
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()){
            case R.id.aktifkan_peta:
                if(isChecked){
                    Settings.getmInstance().peta_aktif = true;
                    mMap.setOnMapClickListener(this);
                    Toast.makeText(EbscActivity.this, "PETA AKTIF", Toast.LENGTH_SHORT).show();
                }else{
                    Settings.getmInstance().peta_aktif = false;
                    mMap.setOnMapClickListener(null);
                    mMap.clear();
                    this.addMarkerFromDatabase();
                    Toast.makeText(EbscActivity.this, "PETA TIDAK AKTIF", Toast.LENGTH_SHORT).show();
                }
                /*SharedPreferences pref = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                if(isChecked){
                    mMap.setOnMapClickListener(this);

                    editor.putBoolean(aktifkan_peta_status, true);
                    editor.commit();

                   // Toast.makeText(this,"Edit Peta Aktif \n status: "+pref.getBoolean(aktifkan_peta_status,false) , Toast.LENGTH_SHORT).show();
                }else{
                    mMap.setOnMapClickListener(null);
                    mMap.clear();

                    editor.putBoolean(aktifkan_peta_status, false);
                    editor.commit();

                    this.addMarkerFromDatabase();
                    //Toast.makeText(this,"Edit Peta Tidak Aktif \n status: "+pref.getBoolean(aktifkan_peta_status,false), Toast.LENGTH_SHORT).show();
                }*/
                break;
        }

    }

    //build google api
    private void buildGoogleApiClient(){
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    //set up the map
    private void setUpMapIfNeeded(){
        if(supportMapFragment == null){
            supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            supportMapFragment.getMapAsync(this);
        }else{
            setUpMap();
            //Toast.makeText(this, "Map ada", Toast.LENGTH_SHORT).show();
        }
    }

    //set up on click lisneter
    private void setUpMap(){
        try {
            mMap.setMyLocationEnabled(true);
        }catch (SecurityException e){

        }
        //mMap.setOnMapClickListener(this);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
        //default view indonesia
        if(Fema.getmInstance()._latitude != null){
            LatLng latLng = new LatLng(Double.parseDouble(Fema.getmInstance()._latitude), Double.parseDouble(Fema.getmInstance()._longitude));
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15);
            mMap.animateCamera(cameraUpdate);
        }else{
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(INDONESIA, 4));
        }
        mMap.setOnMarkerClickListener(this);
        customInfoWindow(mMap);
    }

    @Override public void onStart() {
        super.onStart();
        if (googleApiClient != null) {
            googleApiClient.connect();
        }
    }

    @Override public void onStop() {
        super.onStop();
    try {
        if (googleApiClient.isConnected()) {
            googleApiClient.disconnect();
        }
    }catch (NullPointerException e){
        Log.d("googleApiClient", e+"");
    }

    }

    // Inflate the menu; this adds items to the action bar if it is present.
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //return super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    private void setCheckedItem(MenuItem item){
        if (item.isChecked()) item.setChecked(false);
        else item.setChecked(true);
    }
    //set on click menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_search:
                findPlace();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }



    private void displayView(int position) {
        String title = getString(R.string.app_name);
        switch (position) {
            case 0:
                int selection = 0;
                switch (Settings.getmInstance().base_layers){
                    case "NORMAL":
                        selection = 0;
                        break;
                    case "SATELITE":
                        selection = 1;
                        break;
                    case "TERRAIN":
                        selection = 2;
                        break;
                    case "HYBRID":
                        selection = 3;
                        break;
                    case "NONE":
                        selection = 4;
                        break;
                    default:
                        break;
                }
                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_singlechoice);
                arrayAdapter.add("NORMAL");
                arrayAdapter.add("SATELITE");
                arrayAdapter.add("TERRAIN");
                arrayAdapter.add("HYBRID");
                arrayAdapter.add("NONE");

                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

                alertDialog.setTitle("Pilih jenis peta");

                alertDialog.setSingleChoiceItems(arrayAdapter, selection, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreferences pref = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();

                        switch (which){
                        case 0:
                            if (mMap != null) {
                                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                                Settings.getmInstance().base_layers = "NORMAL";
                            }
                            break;
                        case 1:
                            if (mMap != null) {
                                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                                Settings.getmInstance().base_layers = "SATELITE";

                            }
                            break;
                        case 2:
                            if (mMap != null) {
                                mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                                Settings.getmInstance().base_layers = "TERRAIN";

                            }
                            break;
                        case 3:
                            if (mMap != null) {
                                mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                                Settings.getmInstance().base_layers = "HYBRID";

                            }
                            break;
                        case 4:
                            if (mMap != null) {
                                mMap.setMapType(GoogleMap.MAP_TYPE_NONE);
                                Settings.getmInstance().base_layers = "NONE";

                            }
                            break;
                        }
                        dialog.dismiss();
                    }
                });

                alertDialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialog.show();
                break;
            /*case 1:
                String licenseInfo = GoogleApiAvailability.getInstance().getOpenSourceSoftwareLicenseInfo(this);
                AlertDialog.Builder LicenseDialog = new AlertDialog.Builder(EbscActivity.this);
                LicenseDialog.setTitle("Legal Notices");
                LicenseDialog.setMessage("Lisensi: "+licenseInfo);
                LicenseDialog.show();
                break;*/
            case 1:
                Toast.makeText(EbscActivity.this, "MASUK", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(this, PDFWithScaleActivity.class));
                //fragment = new MessagesFragment();
                title = getString(R.string.title_messages);
                break;
            default:
                break;
        }

    }

    @Override
    public void onConnected(Bundle bundle) {

        if(location != null){
            //Toast.makeText(this, "GPS location was found!", Toast.LENGTH_SHORT).show();
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 17);
            mMap.animateCamera(cameraUpdate);

            //get adreess
            geocoder = new Geocoder(getBaseContext(), Locale.getDefault());
            try {
                addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(),1);
                StringBuilder str = new StringBuilder();
                if (geocoder.isPresent()) {
                    //Toast.makeText(getApplicationContext(), "geocoder present", Toast.LENGTH_SHORT).show();
                    Address returnAddress = addresses.get(0);

                    country = returnAddress.getCountryName();
                    city = returnAddress.getLocality();
                    region_code = returnAddress.getCountryCode();
                    zipcode = returnAddress.getPostalCode();

                    str.append(country + "");
                    str.append(city + "" + region_code + "");
                    str.append(zipcode + "");

                    Toast.makeText(getApplicationContext(), str,
                            Toast.LENGTH_SHORT).show();

                } else {
                    Log.e("tag", "geocoder not present");
                }
            }catch (IOException e){
                Log.e("tag", e.getMessage());
            }

            mMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .snippet("Lat: " + location.getLatitude() + ", Lng: " + location.getLongitude())
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location))
                    .title(city+" "+country).draggable(true));

            startLocationUpdates();

        } else {
            Toast.makeText(this, "Current location was null, enable GPS on emulator!", Toast.LENGTH_SHORT).show();
        }
    }

    public void findPlace() {
        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                            .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            Log.d("Error: ",""+e);
            // TODO: Handle the error.
        } catch (GooglePlayServicesNotAvailableException e) {
            Log.d("Error: ",""+e);
            // TODO: Handle the error.
        }
    }

    protected void startLocationUpdates(){
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        try {
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, mLocationRequest,this);
        }catch (SecurityException e){

        }

    }

    private void getAddress(Location location) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onMapClick(LatLng latLng) {
        mMap.clear();
        mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location)).draggable(true).snippet("0"));
        //customInfoWindow(mMap);
        this.addMarkerFromDatabase();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(Settings.getmInstance().peta_aktif){
            mMap.setOnMapClickListener(this);
        }

        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                Log.i("Place: ", ""+ place.getName());
                final LatLng location = place.getLatLng();

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location,15));
                mMap.addMarker(new MarkerOptions().position(location).title(""+place.getName()));


            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i("Error: ", ""+ status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }



        Toast.makeText(EbscActivity.this, "onActivityResult", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //Toast.makeText(EbscActivity.this, "onMapReady()", Toast.LENGTH_SHORT).show();
        buildGoogleApiClient();
        setUpMapIfNeeded();
        this.addMarkerFromDatabase();
        if(Settings.getmInstance().peta_aktif){
            mMap.setOnMapClickListener(this);
        }
        switch (Settings.getmInstance().base_layers){
            case "NORMAL":
                if (mMap != null) {
                    mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                }
                break;
            case "SATELITE":
                if (mMap != null) {
                    mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                }
                break;
            case "TERRAIN":
                if (mMap != null) {
                    mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                }
                break;
            case "HYBRID":
                if (mMap != null) {
                    mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                }
                break;
            case "NONE":
                if (mMap != null) {
                    mMap.setMapType(GoogleMap.MAP_TYPE_NONE);

                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        //marker.showInfoWindow();
        if(marker.getSnippet().equals("1")){
            showEditMarker(marker);
        }else{
            showAlert(marker);
        }
        return true;
    }

    public void addMarkerFromDatabase(){
        FemaRepo femaRepo = new FemaRepo(this);

        List<Fema> femaList = femaRepo.getAllFema();

        for (Fema fm : femaList) {
            double lat = Double.parseDouble(fm.get_latitude());
            double lon = Double.parseDouble(fm.get_longitude());
            int id_fema = Integer.parseInt(fm.get_id());
            String seismicity = fm.get_seismicity();

            FinalScoreRepo finalScoreRepo = new FinalScoreRepo(this);
            if(seismicity.equals("HIGH")){
                String resiko_gempa = finalScoreRepo.getResikoGempa(FinalScore.TABLE_FINAL_SCORE_HIGH, id_fema+"");
                setMarker(resiko_gempa, lat, lon,id_fema);
            }

            if(seismicity.equals("MODERATE")){
                String resiko_gempa = finalScoreRepo.getResikoGempa(FinalScore.TABLE_FINAL_SCORE_MODERATE, id_fema+"");
                setMarker(resiko_gempa, lat, lon,id_fema);
            }

            if(seismicity.equals("LOW")){
                String resiko_gempa = finalScoreRepo.getResikoGempa(FinalScore.TABLE_FINAL_SCORE_LOW, id_fema+"");
                setMarker(resiko_gempa, lat, lon,id_fema);
            }
        }
    }

    public void setMarker(String params, double lat, double lon, int id_fema){
            if(params.equals("Ya")) {
                mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lon)).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location_danger)).draggable(true).title(id_fema+"").snippet("1"));
            }else if(params.equals("Tidak")){
                mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lon)).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location_good)).draggable(true).title(id_fema+"").snippet("1"));
            }else{
                mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lon)).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location_warning)).draggable(true).title(id_fema+"").snippet("1"));
            }

    }

    private void customInfoWindow(GoogleMap googleMap){
        googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                //digunakan jika tampilan pake yang custom

                LatLng latLng = marker.getPosition();
                tvLat = (TextView) infoWindow.findViewById(R.id.tv_lat);
                tvLng = (TextView) infoWindow.findViewById(R.id.tv_lng);
                // Setting the latitude
                tvLat.setText("Latitude:" + latLng.latitude);
                // Setting the longitude
                tvLng.setText("Longitude:"+ latLng.longitude);
                infoButtonListener.setMarker(marker);
                return infoWindow;
            }

            @Override
            public View getInfoContents(Marker marker) {
                //digunakan jika tampilan pake yang default
               return null;
            }
        });
    }

    public void setIcon(String params, ImageView imageView, TextView txt_resiko_gempa){
            if(params.equals("Ya")){
                imageView.setImageResource(R.drawable.ic_hotel_danger);
                txt_resiko_gempa.setText("Kesimpulan\n"+"Perlu dilakukan evaluasi lebih  lanjut atau lebih rinci dengan metode Detail Seismic Evaluation(FEMA 310 , 1998)");
                txt_resiko_gempa.setTextColor(Color.parseColor("#F29B33"));
            }else if(params.equals("Tidak")){
                imageView.setImageResource(R.drawable.ic_hotel_good);
                txt_resiko_gempa.setText("Kesimpulan\n"+"Tidak perlu dilakukan evaluasi lebih lanjut kecuali ingin diketahui lebih detail");
                txt_resiko_gempa.setTextColor(Color.parseColor("#417505"));
            }else{
                imageView.setImageResource(R.drawable.ic_hotel_warning);
                txt_resiko_gempa.setText("Resiko terhadap gempa: "+params);
                txt_resiko_gempa.setTextColor(Color.parseColor("#F8E71C"));
            }

    }

    private void showEditMarker(final Marker marker){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        View dialodView = this.getLayoutInflater().inflate(R.layout.alert_edit_marker, null);

        TextView txt_info_alert = (TextView) dialodView.findViewById(R.id.txt_info_alert);
        TextView txt_resiko_gempa  = (TextView) dialodView.findViewById(R.id.txt_resiko_gempa);
        ImageView imageView = (ImageView) dialodView.findViewById(R.id.icon);
        final Fema fema;
        final FemaRepo femaRepo = new FemaRepo(this);
        fema = femaRepo.getFemaById(marker.getTitle());
        alertDialogBuilder.setTitle(fema.get_nama_bangunan());
        txt_info_alert.setText(
                "Koordinat: "+fema.get_latitude()+","+fema.get_longitude()+
                "\nScreener: "+fema.get_screener()
        );

        if(fema.get_seismicity().equals("HIGH")){
            FinalScore finalScore;
            FinalScoreRepo finalScoreRepo = new FinalScoreRepo(this);
            finalScore = finalScoreRepo.getFinalScroreById(FinalScore.TABLE_FINAL_SCORE_HIGH, marker.getTitle());
            setIcon(finalScore.get_resiko_gempa(), imageView, txt_resiko_gempa);
        }
        if(fema.get_seismicity().equals("MODERATE")){
            FinalScore finalScore;
            FinalScoreRepo finalScoreRepo = new FinalScoreRepo(this);
            finalScore = finalScoreRepo.getFinalScroreById(FinalScore.TABLE_FINAL_SCORE_MODERATE, marker.getTitle());
            setIcon(finalScore.get_resiko_gempa(), imageView, txt_resiko_gempa);
        }
        if(fema.get_seismicity().equals("LOW")){
            FinalScore finalScore;
            FinalScoreRepo finalScoreRepo = new FinalScoreRepo(this);
            finalScore = finalScoreRepo.getFinalScroreById(FinalScore.TABLE_FINAL_SCORE_LOW, marker.getTitle());
            txt_resiko_gempa.setText("Resiko terhadap gempa: "+finalScore.get_resiko_gempa());
            setIcon(finalScore.get_resiko_gempa(), imageView, txt_resiko_gempa);
        }


        alertDialogBuilder.setView(dialodView);

         alertDialogBuilder.setPositiveButton("Download", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                Toast.makeText(EbscActivity.this, "Download Excel", Toast.LENGTH_SHORT).show();
                //ExportDatabaseCSVTask task = new ExportDatabaseCSVTask();
                //AsyncTaskCompat.executeParallel(task);
                File exportDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "");
                if (!exportDir.exists()) {
                    exportDir.mkdirs();
                }
                boolean status = false;
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HH_mm_ss");
                String  currentTimeStamp = dateFormat.format(new Date());
                File file = new File(exportDir, "ebsc_"+fema.get_nama_bangunan()+"_"+currentTimeStamp+".csv");
                try {
                    file.createNewFile();

                    CSVWriter csvWrite = new CSVWriter(new FileWriter(file));

                    ArrayList<String> listdata;

                    //header
                    listdata= new ArrayList<String>();
                    listdata.add("FORMULIR FEMA");
                    String arrStrHead1[] ={listdata.get(0)};
                    csvWrite.writeNext(arrStrHead1);


                    String arrStr[] ={"Kota", fema.get_kota()};
                    csvWrite.writeNext(arrStr);

                    String arrStr1[] ={"Latitude", fema.get_latitude()};
                    csvWrite.writeNext(arrStr1);

                    String arrStr99[] ={"Longitude", fema.get_longitude()};
                    csvWrite.writeNext(arrStr99);

                    String arrStr2[] ={"SS", fema.get_ss()};
                    csvWrite.writeNext(arrStr2);

                    String arrStr3[] ={"S1", fema.get_s1()};
                    csvWrite.writeNext(arrStr3);

                    String arrStr4[] ={"Seismicity Region", fema.get_seismicity()};
                    csvWrite.writeNext(arrStr4);

                    String arrStr5[] ={"Nama Bangunan", fema.get_nama_bangunan()};
                    csvWrite.writeNext(arrStr5);

                    String arrStr6[] ={"Alamat", fema.get_alamat()};
                    csvWrite.writeNext(arrStr6);

                    String arrStr7[] ={"Kode Pos", fema.get_kode_pos()};
                    csvWrite.writeNext(arrStr7);

                    String arrStr8[] ={"Identitas Lain", fema.get_identitas_lain()};
                    csvWrite.writeNext(arrStr8);

                    String arrStr9[] ={"Jumlah lantai", fema.get_no_stories()};
                    csvWrite.writeNext(arrStr9);

                    /*String arrStr10[] ={"Tahun Pembangunan", fema.get_tahun_pembangunan()};
                    csvWrite.writeNext(arrStr10);*/

                    String arrStr11[] ={"Screener", fema.get_screener()};
                    csvWrite.writeNext(arrStr11);

                    String arrStr12[] ={"Tanggal", fema.get_tanggal()};
                    csvWrite.writeNext(arrStr12);


                    String arrStr13[] ={"Luas Area", fema.get_jumlah_lantai()};
                    csvWrite.writeNext(arrStr13);

                    String arrStr133[] ={"Tahun Pembangunan", fema.get_tahun_pembangunan()};
                    csvWrite.writeNext(arrStr133);

                    String arrStr14[] ={"Kegunaan", fema.get_kegunaan()};
                    csvWrite.writeNext(arrStr14);

                    String arrStr15[] ={"Jenis Bangunan", fema.get_jenis_bangunan()};
                    csvWrite.writeNext(arrStr15);

                    String arrStr16[] ={"Vertikal Bangunan", fema.get_vertikal_bangunan()};
                    csvWrite.writeNext(arrStr16);

                    String arrStr199[] ={"Denah Bangunan", fema.get_denah_bangunan()};
                    csvWrite.writeNext(arrStr199);

                    String arrStr1999[] ={"Pembuatan Berdasarkan", fema.get_pembuatan_berdasarkan()};
                    csvWrite.writeNext(arrStr1999);


                    String arrStr17[] ={"Kepemilikan", fema.get_kepemilikan()};
                    csvWrite.writeNext(arrStr17);

                    String arrStr18[] ={"Jumlah Pengguna", fema.get_jumlah_pengguna()};
                    csvWrite.writeNext(arrStr18);

                    String arrStr19[] ={"Jenis Tanah", fema.get_jenis_tanah()};
                    csvWrite.writeNext(arrStr19);

                    String arrStr20[] ={"Elemen Berbahaya Jatuh", fema.get_berbahaya_jatuh()};
                    csvWrite.writeNext(arrStr20);

                    String arrStr21[] ={"Lainya", fema.get_text_berbahaya_jatuh()};
                    csvWrite.writeNext(arrStr21);

                    String enter[] = {" "};
                    csvWrite.writeNext(enter);

                    String enter2[] = {" "};
                    csvWrite.writeNext(enter2);

                    //header
                    String arrStrHead2[] ={"HASIL PERHITUNGAN"};
                    csvWrite.writeNext(arrStrHead2);
                    FinalScore finalScore;
                    FinalScoreRepo finalScoreRepo = new FinalScoreRepo(getApplicationContext());

                    //hasil perhitungan low
                    finalScore = finalScoreRepo.getFinalScroreById(FinalScore.TABLE_FINAL_SCORE_LOW, marker.getTitle());
                    String seismicity[] ={"Seismicity", "Low"};
                    csvWrite.writeNext(seismicity);
                    String basic_score[] ={"Basic Score", finalScore.get_basic_score()};
                    csvWrite.writeNext(basic_score);
                    String stories[] ={"Stories", finalScore.get_stories()};
                    csvWrite.writeNext(stories);

                    String vertical_irregularity[] ={"Vertical Irregularity", finalScore.get_vertical_irregularity()};
                    csvWrite.writeNext(vertical_irregularity);

                    String plan_irregularity[] ={"Plan Irregularity", finalScore.get_plan_irregularity()};
                    csvWrite.writeNext(plan_irregularity);

                    String bencmark[] ={finalScore.get_text_code_bencmark(), finalScore.get_code_bencmark()};
                    csvWrite.writeNext(bencmark);
                    String soiltype[] ={"Soil type", finalScore.get_jenis_tanah_v()};
                    csvWrite.writeNext(soiltype);
                    String final_score[] ={"Final Score", finalScore.get_final_score()};
                    csvWrite.writeNext(final_score);
                    String resiko_gempa[] ={"Resiko Gempa", finalScore.get_resiko_gempa()};
                    csvWrite.writeNext(resiko_gempa);
                    String detail_evaluasi[] ={"Detail Evaluasi", finalScore.get_resiko_gempa()};
                    csvWrite.writeNext(detail_evaluasi);
                    String komentar[] ={"Komentar", finalScore.get_komen()};
                    csvWrite.writeNext(komentar);

                    //enter
                    String enter3[] = {" "};
                    csvWrite.writeNext(enter3);

                    //hasil perhitungan moderate
                    finalScore = finalScoreRepo.getFinalScroreById(FinalScore.TABLE_FINAL_SCORE_MODERATE, marker.getTitle());
                    String seismicitym[] ={"Seismicity", "Moderate"};
                    csvWrite.writeNext(seismicitym);
                    String basic_scorem[] ={"Basic Score", finalScore.get_basic_score()};
                    csvWrite.writeNext(basic_scorem);
                    String storiesm[] ={"Stories", finalScore.get_stories()};
                    csvWrite.writeNext(storiesm);

                    String vertical_irregularitym[] ={"Vertical Irregularity", finalScore.get_vertical_irregularity()};
                    csvWrite.writeNext(vertical_irregularitym);

                    String plan_irregularitym[] ={"Plan Irregularity", finalScore.get_plan_irregularity()};
                    csvWrite.writeNext(plan_irregularitym);

                    String bencmarkm[] ={finalScore.get_text_code_bencmark(), finalScore.get_code_bencmark()};
                    csvWrite.writeNext(bencmarkm);
                    String soiltypem[] ={"Soil type", finalScore.get_jenis_tanah_v()};
                    csvWrite.writeNext(soiltypem);
                    String final_scorem[] ={"Final Score", finalScore.get_final_score()};
                    csvWrite.writeNext(final_scorem);
                    String resiko_gempam[] ={"Resiko Gempa", finalScore.get_resiko_gempa()};
                    csvWrite.writeNext(resiko_gempam);
                    String detail_evaluasim[] ={"Detail Evaluasi", finalScore.get_resiko_gempa()};
                    csvWrite.writeNext(detail_evaluasim);
                    String komentarm[] ={"Komentar", finalScore.get_komen()};
                    csvWrite.writeNext(komentarm);

                    //enter
                    String enter4[] = {" "};
                    csvWrite.writeNext(enter4);

                    //hasil perhitungan high
                    finalScore = finalScoreRepo.getFinalScroreById(FinalScore.TABLE_FINAL_SCORE_HIGH, marker.getTitle());
                    String seismicitymh[] ={"Seismicity", "High"};
                    csvWrite.writeNext(seismicitymh);
                    String basic_scoremh[] ={"Basic Score", finalScore.get_basic_score()};
                    csvWrite.writeNext(basic_scoremh);
                    String storiesmh[] ={"Stories", finalScore.get_stories()};
                    csvWrite.writeNext(storiesmh);

                    String vertical_irregularitymh[] ={"Vertical Irregularity", finalScore.get_vertical_irregularity()};
                    csvWrite.writeNext(vertical_irregularitymh);

                    String plan_irregularitymh[] ={"Plan Irregularity", finalScore.get_plan_irregularity()};
                    csvWrite.writeNext(plan_irregularitymh);

                    String bencmarkmh[] ={finalScore.get_text_code_bencmark(), finalScore.get_code_bencmark()};
                    csvWrite.writeNext(bencmarkmh);
                    String soiltypemh[] ={"Soil type", finalScore.get_jenis_tanah_v()};
                    csvWrite.writeNext(soiltypemh);
                    String final_scoremh[] ={"Final Score", finalScore.get_final_score()};
                    csvWrite.writeNext(final_scoremh);
                    String resiko_gempamh[] ={"Resiko Gempa", finalScore.get_resiko_gempa()};
                    csvWrite.writeNext(resiko_gempamh);
                    String detail_evaluasimh[] ={"Detail Evaluasi", finalScore.get_resiko_gempa()};
                    csvWrite.writeNext(detail_evaluasimh);
                    String komentarmh[] ={"Komentar", finalScore.get_komen()};
                    csvWrite.writeNext(komentarmh);

                    csvWrite.close();
                    status = true;
                }
                catch (IOException e){
                    Log.e("Download Excel", e.getMessage(), e);
                    status = false;
                }

                if(status){
                    Toast.makeText(EbscActivity.this, "Donwnload Berhasil", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(EbscActivity.this, "Donwnload Gagal", Toast.LENGTH_SHORT).show();
                }


            }
        });
        alertDialogBuilder.setNeutralButton("Fema 154", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                Intent intent = new Intent(EbscActivity.this, FemaActivity.class);
                intent.putExtra("id_fema",marker.getTitle());
                startActivity(intent);
            }
        });


        alertDialogBuilder.setNeutralButton("Final Score", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(EbscActivity.this, FinalScoreActivity.class);
                intent.putExtra("id_fema",marker.getTitle());
                startActivity(intent);

            }
        });

        alertDialogBuilder.setNegativeButton("Hapus",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //finish();
                if(femaRepo.delete(marker.getTitle())){
                    mMap.setOnMapClickListener(null);
                    mMap.clear();
                    addMarkerFromDatabase();
                    Toast.makeText(EbscActivity.this, "Data berhasil di hapus!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();

        //WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        //lp.copyFrom(alertDialog.getWindow().getAttributes());
        //lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        //lp.height = WindowManager.LayoutParams.MATCH_PARENT;

        alertDialog.show();
        //alertDialog.getWindow().setAttributes(lp);

    }

    private void showAlert(Marker marker) {
        String pesan = "";
        final LatLng latLng = marker.getPosition();
        pesan = pesan + "Koordinat: " + latLng.latitude +","+ latLng.longitude;

        //get adreess
        geocoder = new Geocoder(getBaseContext(), Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude,1);
            StringBuilder str = new StringBuilder();
            if (geocoder.isPresent()) {
                try {
                    Address returnAddress = addresses.get(0);

                    country = returnAddress.getCountryName();
                    city = returnAddress.getLocality();
                    region_code = returnAddress.getCountryCode();
                    zipcode = returnAddress.getPostalCode();
                    admin_area =returnAddress.getAdminArea();
                    if(returnAddress.getSubAdminArea() != null || returnAddress.getSubAdminArea()==""){
                        sub_admin_area=returnAddress.getSubAdminArea();
                    }else{
                        sub_admin_area = "Kota tidak terdeteksi";
                    }
                }catch (IndexOutOfBoundsException e){
                    Toast.makeText(getApplicationContext(),
                            "maaf anda diluar jangkauan", Toast.LENGTH_SHORT).show();
                    sub_admin_area = "maaf anda diluar jangkauan";
                }
            } else {
                Toast.makeText(getApplicationContext(),
                        "maaf geocoder not present", Toast.LENGTH_SHORT).show();
            }
        }catch (IOException e){
            Log.e("Geocoder", e.getMessage());
        }

        pesan = pesan+"\nKota: "+admin_area;

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        View dialodView = this.getLayoutInflater().inflate(R.layout.alert_edit_marker, null);
        alertDialogBuilder.setView(dialodView);
        TextView txt_info_alert = (TextView) dialodView.findViewById(R.id.txt_info_alert);

        alertDialogBuilder.setTitle("Simpan Lokasi ini?");

        txt_info_alert.setText(pesan);

        //set koordinat
        latitude = String.valueOf(latLng.latitude);
        longitude = String.valueOf(latLng.longitude);

        final String finalPesan = pesan;
        alertDialogBuilder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                Intent intent = new Intent(EbscActivity.this, FemaActivityWizard.class);

                Fema fema = new Fema(admin_area, latitude, longitude, zipcode);
                Fema.getmInstance()._kota = admin_area;
                Fema.getmInstance()._latitude =  latitude;
                Fema.getmInstance()._longitude =  longitude;
                Fema.getmInstance()._kode_pos =  zipcode;
                startActivity(intent);
            }
        });

        alertDialogBuilder.setNegativeButton("Tidak",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //finish();
                dialog.cancel();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


}
