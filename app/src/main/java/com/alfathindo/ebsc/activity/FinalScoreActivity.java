package com.alfathindo.ebsc.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.alfathindo.ebsc.R;
import com.alfathindo.ebsc.fragments.FinalScoreHigh;
import com.alfathindo.ebsc.fragments.FinalScoreLow;
import com.alfathindo.ebsc.fragments.FinalScoreModerate;
import com.alfathindo.ebsc.fragments.FormulirFemaDuaFragment;
import com.alfathindo.ebsc.fragments.FormulirFemaSatuFragment;
import com.alfathindo.ebsc.fragments.ReviewFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by safeimuslim on 3/14/16.
 */
public class FinalScoreActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ProgressDialog progress;

    String id_fema;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.final_score_activity);


        Bundle extras= getIntent().getExtras();
        if(extras!=null){
            id_fema = extras.getString("id_fema");
        }else{
            Toast.makeText(this,"Null Intent",Toast.LENGTH_SHORT).show();
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(NIReviewFragment(id_fema),"REVIEW");
        adapter.addFragment(NIFinalScoreLow(id_fema), "LOW");
        adapter.addFragment(NIFinalScoreModerate(id_fema), "MODERATE");
        adapter.addFragment(NIFinalScoreHigh(id_fema), "HIGH");
        viewPager.setAdapter(adapter);
    }

    public static ReviewFragment NIReviewFragment(String id_fema){
        Bundle args = new Bundle();
        args.putString("id_fema",id_fema);
        ReviewFragment fragment = new ReviewFragment();
        fragment.setArguments(args);
        return fragment;

    }


    public static FinalScoreLow NIFinalScoreLow(String id_fema) {
        
        Bundle args = new Bundle();
        args.putString("id_fema",id_fema);
        FinalScoreLow fragment = new FinalScoreLow();
        fragment.setArguments(args);
        return fragment;
    }

    public static FinalScoreModerate NIFinalScoreModerate(String id_fema) {
        
        Bundle args = new Bundle();
        args.putString("id_fema",id_fema);
        FinalScoreModerate fragment = new FinalScoreModerate();
        fragment.setArguments(args);
        return fragment;
    }

    public static FinalScoreHigh NIFinalScoreHigh(String id_fema) {
        
        Bundle args = new Bundle();
        args.putString("id_fema",id_fema);
        FinalScoreHigh fragment = new FinalScoreHigh();
        fragment.setArguments(args);
        return fragment;
    }
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
