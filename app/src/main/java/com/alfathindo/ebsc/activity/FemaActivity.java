package com.alfathindo.ebsc.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.alfathindo.ebsc.database.FinalScoreRepo;
import com.alfathindo.ebsc.entity.Fema;
import com.alfathindo.ebsc.database.FemaRepo;
import com.alfathindo.ebsc.entity.FinalScore;
import com.alfathindo.ebsc.entity.TableFema154;
import com.alfathindo.ebsc.fragments.FormulirFemaDuaFragment;
import com.alfathindo.ebsc.fragments.FormulirFemaSatuFragment;
import com.alfathindo.ebsc.fragments.FormulirFemaTigaFragment;
import com.alfathindo.ebsc.R;
import com.alfathindo.ebsc.fragments.FormulirSeismicityRegion;
import com.alfathindo.ebsc.model.ToggleButtonGroupTableLayout;

/**
 * Created by safeimuslim on 3/1/16.
 */
public class FemaActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ProgressDialog progress;

    private String latitude,longitude,kota,seismicity;
    private String nama_bangunan,alamat,kode_pos,identitas_lain,no_stories,tahun_pembangunan,screener,tanggal,jumlah_lantai,kegunaan;
    private String kepemilikan,jumlah_pengguna,jenis_tanah,berbahaya_jatuh,jenis_bangunan,bentuk_bangunan;
    private String folder;
    private String id_fema;

    double final_score,basic_score,stories,irregularity,code_bencmark,jenis_tanah_v;
    String resiko_gempa="";
    DecimalFormat df = new DecimalFormat("#.##");

    public EditText ed_latitude ,ed_city,ed_longitude;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fema_activity);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        Bundle extras= getIntent().getExtras();
        if(extras!=null){
            if(extras.getString("id_fema") != null){
                id_fema = extras.getString("id_fema");
                Toast.makeText(this,"Ada id Fema: "+id_fema,Toast.LENGTH_SHORT).show();
            }else{
                longitude = extras.getString("longitude");
                latitude = extras.getString("latitude");
                kota = extras.getString("kota");
                kode_pos = extras.getString("kode_pos");
                Toast.makeText(this,"Tidak ada fema",Toast.LENGTH_SHORT).show();
            }

        }else{
            Toast.makeText(this,"Null Intent",Toast.LENGTH_SHORT).show();
        }

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(nIFormulirSeismicityRegion(id_fema,latitude,longitude,kota,kode_pos), "Seismicity");
        adapter.addFragment(nIFormulirFemaSatuFragment(id_fema), "Fema 1");
        adapter.addFragment(nIFormulirFemaDuaFragment(id_fema), "Fema 2");
        adapter.addFragment(nIFormulirFemaTigaFragment(id_fema), "Fema 3");
        viewPager.setAdapter(adapter);
    }



    public static FormulirSeismicityRegion nIFormulirSeismicityRegion(String id_fema,String latitude,String longitude, String kota,String kode_pos) {
        Bundle args = new Bundle();
        if(id_fema != null){
            args.putString("id_fema", id_fema);
        }else{
            args.putString("latitude", latitude);
            args.putString("longitude", longitude);
            args.putString("kota", kota);
            args.putString("kode_pos", kode_pos);
        }
        FormulirSeismicityRegion fragment = new FormulirSeismicityRegion();
        fragment.setArguments(args);
        return fragment;
    }

    public static FormulirFemaSatuFragment nIFormulirFemaSatuFragment(String id_fema) {

        Bundle args = new Bundle();
        args.putString("id_fema", id_fema);

        FormulirFemaSatuFragment fragment = new FormulirFemaSatuFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static FormulirFemaDuaFragment nIFormulirFemaDuaFragment(String id_fema) {

        Bundle args = new Bundle();
        args.putString("id_fema", id_fema);

        FormulirFemaDuaFragment fragment = new FormulirFemaDuaFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static FormulirFemaTigaFragment nIFormulirFemaTigaFragment(String id_fema) {

        Bundle args = new Bundle();
        args.putString("id_fema", id_fema);

        FormulirFemaTigaFragment fragment = new FormulirFemaTigaFragment();
        fragment.setArguments(args);
        return fragment;
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_fema,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_save:
                if(collectDataPageSeismicityRegion())
                    break;
                if(collectDataPageOne())
                    break;
                if(collectDataPageTwo())
                    break;
                if(collectDataPageThree())
                    break;
                saveDatabase();
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }



    public boolean collectDataPageSeismicityRegion(){
        //collect from page 0
        viewPager.setCurrentItem(0);
        EditText ed_city = (EditText) findViewById(R.id.ed_city);
        EditText ed_longitude = (EditText) findViewById(R.id.ed_longitude);
        EditText ed_latitude = (EditText) findViewById(R.id.ed_latitude);
        EditText ed_seismicity = (EditText) findViewById(R.id.ed_nilai);
        kota = ed_city.getText().toString();
        latitude = ed_latitude.getText().toString();
        longitude = ed_longitude.getText().toString();
        seismicity = ed_seismicity.getText().toString();

        if(kota.isEmpty() || latitude.isEmpty() || longitude.isEmpty() || seismicity.isEmpty()){
            Toast.makeText(getBaseContext(),"Data kota,latitude, longitude, seismicity harus diisi!",Toast.LENGTH_SHORT).show();
            return true;
        }else{
            return false;
        }
    }

    public boolean collectDataPageOne(){
        //collect from page 1
        viewPager.setCurrentItem(1);
        EditText ed_nama_bangunan,ed_alamat,ed_kode_pos,ed_identitas_lain,ed_no_stories,ed_tahun_pembangunan,ed_screener,ed_tanggal,ed_jumlah_lantai,ed_kegunaan;
        ed_nama_bangunan = (EditText) findViewById(R.id.ed_nama_bangunan);
        ed_alamat=(EditText) findViewById(R.id.ed_alamat);
        ed_kode_pos=(EditText) findViewById(R.id.ed_kode_pos);
        ed_identitas_lain=(EditText) findViewById(R.id.ed_identitas_lain);
        ed_no_stories=(EditText) findViewById(R.id.ed_no_stories);
        ed_tahun_pembangunan=(EditText) findViewById(R.id.ed_tahun_pembangunan);
        ed_screener=(EditText) findViewById(R.id.ed_screener);
        ed_tanggal=(EditText) findViewById(R.id.ed_tanggal);
        ed_jumlah_lantai=(EditText) findViewById(R.id.ed_jumlah_lantai);
        ed_kegunaan=(EditText) findViewById(R.id.ed_kegunaan);

        nama_bangunan = ed_nama_bangunan.getText().toString();
        kode_pos  =ed_kode_pos.getText().toString();
        alamat = ed_alamat.getText().toString();
        identitas_lain = ed_identitas_lain.getText().toString();
        no_stories = ed_no_stories.getText().toString();
        tahun_pembangunan = ed_tahun_pembangunan.getText().toString();
        screener = ed_screener.getText().toString();
        tanggal = ed_tanggal.getText().toString();
        jumlah_lantai = ed_jumlah_lantai.getText().toString();
        kegunaan = ed_kegunaan.getText().toString();

        if(nama_bangunan.isEmpty()){
            Toast.makeText(getBaseContext(),"Data nama bangunan harus diisi!",Toast.LENGTH_SHORT).show();
            return true;
        }else{
            return false;
        }
    }

    public boolean collectDataPageTwo(){
        //collect from page 2
        viewPager.setCurrentItem(2);

        //kepemilikan
        ToggleButtonGroupTableLayout radGroupKepemilikan = (ToggleButtonGroupTableLayout) this.findViewById(R.id.radGroupKepemilikan);
        int selGroupKepemilikan = radGroupKepemilikan.getCheckedRadioButtonId();
        RadioButton radKepemilikan = (RadioButton) this.findViewById(selGroupKepemilikan);

        //jumlah pengguna
        ToggleButtonGroupTableLayout radGroupJumlahPengguna = (ToggleButtonGroupTableLayout) findViewById(R.id.radGroupJumlahPengguna);
        int selGroupJumlahPengguna = radGroupJumlahPengguna.getCheckedRadioButtonId();
        RadioButton radJumlahPengguna = (RadioButton) findViewById(selGroupJumlahPengguna);

        //jenis tanah
        ToggleButtonGroupTableLayout radGroupJenisTanah = (ToggleButtonGroupTableLayout) findViewById(R.id.radGroupJenisTanah);
        int selGroupJenisTanah = radGroupJenisTanah.getCheckedRadioButtonId();
        RadioButton radJenisTanah = (RadioButton) findViewById(selGroupJenisTanah);


        //jenis tanah
        ToggleButtonGroupTableLayout radGroupBerbahayaJatuh = (ToggleButtonGroupTableLayout) findViewById(R.id.radGroupBerbahayaJatuh);
        int selBerbahayaJatuh = radGroupBerbahayaJatuh.getCheckedRadioButtonId();
        RadioButton radBerbahayaJatuh = (RadioButton) findViewById(selBerbahayaJatuh);

        //jenis bangunan
        Spinner spinnerJenisBangunan = (Spinner) findViewById(R.id.spinnerJenisBangunan);

        //bentuk bangunan
        Spinner spinnerBentukBangunan = (Spinner) findViewById(R.id.spinnerBentukBangunan);

        try{
            kepemilikan = radKepemilikan.getText().toString();
            jumlah_pengguna = radJumlahPengguna.getText().toString();
            jenis_tanah = radJenisTanah.getText().toString();
            berbahaya_jatuh = radBerbahayaJatuh.getText().toString();
            jenis_bangunan = spinnerJenisBangunan.getSelectedItem().toString();
            bentuk_bangunan = spinnerBentukBangunan.getSelectedItem().toString();
            return false;
        }catch (NullPointerException e){
            Toast.makeText(getBaseContext(),"Data kepemilikan, jumlah pengguna, berbahaya jatuh, jenis bangunan,bentuk bangunan harus dipilih!",Toast.LENGTH_SHORT).show();
            return true;
        }
    }

    public boolean collectDataPageThree(){
        //collect from page 3
        viewPager.setCurrentItem(3);
        EditText ed_folder = (EditText) findViewById(R.id.ed_folder);
        folder = ed_folder.getText().toString();
        if(folder.isEmpty()){
            Toast.makeText(getBaseContext(),"Data nama folder harus diisi!",Toast.LENGTH_SHORT).show();
            return true;
        }else{
            return false;
        }
    }

    public void saveDatabase(){
        FemaRepo femaRepo = new FemaRepo(this);
        Fema fema = new Fema();

        fema.set_kota(kota);
        fema.set_latitude(latitude);
        fema.set_longitude(longitude);
        fema.set_seismicity(seismicity);
        fema.set_nama_bangunan(nama_bangunan);
        fema.set_kode_pos(kode_pos);
        fema.set_alamat(alamat);
        fema.set_identitas_lain(identitas_lain);
        fema.set_no_stories(no_stories);
        fema.set_tahun_pembangunan(tahun_pembangunan);
        fema.set_screener(screener);
        fema.set_tanggal(tanggal);
        fema.set_jumlah_lantai(jumlah_lantai);
        fema.set_kegunaan(kegunaan);
        fema.set_kepemilikan(kepemilikan);
        fema.set_jumlah_pengguna(jumlah_pengguna);
        fema.set_jenis_tanah(jenis_tanah);
        fema.set_berbahaya_jatuh(berbahaya_jatuh);
        fema.set_jenis_bangunan(jenis_bangunan);
        fema.set_bentuk_bangunan(bentuk_bangunan);
        fema.set_folder(folder);

        if(id_fema != null) {
          //  fema.set_id(Integer.parseInt(id_fema));
            finalScore(Integer.parseInt(id_fema));
            if(femaRepo.updateFema(fema) == 1){
                Toast.makeText(this,"Update Fema berhasil",Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(this,"Update Fema gagal",Toast.LENGTH_SHORT).show();
            }
        }else{
            femaRepo.addFema(fema);
            finalScore(femaRepo.getFemaLastId());
        }
    }

    public void finalScore(int id_fema){
        int index = Arrays.asList(TableFema154.bulding_types).indexOf(jenis_bangunan);
        finalScoreLow(index, id_fema);
        finalScoreModerate(index, id_fema);
        finalScoreHigh(index, id_fema);
    }

    public void finalScoreLow(int index,int id_fema) {
        final_score=0;
        basic_score=0;
        stories=0;
        irregularity=0;
        code_bencmark=0;
        jenis_tanah_v=0;
        basic_score = Double.parseDouble(TableFema154.low_basic_score[index]);

        stories = Double.parseDouble(no_stories);
        if (stories > 7){
            stories = Double.parseDouble((TableFema154.low_high_rise[index].equals("N/A")) ? "0" : TableFema154.low_high_rise[index]);
        }else if(stories <=7 && stories >= 4){
            stories = Double.parseDouble((TableFema154.low_mid_rise[index].equals("N/A")) ? "0" : TableFema154.low_mid_rise[index]);
        }else{
            stories = 0;
        }

        irregularity = 0;
        if(bentuk_bangunan.equals("Tidak Simetris")){
            irregularity = Double.parseDouble((TableFema154.low_plan_irregularity[index].equals("N/A")) ? "0" : TableFema154.low_plan_irregularity[index]);
        }else{
            irregularity = Double.parseDouble((TableFema154.low_vertical_irregularity[index].equals("N/A")) ? "0" : TableFema154.low_vertical_irregularity[index]);
        }

        code_bencmark = Double.parseDouble(tahun_pembangunan);
        if(code_bencmark <= 1971){
            code_bencmark = Double.parseDouble((TableFema154.low_pre_code[index].equals("N/A")) ? "0" : TableFema154.low_pre_code[index]);
        }

        if(code_bencmark >=1992){
            code_bencmark = Double.parseDouble((TableFema154.low_post_bencmark[index].equals("N/A")) ? "0" : TableFema154.low_post_bencmark[index]);
        }

        jenis_tanah_v= 0;

        if(jenis_tanah.equals("C - Tanah keras")){
            jenis_tanah_v = Double.parseDouble(TableFema154.low_soil_type_c[index]);
        }
        if(jenis_tanah.equals("D - Tanah kaku")){
            jenis_tanah_v = Double.parseDouble(TableFema154.low_soil_type_d[index]);
        }
        if(jenis_tanah.equals("E - Tanah lunak")){
            jenis_tanah_v = Double.parseDouble(TableFema154.low_soil_type_e[index]);
        }

        final_score = basic_score + stories + irregularity + code_bencmark + jenis_tanah_v;
        setResikoGempa(final_score);
        saveFinalScore(FinalScore.TABLE_FINAL_SCORE_LOW, id_fema);
    }

    public void setResikoGempa(double final_score){
        if(final_score < 2){
            resiko_gempa = "Ya";
        }

        if(final_score >=2 && final_score <=3){
            resiko_gempa = "Sedang";
        }

        if (final_score > 3) {
            resiko_gempa = "Tidak";
        }

    }
    public void finalScoreModerate(int index,int id_fema) {
        final_score=0;
        basic_score=0;
        stories=0;
        irregularity=0;
        code_bencmark=0;
        jenis_tanah_v=0;
        basic_score = Double.parseDouble(TableFema154.moderate_basic_score[index]);

        stories = Double.parseDouble(no_stories);
        if (stories > 7){
            stories = Double.parseDouble((TableFema154.moderate_high_rise[index].equals("N/A")) ? "0" : TableFema154.moderate_high_rise[index]);
        }else if(stories <=7 && stories >= 4){
            stories = Double.parseDouble((TableFema154.moderate_mid_rise[index].equals("N/A")) ? "0" : TableFema154.moderate_mid_rise[index]);
        }else{
            stories = 0;
        }

        irregularity = 0;
        if(bentuk_bangunan.equals("Tidak Simetris")){
            irregularity = Double.parseDouble((TableFema154.moderate_plan_irregularity[index].equals("N/A")) ? "0" : TableFema154.moderate_plan_irregularity[index]);
        }else{
            irregularity = Double.parseDouble((TableFema154.moderate_vertical_irregularity[index].equals("N/A")) ? "0" : TableFema154.moderate_vertical_irregularity[index]);
        }

        code_bencmark = Double.parseDouble(tahun_pembangunan);
        if(code_bencmark <= 1971){
            code_bencmark = Double.parseDouble((TableFema154.moderate_pre_code[index].equals("N/A")) ? "0" : TableFema154.moderate_pre_code[index]);
        }

        if(code_bencmark >=1992){
            code_bencmark = Double.parseDouble((TableFema154.moderate_post_bencmark[index].equals("N/A")) ? "0" : TableFema154.moderate_post_bencmark[index]);
        }

        jenis_tanah_v= 0;

        if(jenis_tanah.equals("C - Tanah keras")){
            jenis_tanah_v = Double.parseDouble(TableFema154.moderate_soil_type_c[index]);
        }
        if(jenis_tanah.equals("D - Tanah kaku")){
            jenis_tanah_v = Double.parseDouble(TableFema154.moderate_soil_type_d[index]);
        }
        if(jenis_tanah.equals("E - Tanah lunak")){
            jenis_tanah_v = Double.parseDouble(TableFema154.moderate_soil_type_e[index]);
        }

        final_score = basic_score + stories + irregularity + code_bencmark + jenis_tanah_v;

        setResikoGempa(final_score);
        saveFinalScore(FinalScore.TABLE_FINAL_SCORE_MODERATE, id_fema);
    }

    public void finalScoreHigh(int index, int id_fema){
        final_score=0;
        basic_score=0;
        stories=0;
        irregularity=0;
        code_bencmark=0;
        jenis_tanah_v=0;
        basic_score = Double.parseDouble(TableFema154.high_basic_score[index]);

        stories = Double.parseDouble(no_stories);
        if (stories > 7){
            stories = Double.parseDouble((TableFema154.high_high_rise[index].equals("N/A")) ? "0" : TableFema154.high_high_rise[index]);
        }else if(stories <=7 && stories >= 4){
            stories = Double.parseDouble((TableFema154.high_mid_rise[index].equals("N/A")) ? "0" : TableFema154.high_mid_rise[index]);
        }else{
            stories = 0;
        }

         irregularity = 0;
        if(bentuk_bangunan.equals("Tidak Simetris")){
            irregularity = Double.parseDouble((TableFema154.high_plan_irregularity[index].equals("N/A")) ? "0" : TableFema154.high_plan_irregularity[index]);
        }else{
            irregularity = Double.parseDouble((TableFema154.high_vertical_irregularity[index].equals("N/A")) ? "0" : TableFema154.high_vertical_irregularity[index]);
        }

         code_bencmark = Double.parseDouble(tahun_pembangunan);
        if(code_bencmark <= 1971){
            code_bencmark = Double.parseDouble((TableFema154.high_pre_code[index].equals("N/A")) ? "0" : TableFema154.high_pre_code[index]);
        }

        if(code_bencmark >=1992){
            code_bencmark = Double.parseDouble((TableFema154.high_post_bencmark[index].equals("N/A")) ? "0" : TableFema154.high_post_bencmark[index]);
        }

         jenis_tanah_v= 0;

        if(jenis_tanah.equals("C - Tanah keras")){
            jenis_tanah_v = Double.parseDouble(TableFema154.high_soil_type_c[index]);
        }
        if(jenis_tanah.equals("D - Tanah kaku")){
            jenis_tanah_v = Double.parseDouble(TableFema154.high_soil_type_d[index]);
        }
        if(jenis_tanah.equals("E - Tanah lunak")){
            jenis_tanah_v = Double.parseDouble(TableFema154.high_soil_type_e[index]);
        }

        final_score = basic_score + stories + irregularity + code_bencmark + jenis_tanah_v;
        setResikoGempa(final_score);
        saveFinalScore(FinalScore.TABLE_FINAL_SCORE_HIGH, id_fema);

    }

    public void saveFinalScore(String tabel, int id_fema_s){
        FinalScoreRepo finalScoreRepo = new FinalScoreRepo(this);
        FinalScore finalScore = new FinalScore();
        finalScore.set_id_fema(id_fema_s+"");
        finalScore.set_basic_score(basic_score+"");
        finalScore.set_stories(stories+"");
        finalScore.set_irregularity(irregularity+"");
        finalScore.set_code_bencmark(code_bencmark+"");
        finalScore.set_jenis_tanah_v(jenis_tanah_v+"");
        finalScore.set_resiko_gempa(resiko_gempa);
        finalScore.set_final_score(df.format(final_score)+"");

        if(id_fema != null) {

            if(finalScoreRepo.updateFinalScore(tabel,finalScore) == 1){
                Toast.makeText(this,"Update Final Score berhasil",Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(this,"Update Final Score gagal",Toast.LENGTH_SHORT).show();
            }
        }else{
            finalScoreRepo.addFinalScore(tabel, finalScore);
            startActivity(new Intent(FemaActivity.this, EbscActivity.class));
        }

    }



    public void loading(){
        progress=new ProgressDialog(this);
        progress.setMessage("Sedang menyimpan data ke database");
        progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progress.setIndeterminate(true);
        progress.setProgress(0);
        progress.show();

        final int totalProgressTime = 100;
        final Thread t = new Thread() {
            int jumpTime = 0;

            @Override
            public void run() {

                while(jumpTime < totalProgressTime) {
                    try {
                        sleep(200);
                        jumpTime += 5;
                        progress.setProgress(jumpTime);
                    }
                    catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        };
        t.start();
    }

}
