package com.alfathindo.ebsc.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.SyncStateContract;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.alfathindo.ebsc.R;
import com.alfathindo.ebsc.database.DatabaseHandler;
import com.alfathindo.ebsc.database.FemaRepo;
import com.alfathindo.ebsc.database.FinalScoreRepo;
import com.alfathindo.ebsc.entity.Fema;
import com.alfathindo.ebsc.entity.FemaCalculate;
import com.alfathindo.ebsc.entity.FinalScore;
import com.alfathindo.ebsc.entity.TableFema154;
import com.alfathindo.ebsc.wizard.model.FemaSatuPage;
import com.alfathindo.ebsc.wizard.model.SeismicityRegionPage;
import com.tech.freak.wizardpager.model.AbstractWizardModel;
import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.ReviewItem;
import com.tech.freak.wizardpager.ui.PageFragmentCallbacks;
import com.tech.freak.wizardpager.ui.ReviewFragment;
import com.tech.freak.wizardpager.ui.StepPagerStrip;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import au.com.bytecode.opencsv.CSVWriter;

/**
 * Created by safeimuslim on 7/25/16.
 */
public class FemaActivityWizard extends FragmentActivity
        implements PageFragmentCallbacks,
        ReviewFragment.Callbacks,
        ModelCallbacks
{
    private ViewPager mPager;
    private MyPagerAdapter mPagerAdapter;

    private boolean mEditingAfterReview;

    private AbstractWizardModel mWizardModel = new FemaActivityWizardModel(this);

    private boolean mConsumePageSelectedEvent;

    private Button mNextButton;
    private Button mPrevButton;

    private List<Page> mCurrentPageSequence;
    private StepPagerStrip mStepPagerStrip;

    private FemaSatuPage mPage;
    private String latitude,longitude,kota,seismicity,ss,s1,text_berbahaya_jatuh;
    private String nama_bangunan,alamat,kode_pos,identitas_lain,no_stories,tahun_pembangunan,screener,tanggal,jumlah_lantai,kegunaan,pembuatan_berdasarkan;
    private String kepemilikan,jumlah_pengguna,jenis_tanah,berbahaya_jatuh,jenis_bangunan,bentuk_bangunan,vertikal_bangunan,denah_bangunan,text_code_benchmark;
    private String folder;
    private String id_fema;

    double final_score,basic_score,stories,irregularity,code_bencmark,jenis_tanah_v,ver_bangunan,den_bangunan;
    String resiko_gempa="";
    DecimalFormat df = new DecimalFormat("#.##");
    FemaRepo femaRepo;
    Fema fema;
    int last_id;

    @Override
    public void onAttachFragment(android.app.Fragment fragment) {
        super.onAttachFragment(fragment);

        Toast.makeText(FemaActivityWizard.this, "MASUK FRAGMENT: "+String.valueOf(fragment.getId()), Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fema_activity_wizard);

        if (savedInstanceState != null) {
            mWizardModel.load(savedInstanceState.getBundle("model"));
        }

        mWizardModel.registerListener(this);
        mPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        //mPagerAdapter = new MyPagerAdapter();
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(mPagerAdapter);
        mStepPagerStrip = (StepPagerStrip) findViewById(R.id.strip);
        mStepPagerStrip.setOnPageSelectedListener(new StepPagerStrip.OnPageSelectedListener() {
            @Override
            public void onPageStripSelected(int position) {
                position = Math.min(mPagerAdapter.getCount() - 1, position);
                if (mPager.getCurrentItem() != position) {
                    mPager.setCurrentItem(position);
                }
            }
        });

        mNextButton = (Button) findViewById(R.id.next_button);
        mPrevButton = (Button) findViewById(R.id.prev_button);

        mPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                mStepPagerStrip.setCurrentPage(position);

                if (mConsumePageSelectedEvent) {
                    mConsumePageSelectedEvent = false;
                    return;
                }

                mEditingAfterReview = false;
                updateBottomBar();
            }
        });
        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mPager.getCurrentItem() == mCurrentPageSequence.size()) {
                    setupValue();
                    if(validasi()){
                        saveDatabase();
                        startActivity(new Intent(FemaActivityWizard.this, EbscActivity.class));

                    }else{
                        Toast.makeText(FemaActivityWizard.this, "Mohon isi data dengan lengkap", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (mEditingAfterReview) {
                        mPager.setCurrentItem(mPagerAdapter.getCount() - 1);
                    } else {
                        mPager.setCurrentItem(mPager.getCurrentItem() + 1);
                    }
                }
            }
        });

        mPrevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPager.setCurrentItem(mPager.getCurrentItem() - 1);
            }
        });

        onPageTreeChanged();
        updateBottomBar();
    }

    @Override
    public Page onGetPage(String key) {
        return mWizardModel.findByKey(key);
    }

    @Override
    public AbstractWizardModel onGetModel() {
        return mWizardModel;
    }

    @Override
    public void onEditScreenAfterReview(String key) {
        for (int i = mCurrentPageSequence.size() - 1; i >= 0; i--) {
            if (mCurrentPageSequence.get(i).getKey().equals(key)) {
                mConsumePageSelectedEvent = true;
                mEditingAfterReview = true;
                mPager.setCurrentItem(i);
                updateBottomBar();
                break;
            }
        }
    }

    @Override
    public void onPageDataChanged(Page page) {
        if (page.isRequired()) {
            if (recalculateCutOffPage()) {
                mPagerAdapter.notifyDataSetChanged();
                updateBottomBar();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mWizardModel.unregisterListener(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBundle("model", mWizardModel.save());
    }

    private void updateBottomBar() {
        int position = mPager.getCurrentItem();
        if (position == mCurrentPageSequence.size()) {
            mNextButton.setText("Selesai");
            //mNextButton.setBackgroundResource(colo);
            mNextButton.setBackgroundResource(R.color.colorPrimary);
            //mNextButton.setBackgroundResource(R.drawable.selectable_item_background);
            mNextButton.setTextAppearance(this, R.style.TextAppearanceFinish);
        } else {
            mNextButton.setText(mEditingAfterReview
                    ? R.string.review
                    : R.string.next);
            mNextButton.setBackgroundResource(R.drawable.selectable_item_background);
            TypedValue v = new TypedValue();
            getTheme().resolveAttribute(android.R.attr.textAppearanceMedium, v, true);
            mNextButton.setTextAppearance(this, v.resourceId);
            mNextButton.setEnabled(position != mPagerAdapter.getCutOffPage());
        }

        mPrevButton.setVisibility(position <= 0 ? View.INVISIBLE : View.VISIBLE);
    }


    @Override
    public void onPageTreeChanged() {
        mCurrentPageSequence = mWizardModel.getCurrentPageSequence();
        recalculateCutOffPage();
        mStepPagerStrip.setPageCount(mCurrentPageSequence.size() + 1); // + 1 = review step
        mPagerAdapter.notifyDataSetChanged();
        updateBottomBar();
    }

    private boolean recalculateCutOffPage() {
        // Cut off the pager adapter at first required page that isn't completed
        int cutOffPage = mCurrentPageSequence.size() + 1;
        for (int i = 0; i < mCurrentPageSequence.size(); i++) {
            Page page = mCurrentPageSequence.get(i);
            if (page.isRequired() && !page.isCompleted()) {
                cutOffPage = i;
                break;
            }
        }

        if (mPagerAdapter.getCutOffPage() != cutOffPage) {
            mPagerAdapter.setCutOffPage(cutOffPage);
            return true;
        }

        return false;
    }
    public class MyPagerAdapter extends FragmentStatePagerAdapter {
        private int mCutOffPage;
        private Fragment mPrimaryItem;


        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            if (i >= mCurrentPageSequence.size()) {
                return new ReviewFragment();
            }

            return mCurrentPageSequence.get(i).createFragment();
        }

        @Override
        public int getItemPosition(Object object) {
            // TODO: be smarter about this
            if (object == mPrimaryItem) {
                // Re-use the current fragment (its position never changes)
                return POSITION_UNCHANGED;
            }

            return POSITION_NONE;
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            super.setPrimaryItem(container, position, object);
            mPrimaryItem = (Fragment) object;
        }

        @Override
        public int getCount() {
            if (mCurrentPageSequence == null) {
                return 0;
            }
            return Math.min(mCutOffPage + 1, mCurrentPageSequence.size() + 1);
        }

        public void setCutOffPage(int cutOffPage) {
            if (cutOffPage < 0) {
                cutOffPage = Integer.MAX_VALUE;
            }
            mCutOffPage = cutOffPage;
        }

        public int getCutOffPage() {
            return mCutOffPage;
        }
    }

    public void setupValue(){
            latitude = Fema.getmInstance()._latitude;
            longitude = Fema.getmInstance()._longitude;
            kota = Fema.getmInstance()._kota;
            seismicity = FemaCalculate.getmInstance().nilai;
            nama_bangunan = Fema.getmInstance()._nama_bangunan;
            alamat = Fema.getmInstance()._alamat;
            kode_pos = Fema.getmInstance()._kode_pos;
            identitas_lain = Fema.getmInstance()._identitas_lain;
            no_stories =  Fema.getmInstance()._no_stories;
            tahun_pembangunan = Fema.getmInstance()._tahun_pembangunan;
            screener = Fema.getmInstance()._screener;
            tanggal = Fema.getmInstance()._tanggal;
            jumlah_lantai = Fema.getmInstance()._jumlah_lantai;
            kegunaan = Fema.getmInstance()._kegunaan;
            kepemilikan= Fema.getmInstance()._kepemilikan;
            jumlah_pengguna= Fema.getmInstance()._jumlah_pengguna;
            jenis_tanah= Fema.getmInstance()._jenis_tanah;
            berbahaya_jatuh= Fema.getmInstance()._berbahaya_jatuh;
            jenis_bangunan= Fema.getmInstance()._jenis_bangunan;
            bentuk_bangunan= Fema.getmInstance()._bentuk_bangunan;
            folder= Fema.getmInstance()._folder;
            id_fema = Fema.getmInstance()._id;
            ss = FemaCalculate.getmInstance().ss;
            s1 = FemaCalculate.getmInstance().s1;
            text_berbahaya_jatuh = Fema.getmInstance()._text_berbahaya_jatuh;
            vertikal_bangunan= Fema.getmInstance()._vertikal_bangunan;
            denah_bangunan = Fema.getmInstance()._denah_bangunan;
            pembuatan_berdasarkan = Fema.getmInstance()._pembuatan_berdasarkan;
    }

    public boolean validasi(){
        return (latitude != null
                && longitude != null
                && kota!= null
                && seismicity!= null
                && ss!= null
                && s1!= null
                && nama_bangunan != null
                && alamat!= null
                && kode_pos!= null
                && identitas_lain!= null
                && no_stories!= null
                && screener!= null
                && tanggal!= null
                && jumlah_lantai!= null
                && kegunaan!= null
                && kepemilikan!= null
                && jumlah_pengguna!= null
                && jenis_tanah!= null
                && berbahaya_jatuh!= null
                && jenis_bangunan!= null
                && pembuatan_berdasarkan!= null
                );
    }

    public void saveDatabase(){
        femaRepo = new FemaRepo(this);
        fema = new Fema();

        fema.set_kota(kota);
        fema.set_latitude(latitude);
        fema.set_longitude(longitude);
        fema.set_seismicity(seismicity);
        fema.set_ss(ss);
        fema.set_s1(s1);
        fema.set_nama_bangunan(nama_bangunan);
        fema.set_kode_pos(kode_pos);
        fema.set_alamat(alamat);
        fema.set_identitas_lain(identitas_lain);
        fema.set_no_stories(no_stories);
        fema.set_tahun_pembangunan(tahun_pembangunan);
        fema.set_screener(screener);
        fema.set_tanggal(tanggal);
        fema.set_jumlah_lantai(jumlah_lantai);
        fema.set_kegunaan(kegunaan);
        fema.set_kepemilikan(kepemilikan);
        fema.set_jumlah_pengguna(jumlah_pengguna);
        fema.set_jenis_tanah(jenis_tanah);
        fema.set_berbahaya_jatuh(berbahaya_jatuh);
        fema.set_jenis_bangunan(jenis_bangunan);
        fema.set_bentuk_bangunan(bentuk_bangunan);
        fema.set_text_berbahaya_jatuh(text_berbahaya_jatuh);
        fema.set_vertikal_bangunan(vertikal_bangunan);
        fema.set_denah_bangunan(denah_bangunan);
        fema.set_folder(folder);
        fema.set_pembuatan_berdasarkan(pembuatan_berdasarkan);

        if(id_fema != null) {
            //update fema
            fema.set_id(id_fema);
            finalScore(Integer.parseInt(id_fema));
            if(femaRepo.updateFema(fema) == 1){
                Toast.makeText(this,"Update Fema berhasil",Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(this,"Update Fema gagal",Toast.LENGTH_SHORT).show();
            }
        }else{
            //insert fema
            femaRepo.addFema(fema);
            //get fema last id
            last_id = femaRepo.getFemaLastId();
            //update final score low meddium high
            finalScore(last_id);
        }
    }

    public void finalScore(int id_fema){
        int index = Arrays.asList(TableFema154.bulding_types).indexOf(jenis_bangunan);
        finalScoreLow(index, id_fema);
        finalScoreModerate(index, id_fema);
        finalScoreHigh(index, id_fema);

    }

    public void finalScoreLow(int index,int id_fema) {
        final_score=0;
        basic_score=0;
        // TODO: 7/28/16 sotories diganti jumlah lantai
        stories=0; // stories = jumlah lantai
        irregularity=0;
        code_bencmark=0;
        jenis_tanah_v=0;
        basic_score = Double.parseDouble(TableFema154.low_basic_score[index]);

        stories = Double.parseDouble(no_stories);
        if (stories > 7){
            stories = Double.parseDouble((TableFema154.low_high_rise[index].equals("N/A")) ? "0" : TableFema154.low_high_rise[index]);
        }else if(stories <=7 && stories >= 4){
            stories = Double.parseDouble((TableFema154.low_mid_rise[index].equals("N/A")) ? "0" : TableFema154.low_mid_rise[index]);
        }else{
            stories = 0;
        }

        ver_bangunan = 0;
        if(vertikal_bangunan.equals("Tidak Beraturan")){
            ver_bangunan = Double.parseDouble((TableFema154.low_vertical_irregularity[index].equals("N/A")) ? "0" : TableFema154.low_vertical_irregularity[index]);
        }

        den_bangunan = 0;
        if(denah_bangunan.equals("Tidak Simetris")){
            den_bangunan = Double.parseDouble((TableFema154.low_plan_irregularity[index].equals("N/A")) ? "0" : TableFema154.low_plan_irregularity[index]);
        }

        /*
        irregularity = 0;
        if(bentuk_bangunan.equals("Tidak Simetris")){
            irregularity = Double.parseDouble((TableFema154.low_plan_irregularity[index].equals("N/A")) ? "0" : TableFema154.low_plan_irregularity[index]);
        }else{
            irregularity = Double.parseDouble((TableFema154.low_vertical_irregularity[index].equals("N/A")) ? "0" : TableFema154.low_vertical_irregularity[index]);
        }
        code_bencmark = Double.parseDouble(tahun_pembangunan);
        if(code_bencmark < 1992){
            code_bencmark = Double.parseDouble((TableFema154.low_pre_code[index].equals("N/A")) ? "0" : TableFema154.low_pre_code[index]);
            text_code_benchmark = "Pre Code";
        }else if( code_bencmark >= 1992){
            code_bencmark = Double.parseDouble((TableFema154.low_post_bencmark[index].equals("N/A")) ? "0" : TableFema154.low_post_bencmark[index]);
            text_code_benchmark = "Post Benchmark";
        }*/

        if(pembuatan_berdasarkan.equals("Sebelum SNI")){
            code_bencmark = Double.parseDouble((TableFema154.low_pre_code[index].equals("N/A")) ? "0" : TableFema154.low_pre_code[index]);
            text_code_benchmark = "Pre Code";
        }else if(pembuatan_berdasarkan.equals("Sesudah SNI")){
            code_bencmark = Double.parseDouble((TableFema154.low_post_bencmark[index].equals("N/A")) ? "0" : TableFema154.low_post_bencmark[index]);
            text_code_benchmark = "Post Benchmark";
        }

        jenis_tanah_v= 0;

        if(jenis_tanah.equals("C - Tanah Keras")){
            jenis_tanah_v = Double.parseDouble(TableFema154.low_soil_type_c[index]);
        }
        if(jenis_tanah.equals("D - Tanah Kaku")){
            jenis_tanah_v = Double.parseDouble(TableFema154.low_soil_type_d[index]);
        }
        if(jenis_tanah.equals("E - Tanah Lunak")){
            jenis_tanah_v = Double.parseDouble(TableFema154.low_soil_type_e[index]);
        }

        final_score = basic_score + stories + ver_bangunan+ den_bangunan + code_bencmark + jenis_tanah_v;
        setResikoGempa(final_score);
        saveFinalScore(FinalScore.TABLE_FINAL_SCORE_LOW, id_fema);
    }

    public void setResikoGempa(double final_score){
        if(final_score >= 2){
            resiko_gempa = "Tidak";
        }
        if (final_score < 2) {
            resiko_gempa = "Ya";
        }

    }
    public void finalScoreModerate(int index,int id_fema) {
        final_score=0;
        basic_score=0;
        stories=0;
        irregularity=0;
        code_bencmark=0;
        jenis_tanah_v=0;
        basic_score = Double.parseDouble(TableFema154.moderate_basic_score[index]);

        stories = Double.parseDouble(no_stories);
        if (stories > 7){
            stories = Double.parseDouble((TableFema154.moderate_high_rise[index].equals("N/A")) ? "0" : TableFema154.moderate_high_rise[index]);
        }else if(stories <=7 && stories >= 4){
            stories = Double.parseDouble((TableFema154.moderate_mid_rise[index].equals("N/A")) ? "0" : TableFema154.moderate_mid_rise[index]);
        }else{
            stories = 0;
        }

        ver_bangunan = 0;
        if(vertikal_bangunan.equals("Tidak Beraturan")){
            ver_bangunan  = Double.parseDouble((TableFema154.moderate_vertical_irregularity[index].equals("N/A")) ? "0" : TableFema154.moderate_vertical_irregularity[index]);

        }

        den_bangunan = 0;
        if(denah_bangunan.equals("Tidak Simetris")){
            den_bangunan = Double.parseDouble((TableFema154.moderate_plan_irregularity[index].equals("N/A")) ? "0" : TableFema154.moderate_plan_irregularity[index]);
        }

        /*
        irregularity = 0;
        if(bentuk_bangunan.equals("Tidak Simetris")){
            irregularity = Double.parseDouble((TableFema154.moderate_plan_irregularity[index].equals("N/A")) ? "0" : TableFema154.moderate_plan_irregularity[index]);
        }else{
            irregularity = Double.parseDouble((TableFema154.moderate_vertical_irregularity[index].equals("N/A")) ? "0" : TableFema154.moderate_vertical_irregularity[index]);
        }

        code_bencmark = Double.parseDouble(tahun_pembangunan);
        if(code_bencmark < 1992){
            code_bencmark = Double.parseDouble((TableFema154.moderate_pre_code[index].equals("N/A")) ? "0" : TableFema154.moderate_pre_code[index]);
            text_code_benchmark = "Pre Code";
        }else if( code_bencmark >= 1992){
            code_bencmark = Double.parseDouble((TableFema154.moderate_post_bencmark[index].equals("N/A")) ? "0" : TableFema154.moderate_post_bencmark[index]);
            text_code_benchmark = "Post Benchmark";
        }*/

        if(pembuatan_berdasarkan.equals("Sebelum SNI")){
            code_bencmark = Double.parseDouble((TableFema154.moderate_pre_code[index].equals("N/A")) ? "0" : TableFema154.moderate_pre_code[index]);
            text_code_benchmark = "Pre Code";
        }else if(pembuatan_berdasarkan.equals("Sesudah SNI")){
            code_bencmark = Double.parseDouble((TableFema154.moderate_post_bencmark[index].equals("N/A")) ? "0" : TableFema154.moderate_post_bencmark[index]);
            text_code_benchmark = "Post Benchmark";
        }

        jenis_tanah_v= 0;

        if(jenis_tanah.equals("C - Tanah Keras")){
            jenis_tanah_v = Double.parseDouble(TableFema154.moderate_soil_type_c[index]);
        }
        if(jenis_tanah.equals("D - Tanah Kaku")){
            jenis_tanah_v = Double.parseDouble(TableFema154.moderate_soil_type_d[index]);
        }
        if(jenis_tanah.equals("E - Tanah Lunak")){
            jenis_tanah_v = Double.parseDouble(TableFema154.moderate_soil_type_e[index]);
        }

        final_score = basic_score + stories + ver_bangunan +den_bangunan + code_bencmark + jenis_tanah_v;



        setResikoGempa(final_score);
        saveFinalScore(FinalScore.TABLE_FINAL_SCORE_MODERATE, id_fema);
    }

    public void finalScoreHigh(int index, int id_fema){
        final_score=0;
        basic_score=0;
        stories=0;
        irregularity=0;
        code_bencmark=0;
        jenis_tanah_v=0;
        basic_score = Double.parseDouble(TableFema154.high_basic_score[index]);

        stories = Double.parseDouble(no_stories);
        if (stories > 7){
            stories = Double.parseDouble((TableFema154.high_high_rise[index].equals("N/A")) ? "0" : TableFema154.high_high_rise[index]);
        }else if(stories <=7 && stories >= 4){
            stories = Double.parseDouble((TableFema154.high_mid_rise[index].equals("N/A")) ? "0" : TableFema154.high_mid_rise[index]);
        }else{
            stories = 0;
        }

        ver_bangunan = 0;
        if(vertikal_bangunan.equals("Tidak Beraturan")){

            ver_bangunan =  Double.parseDouble((TableFema154.high_vertical_irregularity[index].equals("N/A")) ? "0" : TableFema154.high_vertical_irregularity[index]);
        }

        den_bangunan = 0;
        if(denah_bangunan.equals("Tidak Simetris")){
            den_bangunan = Double.parseDouble((TableFema154.high_plan_irregularity[index].equals("N/A")) ? "0" : TableFema154.high_plan_irregularity[index]);
        }

        /*
        irregularity = 0;
        if(bentuk_bangunan.equals("Tidak Simetris")){
            irregularity = Double.parseDouble((TableFema154.high_plan_irregularity[index].equals("N/A")) ? "0" : TableFema154.high_plan_irregularity[index]);
        }else{
            irregularity = Double.parseDouble((TableFema154.high_vertical_irregularity[index].equals("N/A")) ? "0" : TableFema154.high_vertical_irregularity[index]);
        }

        code_bencmark = Double.parseDouble(tahun_pembangunan);
        if(code_bencmark < 1992){
            code_bencmark = Double.parseDouble((TableFema154.high_pre_code[index].equals("N/A")) ? "0" : TableFema154.high_pre_code[index]);
            text_code_benchmark = "Pre Code";
        }else if( code_bencmark >= 1992){
            code_bencmark = Double.parseDouble((TableFema154.high_post_bencmark[index].equals("N/A")) ? "0" : TableFema154.high_post_bencmark[index]);
            text_code_benchmark = "Post Benchmark";
        }*/

        if(pembuatan_berdasarkan.equals("Sebelum SNI")){
            code_bencmark = Double.parseDouble((TableFema154.high_pre_code[index].equals("N/A")) ? "0" : TableFema154.high_pre_code[index]);
            text_code_benchmark = "Pre Code";
        }else if(pembuatan_berdasarkan.equals("Sesudah SNI")){
            code_bencmark = Double.parseDouble((TableFema154.high_post_bencmark[index].equals("N/A")) ? "0" : TableFema154.high_post_bencmark[index]);
            text_code_benchmark = "Post Benchmark";
        }

        jenis_tanah_v= 0;

        if(jenis_tanah.equals("C - Tanah Keras")){
            jenis_tanah_v = Double.parseDouble(TableFema154.high_soil_type_c[index]);
        }
        if(jenis_tanah.equals("D - Tanah Kaku")){
            jenis_tanah_v = Double.parseDouble(TableFema154.high_soil_type_d[index]);
        }
        if(jenis_tanah.equals("E - Tanah Lunak")){
            jenis_tanah_v = Double.parseDouble(TableFema154.high_soil_type_e[index]);
        }

        final_score = basic_score + stories + ver_bangunan+ den_bangunan + code_bencmark + jenis_tanah_v;
        setResikoGempa(final_score);
        saveFinalScore(FinalScore.TABLE_FINAL_SCORE_HIGH, id_fema);

    }

    public void saveFinalScore(String tabel, int id_fema_s){
        FinalScoreRepo finalScoreRepo = new FinalScoreRepo(this);
        FinalScore finalScore = new FinalScore();
        finalScore.set_id_fema(id_fema_s+"");
        finalScore.set_basic_score(basic_score+"");
        finalScore.set_stories(stories+"");
        finalScore.set_irregularity(irregularity+"");
        finalScore.set_vertical_irregularity(ver_bangunan+"");
        finalScore.set_plan_irregularity(den_bangunan+"");
        finalScore.set_code_bencmark(code_bencmark+"");
        finalScore.set_jenis_tanah_v(jenis_tanah_v+"");
        finalScore.set_resiko_gempa(resiko_gempa);
        finalScore.set_final_score(String.format("%.2f,",final_score) +"");
        finalScore.set_text_code_bencmark(text_code_benchmark);

        if(id_fema != null) {

            if(finalScoreRepo.updateFinalScore(tabel,finalScore) == 1){
                Toast.makeText(this,"Update Final Score berhasil",Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(this,"Update Final Score gagal",Toast.LENGTH_SHORT).show();
            }
        }else{
            finalScoreRepo.addFinalScore(tabel, finalScore);
        }
    }



}
