package com.alfathindo.ebsc.activity;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.net.Uri;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.Toast;

import com.alfathindo.ebsc.R;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by safeimuslim on 10/2/16.
 */

public class PDFWithScaleActivity extends AppCompatActivity {

    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Manual Book");
        setContentView(R.layout.activity_pdf);

        webView = (WebView) findViewById(R.id.webView1);



        String url = "https://drive.google.com/file/d/0By_3UeLr7qyjaUNabm5KNnVqMlE/view";
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);
        //File pdfFile = new File(Environment.getExternalStorageDirectory(),"test.pdf");//File path

        //File file = new File("file:///android_asset/test.pdf");
        //Intent intent = new Intent(Intent.ACTION_VIEW);
        //intent.setDataAndType(Uri.fromFile(file), "application/pdf");
        //intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        //startActivity(intent);
//        CopyReadAssets();

      /*  button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File pdfFile = new File(Environment.getExternalStorageDirectory(),"Contoh-Surat-Pernyataan.pdf");
                try {
                    if (pdfFile.exists()) {
                        Uri path = Uri.fromFile(pdfFile);
                        Intent objIntent = new Intent(Intent.ACTION_VIEW);
                        objIntent.setDataAndType(path, "application/pdf");
                        objIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(objIntent);
                    } else {
                        Toast.makeText(PDFWithScaleActivity.this, "File Not Found",
                                Toast.LENGTH_SHORT).show();
                    }
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(PDFWithScaleActivity.this,
                            "No Viewer Application Found", Toast.LENGTH_SHORT)
                            .show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
*/


        //pdfViewPager = (PDFViewPager) findViewById(R.id.pdfViewPager);

     //   adapter = new PDFPagerAdapter(this, getCacheDir()+"sample.pdf");
       // pdfViewPager.setAdapter(adapter);

    }

    private void configureActionBar() {
    //    int color = getResources().getColor(R.color.pdfViewPager_ab_color);

  //      ActionBar ab = getSupportActionBar();
//        ab.setBackgroundDrawable(new ColorDrawable(color));
    }

    private void CopyReadAssets()
    {

        AssetManager assetManager = getAssets();

        InputStream in = null;
        OutputStream out = null;
        File file = new File(getFilesDir(), "test.pdf");
        try
        {
            in = assetManager.open("test.pdf");
            out = openFileOutput(file.getName(), Context.MODE_WORLD_READABLE);

            copyFile(in, out);
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
        } catch (Exception e)
        {
            Log.e("tag", e.getMessage());
        }

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(
                Uri.parse("file://" + getFilesDir() + "/test.pdf"),
                "application/pdf");

        startActivity(intent);
    }
    private void copyFile(InputStream in, OutputStream out) throws IOException
    {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1)
        {
            out.write(buffer, 0, read);
        }
    }


}