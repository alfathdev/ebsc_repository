package com.alfathindo.ebsc.activity;

import android.content.Context;

import com.alfathindo.ebsc.entity.Fema;
import com.alfathindo.ebsc.wizard.model.FemaDuaPage;
import com.alfathindo.ebsc.wizard.model.FemaSatuPage;
import com.alfathindo.ebsc.wizard.model.FemaTigaPage;
import com.alfathindo.ebsc.wizard.model.SeismicityRegionPage;
import com.tech.freak.wizardpager.model.AbstractWizardModel;
import com.tech.freak.wizardpager.model.PageList;

/**
 * Created by safeimuslim on 7/25/16.
 */
public class FemaActivityWizardModel extends AbstractWizardModel {
    public FemaActivityWizardModel(Context context){
        super(context);
    }
    @Override
    protected PageList onNewRootPageList() {
        return new PageList(
                new SeismicityRegionPage(this, "Seismicity Region ").setRequired(true),
                new FemaSatuPage(this, "Formulir Fema ke 1").setRequired(true),
                new FemaDuaPage(this, "Formulir Fema ke 2").setRequired(true),
                new FemaTigaPage(this, "Formulir Fema ke 3").setRequired(true)
        );
    }
}
